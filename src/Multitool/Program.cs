﻿using McMaster.Extensions.CommandLineUtils;
using NLog;
using OpenWoW.Multitool.Commands;
using System;
using System.Linq;
using System.Reflection;

namespace OpenWoW.Multitool
{
    class Program
    {
        static void Main(string[] args)
        {
            ToolConfig.Initialize();
            SetupLogging();

            var app = new CommandLineApplication
            {
                Name = "Multitool"
            };
            app.HelpOption(inherited: true);

            // Discover and register commands
            var commandTypes = AppDomain.CurrentDomain.GetAssemblies()
                .SelectMany(a => a.GetTypes())
                .Where(t => t.GetInterfaces().Contains(typeof(ICommand)));

            foreach (var commandType in commandTypes)
            {
                var methodInfo = commandType.GetMethod("Configure", BindingFlags.Public | BindingFlags.Static);
                if (methodInfo == null)
                {
                    throw new NotImplementedException($"{commandType.FullName} does not implement the static Configure method!");
                }
                var configureAction = (Action<CommandLineApplication>)methodInfo.CreateDelegate(typeof(Action<CommandLineApplication>));
                app.Command(((ICommand)Activator.CreateInstance(commandType)).Name, configureAction);
            }

            // Show help text if nothing useful is provided
            app.OnExecute(() =>
            {
                app.ShowHelp(false);
                return 1;
            });

            // Run!
            app.Execute(args);
        }

        private const string _logLayout = "${longdate}  ${level:uppercase = true:padding=-5}  ${logger} | ${message}";

        private static void SetupLogging()
        {
            // Setup NLog
            var config = new NLog.Config.LoggingConfiguration();
            var console = new NLog.Targets.ColoredConsoleTarget("console")
            {
                Layout = _logLayout
            };
            config.AddRule(LogLevel.Trace, LogLevel.Fatal, console);

            LogManager.Configuration = config;
        }
    }
}
