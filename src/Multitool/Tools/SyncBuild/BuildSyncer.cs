﻿using Jil;
using Microsoft.EntityFrameworkCore;
using OpenWoW.Common;
using OpenWoW.Database.Web.Contexts;
using OpenWoW.Database.Web.Entities;
using OpenWoW.LibWowData.DBConfig;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;

namespace OpenWoW.Multitool.Tools.SyncBuild
{
    public partial class BuildSyncer
    {
        private const string UNKNOWN_ICON_NAME = "inv_misc_questionmark";
        private static string UNKNOWN_ICON_HASH;
        private static int UNKNOWN_ICON_ID;

        private readonly string BuildPath;
        private readonly BuildData Build;
        private WebDbContext Context;

        private Dictionary<int, Dictionary<string, int>> ColumnNamesMap;
        private Dictionary<string, (string, string)> IconMap = new Dictionary<string, (string, string)>(StringComparer.InvariantCultureIgnoreCase);
        private Dictionary<string, int> RaceIconMap = new Dictionary<string, int>();
        private Dictionary<string, int> TableHashMap;
        private HashSet<int> ValidIconIds;

        private static readonly NLog.Logger _logger = NLog.LogManager.GetCurrentClassLogger();

        public BuildSyncer(string buildPath, BuildData build)
        {
            BuildPath = buildPath;
            Build = build;
        }

        public void Run()
        {
            LoadImageHashes();

            using (Context = ToolConfig.WebDatabaseContext)
            {
                ColumnNamesMap = Context.BuildHash
                    .Where(bh => bh.BuildId == Build.Id)
                    .Join(
                        Context.DbconfigLookup,
                        bh => new { bh._TableHash, bh._LayoutHash },
                        dl => new { dl._TableHash, dl._LayoutHash },
                        (bh, dl) => dl
                    )
                    .ToDictionary(
                        dl => dl._TableHash,
                        dl => dl.FieldNames
                            .Where(x => x != "ID") // TODO: fix this? ID is not included in the data row when it is inline
                            .Select((value, index) => (value, index))
                            .ToDictionary(x => x.value, x => x.index)
                    );

                TableHashMap = Context.TableHashLookup
                    .ToDictionary(thl => thl.Name, thl => thl._TableHash, StringComparer.InvariantCultureIgnoreCase);

                // Icons need to be first, a lot of things depend on it
                SyncIcon();

                SyncClass();
                SyncRace();

                SyncCurrencyCategory();
                SyncCurrencyTypes();
                SyncSkillLine();
            }
        }

        private void LoadImageHashes()
        {
            string hashesFilename = Path.Combine(BuildPath, "icons", "_hashes.txt");
            // original filename:sanitized filename:hash
            foreach (string line in File.ReadAllLines(hashesFilename))
            {
                var parts = line.Split(':');
                if (parts.Length == 3)
                {
                    IconMap[parts[0]] = (parts[1], parts[2]);
                }
            }

            UNKNOWN_ICON_HASH = IconMap[UNKNOWN_ICON_NAME].Item2;
        }

        private int? GetForeignKeyId(int id)
        {
            return id > 0 ? (int?)id : null;
        }

        private int GetIconId(int iconId)
        {
            return ValidIconIds.Contains(iconId) ? iconId : UNKNOWN_ICON_ID;
        }

        private void Sync<TTarget>(string tableName, DbSet<TTarget> dbSet, Func<Dictionary<string, int>, int, dynamic, TTarget> processFunc,
            Func<List<TTarget>> preloadFunc = null, Action<List<TTarget>> postFunc = null, int upsertSize = 10000)
            where TTarget : class
        {
            var sw = Stopwatch.StartNew();
            var timings = new List<(string, long)>();

            int tableHash = TableHashMap[tableName];
            var columnMap = ColumnNamesMap[tableHash];
            var rawData = Context.RawMetadata
                .Where(rm => rm.Locale == WowLocale.enUS && rm.BuildId == Build.Id && rm._TableHash == tableHash)
                .Join(
                    Context.RawData,
                    rm => rm.RowHash,
                    rd => rd.RowHash,
                    (rm, rd) => Tuple.Create(rm.RowId, rd.JsonData)
                )
                .ToArray();

            _logger.Debug("{0}: {1}", tableName, string.Join(",", columnMap.Keys));

            sw.Stop();
            timings.Add(("rawData", sw.ElapsedMilliseconds));
            sw.Restart();

            var results = new List<TTarget>();

            if (preloadFunc != null)
            {
                results.AddRange(preloadFunc());
                sw.Stop();
                timings.Add(("preloadFunc", sw.ElapsedMilliseconds));
                sw.Restart();
            }

            foreach ((int id, string json) in rawData)
            {
                dynamic data = JSON.DeserializeDynamic(json);
                TTarget result = processFunc(columnMap, id, data);
                if (result != null)
                {
                    results.Add(result);
                }
            }

            sw.Stop();
            timings.Add(("process", sw.ElapsedMilliseconds));

            if (postFunc != null)
            {
                postFunc(results);
                sw.Stop();
                timings.Add(("postFunc", sw.ElapsedMilliseconds));
                sw.Restart();
            }

            //_logger.Debug("{0}: {1} results", tableName, results.Count);

            // Do upserts in chunks to get around parameter limit, ugh
            if (results.Count > 0)
            {
                for (int i = 0; i < results.Count; i += upsertSize)
                {
                    dbSet.UpsertRange(results.GetRange(i, Math.Min(upsertSize, results.Count - i))).Run();
                }
            }

            sw.Stop();
            timings.Add(("database", sw.ElapsedMilliseconds));

            _logger.Debug("{0} synced: {1}", tableName, string.Join(" ", timings.Select(t => $"{t.Item1}={t.Item2}ms")));
        }
    }
}
