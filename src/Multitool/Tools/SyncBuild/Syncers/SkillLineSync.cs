﻿using OpenWoW.Database.Web.Entities;

namespace OpenWoW.Multitool.Tools.SyncBuild
{
    public partial class BuildSyncer
    {
        private void SyncSkillLine()
        {
            Sync("SkillLine", Context.WowSkillLine, (columnMap, id, data) =>
            {
                return new WowSkillLine
                {
                    BuildId = Build.Id,
                    Id = id,
                    CategoryId = data[columnMap["Category"]],
                    Flags = data[columnMap["Flags"]],
                    IconId = GetIconId((int)data[columnMap["SpellIcon FileDataID"]]),
                    ParentId = GetForeignKeyId((int)data[columnMap["Parent SkillLineID"]]),
                    ParentTier = data[columnMap["ParentTierIndex"]],
                    Description = data[columnMap["Description"]],
                    Name = data[columnMap["DisplayName"]],
                    HordeName = data[columnMap["HordeDisplayName"]],
                    NeutralName = data[columnMap["NeutralDisplayName"]],
                };
            });
        }
    }
}
