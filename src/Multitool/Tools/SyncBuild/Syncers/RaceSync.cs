﻿using OpenWoW.Database.Web.Entities;
using System.Collections.Generic;

namespace OpenWoW.Multitool.Tools.SyncBuild
{
    public partial class BuildSyncer
    {
        // Some race names have different atlas names for whatever reason
        private static readonly Dictionary<string, string> _raceNameMap = new Dictionary<string, string>
        {
            { "highmountaintauren", "highmountain" },
            { "lightforgeddraenei", "lightforged" },
            { "scourge", "undead" },
            { "zandalaritroll", "zandalari" },
        };

        private void SyncRace()
        {
            Sync("ChrRaces", Context.WowRace, (columnMap, id, data) =>
            {
                string name = data[columnMap["Name"]];
                string cfs = ((string)data[columnMap["ClientFileString"]]).ToLowerInvariant();
                cfs = _raceNameMap.GetValueOrDefault(cfs, cfs);
                string femaleIcon = $"raceicon_{cfs}_female";
                string maleIcon = $"raceicon_{cfs}_male";

                if (!RaceIconMap.ContainsKey(femaleIcon) || !RaceIconMap.ContainsKey(maleIcon))
                {
                    _logger.Warn("Skipping race {0}:{1}, missing icons? {2} {3}", id, name, femaleIcon, maleIcon);
                    return null;
                }

                return new WowRace
                {
                    BuildId = Build.Id,
                    Id = id,
                    Faction = data[columnMap["Faction"]],
                    Flags = data[columnMap["Flags"]],
                    FemaleIconId = RaceIconMap[femaleIcon],
                    MaleIconId = RaceIconMap[maleIcon],
                    StartingLevel = data[columnMap["StartingLevel"]],
                    Name = name,
                    NameFemale = data[columnMap["NameFemale"]],
                };
            });
        }
    }
}
