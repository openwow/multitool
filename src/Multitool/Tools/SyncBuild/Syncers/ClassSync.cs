﻿using OpenWoW.Database.Web.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace OpenWoW.Multitool.Tools.SyncBuild
{
    public partial class BuildSyncer
    {
        private void SyncClass()
        {
            Sync("ChrClasses", Context.WowClass, (columnMap, id, data) =>
            {
                return new WowClass
                {
                    BuildId = Build.Id,
                    Id = id,
                    Flags = data[columnMap["Flags"]],
                    IconId = GetIconId((int)data[columnMap["Icon FileDataID"]]), // TODO: fix not eating spaces
                    StartingLevel = data[columnMap["StartingLevel"]],
                    Name = data[columnMap["Name"]],
                };
            });
        }
    }
}
