﻿using OpenWoW.Database.Web.Contexts;
using OpenWoW.Database.Web.Entities;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace OpenWoW.Multitool.Tools.SyncBuild
{
    public partial class BuildSyncer
    {
        private void SyncIcon()
        {
            Sync("ManifestInterfaceData", Context.WowIcon, (columnMap, id, data) =>
            {
                string filePath = data[columnMap["FilePath"]];
                if (!filePath.Equals(@"interface\icons\", StringComparison.InvariantCultureIgnoreCase))
                {
                    return null;
                }

                string basename = Path.GetFileNameWithoutExtension((string)data[columnMap["FileName"]]);
                string sanitized, hash;
                if (IconMap.TryGetValue(basename, out var tuple))
                {
                    (sanitized, hash) = (tuple.Item1, tuple.Item2);
                }
                else
                {
                    (sanitized, hash) = (UNKNOWN_ICON_NAME, UNKNOWN_ICON_HASH);
                }

                if (sanitized == UNKNOWN_ICON_NAME)
                {
                    UNKNOWN_ICON_ID = id;
                }

                return new WowIcon
                {
                    BuildId = Build.Id,
                    Id = id,
                    Name = sanitized,
                    Hash = hash,
                };
            }, preloadFunc: () =>
            {
                var ret = new List<WowIcon>();
                int id = 100_000_000;
                foreach (var kvp in IconMap.Where(x => x.Key.StartsWith("raceicon_")).OrderBy(x => x.Key))
                {
                    RaceIconMap[kvp.Value.Item1] = id;
                    ret.Add(new WowIcon
                    {
                        BuildId = Build.Id,
                        Id = id,
                        Name = kvp.Value.Item1,
                        Hash = kvp.Value.Item2,
                    });
                    id++;
                }
                return ret;
            }, postFunc: (results) =>
            {
                ValidIconIds = new HashSet<int>(results.Select(x => x.Id));
            });
        }
    }
}
