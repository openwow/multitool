﻿using OpenWoW.Common;
using OpenWoW.Database.Web.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace OpenWoW.Multitool.Tools.SyncBuild
{
    public partial class BuildSyncer
    {
        private void SyncCurrencyCategory()
        {
            Sync("CurrencyCategory", Context.WowCurrencyCategory, (columnMap, id, data) =>
            {
                return new WowCurrencyCategory
                {
                    BuildId = Build.Id,
                    Id = id,
                    ExpansionId = (int)data[columnMap["ExpansionID"]],
                    Flags = data[columnMap["Flags"]],
                    Name = data[columnMap["Name"]],
                };
            });
        }

        private void SyncCurrencyTypes()
        {
            Sync("CurrencyTypes", Context.WowCurrency, (columnMap, id, data) =>
            {
                return new WowCurrency
                {
                    BuildId = Build.Id,
                    Id = id,
                    CategoryId = data[columnMap["CategoryID"]],
                    FactionId = GetForeignKeyId((int)data[columnMap["FactionID"]]),
                    Flags = data[columnMap["Flags"]],
                    IconId = GetIconId((int)data[columnMap["Icon FileDataID"]]),
                    MaxPerWeek = data[columnMap["MaxEarnablePerWeek"]],
                    MaxQuantity = data[columnMap["MaxQuantity"]],
                    Quality = data[columnMap["Quality"]],
                    Name = data[columnMap["Name"]],
                    Description = data[columnMap["Description"]]
                };
            });
        }
    }
}
