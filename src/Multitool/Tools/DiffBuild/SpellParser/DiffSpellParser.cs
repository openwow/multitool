﻿using OpenWoW.Common;
using OpenWoW.LibSpellParser;
using OpenWoW.LibSpellParser.Interfaces;
using OpenWoW.Multitool.Entities;
using OpenWoW.Multitool.Tools.DiffBuild.Loaders;
using System;
using System.Collections.Generic;
using System.Linq;

namespace OpenWoW.Multitool.Tools.DiffBuild
{
    internal class DiffSpellParser : BaseSpellParser
    {
        private static NLog.Logger _logger = NLog.LogManager.GetCurrentClassLogger();

        private readonly BuildLoader BuildLoader;

        public DiffSpellParser(BuildLoader buildLoader) : base(new SpellParserOptions())
        {
            BuildLoader = buildLoader;
            Options.PlayerLevel = BuildLoader.MaxLevel;
        }

        public DiffSpellParser(BuildLoader buildLoader, SpellParserOptions options) : base(options)
        {
            BuildLoader = buildLoader;
        }

        public override void HandleException(Exception e) => _logger.Error(e);

        public override IEnumerable<IParserChrSpecialization> GetAllChrSpecializationsByClass(Class spellClass) => BuildLoader.Relationships.GetChrSpecializationsByClass(spellClass);
        public override IParserChrSpecialization GetChrSpecialization(int specializationID) => BuildLoader.Db2.ChrSpecialization.Get(specializationID);
        public override IParserGarrAbility GetGarrisonAbility(int abilityID) => BuildLoader.Db2.GarrAbility.Get(abilityID);
        public override IParserGarrAbilityEffect GetGarrisonAbilityEffect(int effectID) => BuildLoader.Db2.GarrAbilityEffect.Get(effectID);
        public override IParserGarrBuilding GetGarrisonBuilding(int buildingID) => BuildLoader.Db2.GarrBuilding.Get(buildingID);
        public override IParserGarrBuilding GetGarrisonBuildingByNameAndLevel(string name, int level) => BuildLoader.Db2.GarrBuilding.All.Where(x => x.Name == name && x.UpgradeLevel == level).FirstOrDefault();
        public override IParserSpell GetSpell(int spellID) => BuildLoader.Db2.Spell.Get(spellID);
        public override IParserSpell GetSpellByExternalID(int spellID) => GetSpell(spellID);
        public override IParserSpellEffect GetSpellEffect(IParserSpell spell, int effectID) => (spell as SpellDb2).GetEffect(effectID, true);

        public override IParserSpellEffect GetFirstWeaponPercentDamageSpellEffect(IParserSpell spell)
        {
            return (spell as SpellDb2).Effects
                .OrderBy(x => x.Index)
                .ThenBy(x => x.DifficultyID)
                .FirstOrDefault(x => x.Effect == SpellEffectEffect.WeaponPercentDamage);
        }

        public override IParserSpell GetNewSpell() => new SpellDb2();

        public override int GetScaledSecondaryStatValue(int value, int itemSlot, int itemLevel)
        {
            var scale = BuildLoader.GameTable.CombatRatingsMultByILvl.Get(itemLevel);
            return (int)Math.Floor(value * scale.GetValueByInventorySlot((InventorySlot)itemSlot));
        }

        // FIXME
        public override string GetChrSpecializationIconURL(IParserChrSpecialization speicalization) => "";
        public override string GetCurrencyLink(int currencyID, string text) => text;
        public override string GetIconHtml(string iconName) => $"[{ iconName }]";
        public override string GetSpellIconHtml(IParserSpell spell) => "";
        public override string GetSpellIconURL(IParserSpell spell) => "";
        public override string RenderSpellTooltip(IParserSpell spell) => "";

        // SimpleParse variants
        public static string SimpleParse(BuildLoader buildLoader, SpellDb2 spell, string description)
        {
            var spellParser = new DiffSpellParser(buildLoader);
            return spellParser.Parse(spell, description);
        }

        public static string SimpleParse(BuildLoader buildLoader, string description) => SimpleParse(buildLoader, null, description);

        public static string SimpleParse(BuildLoader buildLoader, GarrAbilityDb2 garrAbility)
        {
            var spellParser = new DiffSpellParser(buildLoader);
            return spellParser.Parse(garrAbility);
        }
    }
}
