﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OpenWoW.Multitool.Tools.DiffBuild.DiffStuff
{
    public struct DiffResult
    {
        public int ID;
        public string Name;
        public string SubCategories;
        public DiffChangeType ChangeType;
        public DiffCategory Category;
        public DiffOutputType OutputType;
        public List<DiffFieldChange> FieldChanges;

        public void AddFieldChange(DiffFieldChange change)
        {
            if (FieldChanges == null)
            {
                FieldChanges = new List<DiffFieldChange>();
            }
            FieldChanges.Add(change);
        }
    }
}
