﻿using OpenWoW.Multitool.Entities;
using OpenWoW.Multitool.Tools.DiffBuild.Loaders;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Threading.Tasks;

namespace OpenWoW.Multitool.Tools.DiffBuild.DiffStuff
{
    internal class MagicDiffer
    {
        private readonly BuildLoader OldBuild;
        private readonly BuildLoader NewBuild;

        private static readonly NLog.Logger _logger = NLog.LogManager.GetCurrentClassLogger();

        public MagicDiffer(BuildLoader oldBuild, BuildLoader newBuild)
        {
            OldBuild = oldBuild;
            NewBuild = newBuild;
        }

        public Dictionary<DiffCategory, DiffResult[]> Run()
        {
            var results = new Dictionary<DiffCategory, DiffResult[]>();

            var sw = Stopwatch.StartNew();

            DiffDb2s(results);
            var t1 = sw.ElapsedMilliseconds;

            DiffGameTables(results);
            var t2 = sw.ElapsedMilliseconds;

            DiffAllImages(results);
            var t3 = sw.ElapsedMilliseconds;

            sw.Stop();
            _logger.Debug("Diffs completed! db2={0}ms gametable={1}ms image={2}ms", t1, t2 - t1, t3 - t2);

            return results;
        }

        #region Db2s
        private void DiffDb2s(Dictionary<DiffCategory, DiffResult[]> allResults)
        {
            var genericMethod = typeof(MagicDiffer).GetMethod("DiffDb2", BindingFlags.Static | BindingFlags.NonPublic);

            // Db2Holder properties are all `Db2Loader<T>`, we want to:
            // - find the IDiffable ones
            // - build a lambda expression to call `DiffDb2<T>` with the correct types
            var properties = typeof(Db2Holder)
                .GetProperties()
                .Where(p => typeof(IDiffable).IsAssignableFrom(p.PropertyType.GetGenericArguments()[0]))
                .OrderBy(p => p.Name);
            foreach (var property in properties)
            {
                // Create a type-specific version of the generic DiffDb2 method and execute it
                Type[] db2Types = property.PropertyType.GetGenericArguments();
                var loaderMethod = genericMethod.MakeGenericMethod(db2Types);
                var loaderType = typeof(Db2Loader<>).MakeGenericType(db2Types);

                var allResultsParam = Expression.Parameter(typeof(Dictionary<DiffCategory, DiffResult[]>));
                var oldParam = Expression.Parameter(typeof(object));
                var newParam = Expression.Parameter(typeof(object));
                var lambda = Expression.Lambda<Action<Dictionary<DiffCategory, DiffResult[]>, object, object>>(
                    Expression.Call(
                        loaderMethod,
                        allResultsParam,
                        Expression.TypeAs(oldParam, loaderType),
                        Expression.TypeAs(newParam, loaderType)
                    ),
                    allResultsParam,
                    oldParam,
                    newParam
                );

                lambda.Compile().Invoke(allResults, property.GetValue(OldBuild.Db2), property.GetValue(NewBuild.Db2));
            };
        }

        private static void DiffDb2<TDb2>(Dictionary<DiffCategory, DiffResult[]> allResults, Db2Loader<TDb2> oldLoader, Db2Loader<TDb2> newLoader)
            where TDb2 : Db2Base<TDb2>, IDb2, IDiffable, new()
        {
            var results = new ConcurrentBag<DiffResult>();
            DiffCategory category = typeof(TDb2).GetCustomAttribute<CategoryAttribute>()?.Category ?? DiffCategory.Oops;

            // Create a set of delegates/lambdas for fast access to every IDiffable property
            var diffableProperties = ReflectionHelpers.GetDiffableProperties(typeof(TDb2));
            var getFuncs = new List<(string, Func<TDb2, string>)>();
            foreach (var dp in diffableProperties)
            {
                Func<TDb2, string> getFunc = null;

                switch (dp.Property.PropertyType.Name)
                {
                    case "String":
                        getFunc = ReflectionHelpers.CreateGetDelegate<TDb2, string>(dp.Property);
                        break;

                    /*case "Int32":
                        var getInt32 = ReflectionHelpers.CreateGetDelegate<TDb2, int>(dp.Property);
                        getFunc = (obj) => getInt32(obj).ToString();
                        break;*/

                    case "Float":
                        var getFloat = ReflectionHelpers.CreateGetDelegate<TDb2, float>(dp.Property);
                        getFunc = (obj) => getFloat(obj).ToString();
                        break;

                    default:
                        //_logger.Warn("Using naive ToString() for `{0}`.`{1}`, type `{2}`", typeof(TDb2).Name, dp.Property.Name, dp.Property.PropertyType.Name);
                        var getToString = ReflectionHelpers.CreateToString<TDb2>(dp.Property);
                        getFunc = (obj) => getToString(obj);
                        break;
                }

                if (getFunc != null)
                {
                    string niceName = dp.Name.Split('.').Last();
                    getFuncs.Add((niceName, getFunc));
                }
            }

            _logger.Debug("Diffing {0} => {1}", typeof(TDb2).Name, string.Join(", ", getFuncs.Select(x => x.Item1)));

            // Categorize if required
            if (typeof(ICategorizable).IsAssignableFrom(typeof(TDb2)))
            {
                Parallel.ForEach(oldLoader.All, (instance) => ((ICategorizable)instance).Categorize());
                Parallel.ForEach(newLoader.All, (instance) => ((ICategorizable)instance).Categorize());
            }

            var oldKeys = oldLoader.Keys.OrderBy(x => x).ToArray();
            var newKeys = newLoader.Keys.OrderBy(x => x).ToArray();
            var (added, removed, inBoth) = CompareHelpers.CompareKeys(oldKeys, newKeys);

            // Now we can do the actual diffing
            //foreach (int id in added)
            Parallel.ForEach(added, (id) =>
            {
                TDb2 newRecord = newLoader.Get(id);

                if (newRecord is ICategorizable newCategorizable && !newCategorizable.ShouldDiff)
                {
                    return;
                }

                DiffResult result = new DiffResult
                {
                    Category = category,
                    ChangeType = DiffChangeType.Added,
                    ID = id,
                    Name = newRecord.Name,
                    SubCategories = newRecord.SubCategoryString,
                };

                //_logger.Info("Added {0}: {1}", id, newRecord.Name);
                foreach (var (fieldName, fieldGet) in getFuncs)
                {
                    string value = fieldGet(newRecord);
                    if (value != "" && value != "0")
                    {
                        result.AddFieldChange(new DiffFieldChange
                        {
                            Name = fieldName,
                            NewValue = value,
                        });
                        //_logger.Info("- {0} = {1}", fieldName, value);
                    }
                }

                results.Add(result);
            });

            //foreach (int id in removed)
            Parallel.ForEach(removed, (id) =>
            {
                TDb2 oldRecord = oldLoader.Get(id);

                if (oldRecord is ICategorizable oldCategorizable && !oldCategorizable.ShouldDiff)
                {
                    return;
                }

                results.Add(new DiffResult
                {
                    Category = category,
                    ChangeType = DiffChangeType.Removed,
                    ID = id,
                    Name = oldRecord.Name,
                    SubCategories = oldRecord.SubCategoryString,
                });
            });

            //foreach (int id in inBoth)
            Parallel.ForEach(inBoth, (id) =>
            {
                TDb2 oldRecord = oldLoader.Get(id);
                TDb2 newRecord = newLoader.Get(id);

                if (oldRecord is ICategorizable oldCategorizable && newRecord is ICategorizable newCategorizable && !oldCategorizable.ShouldDiff && !newCategorizable.ShouldDiff)
                {
                    return;
                }

                bool hasChanges = false;
                DiffResult result = new DiffResult();

                foreach (var (fieldName, fieldGet) in getFuncs)
                {
                    string oldValue = fieldGet(oldRecord);
                    string newValue = fieldGet(newRecord);
                    if (oldValue != newValue)
                    {
                        if (!hasChanges)
                        {
                            hasChanges = true;
                            result.Category = category;
                            result.ChangeType = DiffChangeType.Changed;
                            result.ID = id;
                            result.Name = newRecord.Name;
                            result.SubCategories = newRecord.SubCategoryString;
                        }

                        result.AddFieldChange(new DiffFieldChange
                        {
                            Name = fieldName,
                            OldValue = oldValue,
                            NewValue = newValue,
                        });
                    }
                }

                if (hasChanges)
                {
                    results.Add(result);
                }
            });

            if (results.Count > 0)
            {
                allResults[category] = results.ToArray();
            }
        }
        #endregion

        #region GameTables
        private void DiffGameTables(Dictionary<DiffCategory, DiffResult[]> allResults)
        {
            var genericMethod = typeof(MagicDiffer).GetMethod("DiffGameTable", BindingFlags.Static | BindingFlags.NonPublic);

        }

        private static List<DiffResult> DiffGameTable<TKey, TGameTable>(GameTableLoader<TKey, TGameTable> oldLoader, GameTableLoader<TKey, TGameTable> newLoader)
            where TGameTable : IGameTable<TKey>, new()
        {
            var results = new List<DiffResult>();

            return results;
        }
        #endregion

        #region Images
        private void DiffAllImages(Dictionary<DiffCategory, DiffResult[]> allResults)
        {
            allResults[DiffCategory.Icons] = DiffImages(OldBuild.Image.Icons, NewBuild.Image.Icons, DiffCategory.Icons).ToArray();
            allResults[DiffCategory.LoadingScreens] = DiffImages(OldBuild.Image.LoadingScreens, NewBuild.Image.LoadingScreens, DiffCategory.LoadingScreens).ToArray();
        }

        private static List<DiffResult> DiffImages(ImageHashLoader oldLoader, ImageHashLoader newLoader, DiffCategory category)
        {
            var results = new List<DiffResult>();

            var oldKeys = oldLoader.FilenameToHash.Keys.OrderBy(x => x).ToArray();
            var newKeys = newLoader.FilenameToHash.Keys.OrderBy(x => x).ToArray();
            var (added, removed, inBoth) = CompareHelpers.CompareKeys(oldKeys, newKeys);

            foreach (string key in added)
            {
                string newHash = newLoader.FilenameToHash[key];

                var result = new DiffResult
                {
                    Category = category,
                    ChangeType = DiffChangeType.Added,
                    ID = -1,
                    Name = key,
                };

                result.AddFieldChange(new DiffFieldChange
                {
                    OutputType = DiffOutputType.Image,
                    NewValue = newHash,
                });

                results.Add(result);
            }

            foreach (string key in removed)
            {
                string oldHash = oldLoader.FilenameToHash[key];

                results.Add(new DiffResult
                {
                    Category = category,
                    ChangeType = DiffChangeType.Removed,
                    ID = -1,
                    Name = key,
                });
            }

            foreach (string key in inBoth)
            {
                string oldHash = oldLoader.FilenameToHash[key];
                string newHash = newLoader.FilenameToHash[key];

                if (oldHash != newHash)
                {
                    var result = new DiffResult
                    {
                        Category = category,
                        ChangeType = DiffChangeType.Changed,
                        ID = -1,
                        Name = key,
                    };

                    result.AddFieldChange(new DiffFieldChange
                    {
                        OutputType = DiffOutputType.Image,
                        OldValue = oldHash,
                        NewValue = newHash,
                    });

                    results.Add(result);
                }
            }

            return results;
        }
        #endregion
    }
}
