﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OpenWoW.Multitool.Tools.DiffBuild.DiffStuff
{
    public struct DiffFieldChange
    {
        public DiffOutputType OutputType;
        public string Name;
        public string OldValue;
        public string NewValue;
    }
}
