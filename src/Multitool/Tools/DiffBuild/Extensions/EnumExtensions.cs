﻿using EnumsNET;
using System;
using System.Collections.Generic;
using System.Text;

namespace OpenWoW.Multitool.Tools.DiffBuild
{
    public static class EnumExtensions
    {
        public static string GetName(this DiffCategory category)
        {
            return category.AsString(EnumFormat.DisplayName, EnumFormat.Name);
        }
    }
}
