﻿using OpenWoW.Common;
using OpenWoW.Database.Web.Entities;
using OpenWoW.LibWowData.DBConfig;
using OpenWoW.Multitool.Tools.DiffBuild.DiffStuff;
using OpenWoW.Multitool.Tools.DiffBuild.Loaders;
using OpenWoW.Multitool.Tools.DiffBuild.Outputters;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Text;

namespace OpenWoW.Multitool.Tools.DiffBuild
{
    public class BuildDiffer
    {
        internal readonly BuildDifferOptions Options;

        private BuildLoader OldBuildLoader;
        private BuildLoader NewBuildLoader;
        private Dictionary<DiffCategory, DiffResult[]> Results;
        private List<string> Notifications = new List<string>();

        private static readonly NLog.Logger _logger = NLog.LogManager.GetCurrentClassLogger();

        public BuildDiffer(BuildDifferOptions options)
        {
            Options = options;
        }

        public void Run()
        {
            var sw = Stopwatch.StartNew();
            _logger.Info("Diffing between {0} and {1}", Options.OldBuildPath, Options.NewBuildPath);
            SendNotification($"Diffing between { Options.OldBranch.ToString() }/{ Options.OldBuild } and { Options.NewBranch.ToString() }/{ Options.NewBuild }");

            var dbConfig = ToolConfig.DBConfig;

            // Load both builds
            long before = GC.GetTotalMemory(false);

            OldBuildLoader = new BuildLoader(dbConfig, Options.OldBuildPath, Options.OldBranch, Options.OldBuild);
            OldBuildLoader.Load();
            NewBuildLoader = new BuildLoader(dbConfig, Options.NewBuildPath, Options.NewBranch, Options.NewBuild);
            NewBuildLoader.Load();

            long after = GC.GetTotalMemory(false);
            _logger.Debug($"Memory usage: loads +{(after - before) / 1024.0 / 1024.0:0.0}MB");

            // Diff builds
            before = GC.GetTotalMemory(false);

            var differ = new MagicDiffer(OldBuildLoader, NewBuildLoader);
            Results = differ.Run();

            after = GC.GetTotalMemory(false);
            _logger.Debug($"Memory usage: diffs +{(after - before) / 1024.0 / 1024.0:0.0}MB");

            // Write output
            Directory.CreateDirectory(ToolConfig.DiffsOutputPath);

            string baseName = $"{ Options.OldBranch.ToString().ToLower() }_{ Options.OldBuild }-{ Options.NewBranch.ToString().ToLower() }_{ Options.NewBuild }";
            //GenerateOutput<HtmlOutputter>(Path.Combine(Options.OutputPath, "{ baseName }-html.html"));
            GenerateOutput<MmocOutputter>(Path.Combine(ToolConfig.DiffsOutputPath, $"{ baseName }-mmoc.txt"));
            GenerateOutput<TextOutputter>(Path.Combine(ToolConfig.DiffsOutputPath, $"{ baseName }-text.txt"));

            // Yay
            sw.Stop();
            if (Options.NotificationAction != null)
            {
                Notifications.Insert(0, $"Diff process completed in {(sw.ElapsedMilliseconds / 1000.0):0.0}s");
                SendNotification(string.Join("\n", Notifications));
            }
        }

        private void GenerateOutput<TOutputter>(string outPath) where TOutputter : OutputterBase, IOutputter
        {
            var sw = Stopwatch.StartNew();

            _logger.Info("Writing diff output to {0} using {1}", outPath, typeof(TOutputter).Name);

            using (var writer = new StreamWriter(outPath, false, Encoding.UTF8))
            {
                var parms = new object[] { writer, Results, OldBuildLoader.Branch, OldBuildLoader.Build, NewBuildLoader.Branch, NewBuildLoader.Build };
                var instance = Activator.CreateInstance(typeof(TOutputter), parms) as TOutputter;
                instance.Run();
                Notifications.Add($"- { instance.Name }: { ToolConfig.DiffsUrl }/{ Path.GetFileName(outPath) }");
            }

            sw.Stop();
            _logger.Debug($"Wrote diffs in { sw.ElapsedMilliseconds }ms");
        }

        public List<DiffTooltip> GenerateDiffTooltips(BuildData oldBuild, BuildData newBuild)
        {
            var tooltips = new List<DiffTooltip>();
            var sw = Stopwatch.StartNew();

            if (Results.TryGetValue(DiffCategory.Spells, out DiffResult[] spellDiffs))
            {
                foreach (DiffResult spellDiff in spellDiffs)
                {
                    if (spellDiff.ChangeType == DiffChangeType.Removed || spellDiff.ChangeType == DiffChangeType.Changed)
                    {
                        var oldSpell = OldBuildLoader.Db2.Spell.Get(spellDiff.ID);
                        tooltips.Add(new DiffTooltip
                        {
                            BuildId = oldBuild.Id,
                            EntityType = EntityType.Spell,
                            EntityId = spellDiff.ID,
                            JsonData = JsonUtilities.SerializeInterface<ITooltipSpell>(oldSpell),
                        });
                    }
                    if (spellDiff.ChangeType == DiffChangeType.Added || spellDiff.ChangeType == DiffChangeType.Changed)
                    {
                        var newSpell = NewBuildLoader.Db2.Spell.Get(spellDiff.ID);
                        tooltips.Add(new DiffTooltip
                        {
                            BuildId = newBuild.Id,
                            EntityType = EntityType.Spell,
                            EntityId = spellDiff.ID,
                            JsonData = JsonUtilities.SerializeInterface<ITooltipSpell>(newSpell),
                        });
                    }
                }
            }

            sw.Stop();
            _logger.Info("Generated {0} DiffTooltips in {1}ms", tooltips.Count, sw.ElapsedMilliseconds);

            return tooltips;
        }

        private void SendNotification(string message)
        {
            if (Options.NotificationAction != null)
            {
                Options.NotificationAction(message);
            }
        }
    }
}
