﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace OpenWoW.Multitool.Tools.DiffBuild.Loaders
{
    internal class ImageHolder
    {
        public ImageHashLoader Icons { get; private set; }
        public ImageHashLoader LoadingScreens { get; private set; }
        public ImageHashLoader Maps { get; private set; }

        public void Load(string basePath)
        {
            Icons = new ImageHashLoader(Path.Combine(basePath, "icons"));
            LoadingScreens = new ImageHashLoader(Path.Combine(basePath, "loadingscreens"));
            //Maps = new ImageHashLoader(Path.Combine(basePath, "maps"));
        }
    }
}
