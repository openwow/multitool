﻿using OpenWoW.Common;
using OpenWoW.LibWowData;
using OpenWoW.LibWowData.DBConfig;
using OpenWoW.Multitool.Entities;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Linq.Expressions;

namespace OpenWoW.Multitool.Tools.DiffBuild.Loaders
{
    internal class Db2Loader<TDb2> where TDb2 : Db2Base<TDb2>, IDb2, new()
    {
        private static NLog.Logger _logger = NLog.LogManager.GetCurrentClassLogger();

        private readonly string FilePath;
        private readonly string Name;
        private readonly Dictionary<int, TDb2> Storage;
        private readonly DBConfigParser DBConfig;

        private BuildLoader _buildLoader;
        private WowDataFile DataFile;
        private TableLayout Layout;
        private List<FieldInformation> FixedFieldInfo;

        private static Dictionary<FieldType, string> _fieldTypeToNumericUnionMember = new Dictionary<FieldType, string>
        {
            { FieldType.Float, "FloatVal" },
            { FieldType.Integer64, "LongVal" },
            { FieldType.Integer, "IntVal" },
            { FieldType.Integer16, "ShortVal" },
            { FieldType.Integer8, "SByteVal" },
            { FieldType.UnsignedInteger, "UIntVal" },
            { FieldType.UnsignedInteger16, "UShortVal" },
            { FieldType.UnsignedInteger8, "ByteVal" },
        };

        public Db2Loader(BuildLoader buildLoader)
        {
            var filenameAttribute = (FilenameAttribute)Attribute.GetCustomAttribute(typeof(TDb2), typeof(FilenameAttribute));
            if (filenameAttribute != null)
            {
                Name = filenameAttribute.Filename;
            }
            else
            {
                string typeName = typeof(TDb2).Name;
                Name = typeName.Substring(0, typeName.Length - "Db2".Length); // FooThingDb2 -> FooThing
            }

            FilePath = FileUtilities.GetFilenameCaseInsensitive(buildLoader.BasePath, "DBFilesClient", $"{Name}.db2");
            Storage = new Dictionary<int, TDb2>();
            DBConfig = buildLoader.DBConfig;
            _buildLoader = buildLoader;

            //_logger.Debug("Created <{0}> -> {1}", typeof(TDb2).Name, FilePath);
        }

        public Dictionary<int, TDb2>.KeyCollection Keys => Storage.Keys;
        public Dictionary<int, TDb2>.ValueCollection All => Storage.Values;

        public TDb2 Get(int id)
        {
            Storage.TryGetValue(id, out TDb2 ret);
            return ret;
        }

        public IEnumerable<TDb2> GetAll(IEnumerable<int> ids)
        {
            return ids.Select(x => Get(x)).Where(x => x != null);
        }

        public void Load()
        {
            var bytes = File.ReadAllBytes(FilePath);

            // Load the header so that we have the table/layout hashes
            DataFile = new WowDataFile(bytes);
            DataFile.Load(onlyLoadHeader: true);

            // Find a DBConfig entry for this file
            Layout = DBConfig.GetByTableHashAndLayoutHash(DataFile.Header.TableHashHex, DataFile.Header.LayoutHashHex);
            if (Layout == null)
            {
                throw new Exception($"No layout found for tableHash={DataFile.Header.TableHashHex} layoutHash={DataFile.Header.LayoutHashHex}");
            }

            //_logger.Info("Loading {0} (0x{1}) as ID={2} Fields={3}", Name, DataFile.Header.LayoutHashHex, Layout.IdColumn, String.Join(", ", Layout.ExpandedFieldTypes.Select(x => x.ToString())));

            // Load the entire file now
            DataFile = new WowDataFile(bytes, FixFieldLayouts(), Layout.IdColumn);
            DataFile.Load();

            // Create objects so that we can hook up FK references via reflection
            for (int recordIndex = 0; recordIndex < DataFile.Records.Count; recordIndex++)
            {
                int id = unchecked((int)DataFile.Records[recordIndex].ID);
                Storage.Add(id, new TDb2 { ID = id });
            }
        }

        private List<FieldType> FixFieldLayouts()
        {
            var ret = new List<FieldType>();
            FixedFieldInfo = new List<FieldInformation>();
            for (int fieldIndex = 0, headerIndex = 0; fieldIndex < Layout.Fields.Count; fieldIndex++, headerIndex++)
            {
                var fieldInfo = Layout.Fields[fieldIndex];
                var headerInfo = DataFile.Header.Fields[headerIndex];

                // Fun edge case: changing the array size doesn't lead to a new layout hash, so array sizing can just be wrong
                if (headerInfo.ArraySize > 0)
                {
                    if (fieldInfo.ArraySize != headerInfo.ArraySize)
                    {
                        //Console.WriteLine($"??? {Name} {fieldIndex} fieldInfo={fieldInfo.ArraySize} headerInfo={headerInfo.ArraySize}");
                        fieldInfo.ArraySize = headerInfo.ArraySize;
                    }
                    headerIndex += (headerInfo.ArraySize - 1);
                }

                FixedFieldInfo.Add(fieldInfo);

                for (int arrayIndex = 0; arrayIndex < fieldInfo.ArraySize; arrayIndex++)
                {
                    ret.Add(fieldInfo.Type);
                }
            }
            return ret;
        }

        public void Reflect()
        {
            // Create an Action to reflect a record, then use it on all records
            var reflectRecord = CreateReflectRecordAction();
            for (int recordIndex = 0; recordIndex < DataFile.Records.Count; recordIndex++)
            {
                Record record = DataFile.Records[recordIndex];
                TDb2 instance = Storage[unchecked((int)record.ID)];

                reflectRecord(instance, record);
            }

            // Cleanup
            _buildLoader = null;
            DataFile = null;
        }

#pragma warning disable HAA0101 // Array allocation for params parameter
#pragma warning disable HAA0601 // Value type to reference type conversion causing boxing allocation
        /// <summary>
        /// Create a compiled lambda to assign an entire Record to a TDb2 instance. Here there be dragons.
        /// </summary>
        /// <returns></returns>
        private Action<TDb2, Record> CreateReflectRecordAction()
        {
            var getStringMethod = typeof(WowDataFile).GetMethod("GetString", new[] { typeof(NumericUnion) });
            var addReflectIdMethod = typeof(Dictionary<(Db2Base<TDb2>, string), int>).GetMethod("Add", new[] { typeof((Db2Base<TDb2>, string)), typeof(int) });
            var tupleConstructor = typeof(ValueTuple<Db2Base<TDb2>, string>).GetConstructor(new[] { typeof(Db2Base<TDb2>), typeof(string) });

            var bodyStatements = new List<Expression>();

            var argTDb2 = Expression.Parameter(typeof(TDb2));
            var argRecord = Expression.Parameter(typeof(Record));
            var argRecordFields = Expression.Property(argRecord, "Fields");

            var constDataFile = Expression.Constant(DataFile);
            var constBuildLoaderDb2 = Expression.Constant(_buildLoader.Db2);

            int fieldIndex = -1;
            foreach (var fieldInfo in FixedFieldInfo)
            {
                fieldIndex++;

                string fieldName = fieldInfo.Name.Replace(" ", "").Replace("_", "");
                if (string.IsNullOrWhiteSpace(fieldName) || fieldName.Equals("ID", StringComparison.InvariantCultureIgnoreCase))
                {
                    continue;
                }

                var propInfo = typeof(TDb2).GetProperty(fieldName);
                if (propInfo == null && fieldName.EndsWith("ID"))
                {
                    fieldName = fieldName.Substring(0, fieldName.Length - 2);
                    propInfo = typeof(TDb2).GetProperty(fieldName);
                }
                if (propInfo == null)
                {
                    fieldIndex += (fieldInfo.ArraySize - 1);
                    _logger.Warn("No matching field for {0}.{1}", Name, fieldName);
                    continue;
                }

                // Array property, fun
                if (propInfo.PropertyType.IsArray)
                {
                    // Build a set of expressions to initialize the array with member access
                    Type elementType = propInfo.PropertyType.GetElementType();
                    string loaderPropertyName = elementType.Name;
                    if (loaderPropertyName.EndsWith("Db2"))
                    {
                        loaderPropertyName = loaderPropertyName.Substring(0, loaderPropertyName.Length - 3);
                    }

                    var arrayParts = new Expression[fieldInfo.ArraySize];
                    for (int arrayIndex = fieldIndex; arrayIndex < (fieldIndex + fieldInfo.ArraySize); arrayIndex++)
                    {
                        int capturedIndex = arrayIndex;
                        var arrayAccess = Expression.ArrayAccess(argRecordFields, Expression.Constant(capturedIndex));
                        Expression getExpression;

                        if (typeof(IDb2).IsAssignableFrom(elementType))
                        {
                            // Reference to another *Db2 class, ew. Use the correct BuildLoader.Db2.[type].Get() method
                            getExpression = Expression.Call(
                                Expression.Property(constBuildLoaderDb2, loaderPropertyName),
                                "Get",
                                null,
                                Expression.Convert(
                                    Expression.Field(arrayAccess, _fieldTypeToNumericUnionMember[fieldInfo.Type]),
                                    typeof(int)
                                )
                            );
                        }
                        else if (elementType.Name == "String")
                        {
                            // Call DataFile.GetString(field)
                            getExpression = Expression.Call(constDataFile, getStringMethod, arrayAccess);
                        }
                        else
                        {
                            // Use field.[member]
                            getExpression = Expression.Convert(
                                Expression.Field(arrayAccess, _fieldTypeToNumericUnionMember[fieldInfo.Type]),
                                elementType
                            );
                        }
                        arrayParts[arrayIndex - fieldIndex] = getExpression;
                    }

                    // Assign the newly created array to the property
                    bodyStatements.Add(
                        Expression.Assign(
                            Expression.Property(argTDb2, propInfo.Name),
                            Expression.NewArrayInit(elementType, arrayParts)
                        )
                    );

                    fieldIndex += (fieldInfo.ArraySize - 1);
                }
                // Normal property
                else
                {
                    int capturedIndex = fieldIndex;
                    var arrayAccess = Expression.ArrayAccess(argRecordFields, Expression.Constant(capturedIndex));
                    Expression getExpression;

                    if (typeof(IDb2).IsAssignableFrom(propInfo.PropertyType))
                    {
                        // Reference to another *Db2 class, ew. Use the correct BuildLoader.Db2.[type].Get() method
                        string propTypeName = propInfo.PropertyType.Name;
                        getExpression = Expression.Call(
                            Expression.Property(constBuildLoaderDb2, propTypeName.Substring(0, propTypeName.Length - 3)),
                            "Get",
                            null,
                            Expression.Convert(
                                Expression.Field(arrayAccess, _fieldTypeToNumericUnionMember[fieldInfo.Type]),
                                typeof(int)
                            )
                        );
                        // And we need to set a key in ReflectIDs
                        // TODO: this for reference arrays
                        bodyStatements.Add(
                            Expression.Call(
                                Expression.Field(null, typeof(TDb2), "ReflectIDs"),
                                addReflectIdMethod,
                                Expression.New(
                                    tupleConstructor,
                                    argTDb2,
                                    Expression.Constant(fieldName)
                                ),
                                Expression.Convert(
                                    Expression.Field(arrayAccess, _fieldTypeToNumericUnionMember[fieldInfo.Type]),
                                    typeof(int)
                                )
                            )
                        );
                    }
                    else if (propInfo.PropertyType.Name == "String")
                    {
                        // Call DataFile.GetString(field)
                        getExpression = Expression.Call(constDataFile, getStringMethod, arrayAccess);
                    }
                    else
                    {
                        // Use field.[member]
                        getExpression = Expression.Convert(
                            Expression.Field(arrayAccess, _fieldTypeToNumericUnionMember[fieldInfo.Type]),
                            propInfo.PropertyType
                        );
                    }

                    // Assign the value to the property
                    bodyStatements.Add(
                        Expression.Assign(
                            Expression.Property(argTDb2, propInfo.Name),
                            getExpression
                        )
                    );
                }
            }

            var lambda = Expression.Lambda<Action<TDb2, Record>>(Expression.Block(bodyStatements), argTDb2, argRecord);
            return lambda.Compile();
        }
#pragma warning restore HAA0101 // Array allocation for params parameter
#pragma warning restore HAA0601 // Value type to reference type conversion causing boxing allocation
    }
}
