﻿using OpenWoW.Common;
using OpenWoW.LibWowData.DBConfig;
using OpenWoW.Multitool.Entities;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;

namespace OpenWoW.Multitool.Tools.DiffBuild.Loaders
{
    internal class BuildLoader
    {
        public readonly WowBranch Branch;
        public readonly int Build;
        public readonly int MaxLevel = 120; // FIXME set this somewhere :|
        public readonly string BasePath;

        public readonly Db2Holder Db2 = new Db2Holder();
        public readonly GameTableHolder GameTable = new GameTableHolder();
        public readonly ImageHolder Image = new ImageHolder();
        public readonly RelationshipHolder Relationships = new RelationshipHolder();

        public readonly DBConfigParser DBConfig;

        private readonly ConcurrentQueue<string> _errors = new ConcurrentQueue<string>();

        private static readonly NLog.Logger _logger = NLog.LogManager.GetCurrentClassLogger();
        private static readonly HashSet<int> _testMisc1 = new HashSet<int> {
            (int)SpellAuraModifierMisc1.Effect1Value,
            (int)SpellAuraModifierMisc1.Effect2Value,
            (int)SpellAuraModifierMisc1.Effect3Value,
        };

        public BuildLoader(DBConfigParser dbConfig, string basePath, WowBranch branch, int build)
        {
            DBConfig = dbConfig;
            BasePath = basePath;
            Branch = branch;
            Build = build;
        }

        public void Load()
        {
            long before = GC.GetTotalMemory(false);

            LoadDb2s();
            LoadGameTables();
            LoadImageHashes();

            foreach (var error in _errors)
            {
                _logger.Error(error);
            }
            _errors.Clear();

            DoRelationships();

            long after = GC.GetTotalMemory(false);
            _logger.Debug($"Memory usage: before={(before / 1024.0 / 1024.0).ToString()}MB after={(after / 1024.0 / 1024.0).ToString()}MB");
        }

        /// <summary>
        /// Create a Db2Loader<T> for each T in the Db2 instance
        /// </summary>
        private void LoadDb2s()
        {
            var createInstanceArgs = new object[] { this };

            var reflected = new List<ReflectedDb2>();
            var propertyInfos = typeof(Db2Holder)
                .GetProperties()
                .OrderBy(p => p.Name);
            foreach (var property in propertyInfos)
            {
                // Create a Db2Loader<TDb2> instance and set it on the Db2 property
                var genericTypes = property.PropertyType.GetGenericArguments();
                var loaderType = typeof(Db2Loader<>).MakeGenericType(genericTypes);
                var instance = Activator.CreateInstance(loaderType, createInstanceArgs);

                property.SetValue(Db2, instance);

                reflected.Add(new ReflectedDb2
                {
                    Instance = instance,
                    Name = genericTypes[0].Name,
                    LoaderType = loaderType,
                });

                // Set the static Db2Base<T>.BuildLoader property to our instance
                var baseType = typeof(Db2Base<>).MakeGenericType(genericTypes);
                var baseBuildLoaderProperty = baseType.GetProperty("BuildLoader", BindingFlags.Static | BindingFlags.NonPublic);

                baseBuildLoaderProperty.SetValue(null, this);
            }

            // Load the data files and prepare for reflection
            var sw = Stopwatch.StartNew();
            Parallel.ForEach(SingleItemPartitioner.Create(reflected), (reflect) =>
            {
                try
                {
                    //_logger.Debug("[{0}] Loading {1}", Task.CurrentId, reflect.Name);
                    var methodInfo = reflect.LoaderType.GetMethod("Load");
                    methodInfo.Invoke(reflect.Instance, null);
                }
                catch (TargetInvocationException ex)
                {
                    _errors.Enqueue($"{reflect.Name} load error: {ex.InnerException}");
                }
            });
            sw.Stop();
            _logger.Debug("Load DB2s took {0}ms", sw.ElapsedMilliseconds);

            // Reflect the data onto the class instances
            sw.Restart();
            Parallel.ForEach(SingleItemPartitioner.Create(reflected), (reflect) =>
            {
                try
                {
                    var methodInfo = reflect.LoaderType.GetMethod("Reflect");
                    methodInfo.Invoke(reflect.Instance, null);
                }
                catch (TargetInvocationException ex)
                {
                    _errors.Enqueue($"{reflect.Name} reflect error: {ex.InnerException}");
                }
            });
            sw.Stop();
            _logger.Debug("Reflect DB2s took {0}ms", sw.ElapsedMilliseconds);

            /*foreach (var holidayKey in Db2.Holidays.Keys)
            {
                var holiday = Db2.Holidays.Get(holidayKey);
                _logger.Debug("Holiday {0}: names='{1}' descriptions='{2}' flags={3} looping={4} priority={5} region={6} date={7} duration={8} icon={9} reflects={10}",
                    holidayKey, 
                    holiday.HolidayName?.Name ?? "[null]", 
                    holiday.HolidayDescription?.Description ?? "[null]", 
                    holiday.Flags,
                    holiday.Looping,
                    holiday.Priority, 
                    holiday.Region, 
                    string.Join(",", holiday.Date), 
                    string.Join(",", holiday.Duration), 
                    string.Join(",", holiday.IconFileDataID),
                    string.Join(",", holiday.temp.Where(x => x.Key.Item1 == holiday).Select(x => $"{x.Key.Item2}=>{x.Value}"))
                );
            }*/
        }

        /// <summary>
        /// Create a GameTableLoader<T> for each T in the GameTable instance
        /// </summary>
        private void LoadGameTables()
        {
            var createInstanceArgs = new object[] { this };
            var sw = Stopwatch.StartNew();

            var propertyInfos = typeof(GameTableHolder)
                .GetProperties()
                .OrderBy(p => p.Name);
            foreach (var property in propertyInfos)
            {
                var genericTypes = property.PropertyType.GetGenericArguments();
                var loaderType = typeof(GameTableLoader<,>).MakeGenericType(genericTypes);
                var instance = Activator.CreateInstance(loaderType, createInstanceArgs);

                var methodInfo = loaderType.GetMethod("Load");
                methodInfo.Invoke(instance, null);

                property.SetValue(GameTable, instance);
            }

            sw.Stop();
            _logger.Debug("Load GameTables took {0}ms", sw.ElapsedMilliseconds);
        }

        private void LoadImageHashes()
        {
            var sw = Stopwatch.StartNew();

            Image.Load(BasePath);

            sw.Stop();
            _logger.Debug("Image hashes took {0}ms", sw.ElapsedMilliseconds);
        }

        /// <summary>
        /// 
        /// </summary>
        private void DoRelationships()
        {
            var sw = Stopwatch.StartNew();

            // Use the lowest difficulty for all of these, oof
            // - TODO: work out how to diff multiple difficulties of a spell
            foreach (var spellAuraOptions in Db2.SpellAuraOptions.All.Where(x => x.Spell != null).GroupBy(x => x.Spell).Select(g => g.OrderBy(x => x.DifficultyID).First()))
            {
                spellAuraOptions.Spell.AuraOptions = spellAuraOptions;
            }
            foreach (var spellCategories in Db2.SpellCategories.All.Where(x => x.Spell != null).GroupBy(x => x.Spell).Select(g => g.OrderBy(x => x.DifficultyID).First()))
            {
                spellCategories.Spell.Categories = spellCategories;
            }
            foreach (var spellCooldowns in Db2.SpellCooldowns.All.Where(x => x.Spell != null).GroupBy(x => x.Spell).Select(g => g.OrderBy(x => x.DifficultyID).First()))
            {
                spellCooldowns.Spell.Cooldowns = spellCooldowns;
            }
            foreach (var spellInterrupts in Db2.SpellInterrupts.All.Where(x => x.Spell != null).GroupBy(x => x.Spell).Select(g => g.OrderBy(x => x.DifficultyID).First()))
            {
                spellInterrupts.Spell.Interrupts = spellInterrupts;
            }
            foreach (var spellLevels in Db2.SpellLevels.All.Where(x => x.Spell != null).GroupBy(x => x.Spell).Select(g => g.OrderBy(x => x.DifficultyID).First()))
            {
                spellLevels.Spell.Levels = spellLevels;
            }
            foreach (var spellMisc in Db2.SpellMisc.All.Where(x => x.Spell != null).GroupBy(x => x.Spell).Select(g => g.OrderBy(x => x.DifficultyID).First()))
            {
                spellMisc.Spell.Misc = spellMisc;
            }
            foreach (var spellTargetRestrictions in Db2.SpellTargetRestrictions.All.Where(x => x.Spell != null).GroupBy(x => x.Spell).Select(g => g.OrderBy(x => x.DifficultyID).First()))
            {
                spellTargetRestrictions.Spell.TargetRestrictions = spellTargetRestrictions;
            }

            // Basic things that Blizzard de-duplicated
            foreach (var spellXDescriptionVariables in Db2.SpellXDescriptionVariables.All.Where(x => x.Spell != null && x.SpellDescriptionVariables != null))
            {
                spellXDescriptionVariables.Spell.DescriptionVariables = spellXDescriptionVariables.SpellDescriptionVariables;
            }
            foreach (var spellScaling in Db2.SpellScaling.All.Where(x => x.Spell != null))
            {
                spellScaling.Spell.Scaling = spellScaling;
            }

            foreach (var group in Db2.SpellPower.All.Where(x => x.Spell != null).GroupBy(x => x.Spell))
            {
                group.Key.Powers = group.ToArray();
            }

            // Spell <= SpellEffects
            // - TODO: work out how to make this less slow, if possible
            Parallel.ForEach(Db2.SpellEffect.All.Where(e => e.Spell != null).GroupBy(e => e.Spell), (group) =>
            {
                group.Key.Effects = group.OrderBy(x => x.Index).ToArray();
            });

            // Spell <= Talents
            foreach (var group in Db2.Talent.All.Where(t => t.Spell != null).GroupBy(t => t.Spell))
            {
                group.Key.Talents = group.ToArray();
            }

            // 
            foreach (var chrClasses in Db2.ChrClasses.All)
            {
                Relationships.SpellClassSetToClassID.Add(chrClasses.SpellClassSet, chrClasses.ID);
            }

            Relationships.ChrSpecializationsByClassID = Db2.ChrSpecialization.All
                .Where(x => x.Class != Class.None)
                .GroupBy(x => x.Class)
                .ToDictionary(k => (int)k.Key, v => v.ToArray());

            Relationships.FlatModifierEffectsByMisc1 = Db2.SpellEffect.All
                .Where(x => x.Aura == SpellEffectAura.AddFlatModifier && _testMisc1.Contains(x.Misc1))
                .GroupBy(x => x.Misc1)
                .ToDictionary(k => k.Key, v => v.ToArray());

            Relationships.GlobalStrings = Db2.GlobalStrings.All
                .ToDictionary(k => k.BaseTag, v => v.TagText);

            Relationships.PowerTypesByEnum = Db2.PowerType.All
                .ToDictionary(k => k.PowerTypeEnum, v => v);

            sw.Stop();
            _logger.Debug("Relationships took {0}ms", sw.ElapsedMilliseconds);
        }

        private struct ReflectedDb2
        {
            public object Instance;
            public string Name;
            public Type LoaderType;
        }
    }
}
