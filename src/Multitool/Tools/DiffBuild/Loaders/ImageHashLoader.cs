﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace OpenWoW.Multitool.Tools.DiffBuild.Loaders
{
    public class ImageHashLoader
    {
        public Dictionary<string, string> FilenameToHash = new Dictionary<string, string>(); 

        private static readonly NLog.Logger _logger = NLog.LogManager.GetCurrentClassLogger();

        public ImageHashLoader(string imagesPath)
        {
            string hashesFilename = Path.Combine(imagesPath, "_hashes.txt");
            if (!File.Exists(hashesFilename))
            {
                _logger.Error("File does not exist! {0}", hashesFilename);
                return;
            }

            // original filename:sanitized filename:hash
            foreach (string line in File.ReadAllLines(hashesFilename))
            {
                var parts = line.Split(':');
                if (parts.Length == 3)
                {
                    FilenameToHash[parts[1]] = parts[2];
                }
            }
        }
    }
}
