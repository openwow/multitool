﻿using OpenWoW.Common;
using OpenWoW.LibSpellParser;
using OpenWoW.Multitool.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace OpenWoW.Multitool.Tools.DiffBuild.Loaders
{
    internal class RelationshipHolder
    {
        internal Dictionary<int, ChrSpecializationDb2[]> ChrSpecializationsByClassID = new Dictionary<int, ChrSpecializationDb2[]>();
        internal Dictionary<int, SpellEffectDb2[]> FlatModifierEffectsByMisc1 = new Dictionary<int, SpellEffectDb2[]>();
        internal Dictionary<string, string> GlobalStrings = new Dictionary<string, string>();
        internal Dictionary<int, PowerTypeDb2> PowerTypesByEnum = new Dictionary<int, PowerTypeDb2>();
        internal Dictionary<int, Dictionary<int, ParserRelatedSpell>> RelatedSpellsBySpellID = new Dictionary<int, Dictionary<int, ParserRelatedSpell>>();
        internal Dictionary<int, SpellPowerDb2[]> SpellPowersBySpellID = new Dictionary<int, SpellPowerDb2[]>();

        internal Dictionary<int, int> SpellClassSetToClassID = new Dictionary<int, int>();

        internal Dictionary<int, HashSet<ChrSpecializationDb2>> ChrSpecializationsBySpellID = new Dictionary<int, HashSet<ChrSpecializationDb2>>();
        internal Dictionary<int, HashSet<SpellDb2>> SpellsByChrSpecializationID = new Dictionary<int, HashSet<SpellDb2>>();

        public void AddSpecializationSpell(ChrSpecializationDb2 spec, SpellDb2 spell)
        {
            // Specializations by spell
            if (!ChrSpecializationsBySpellID.TryGetValue(spell.ID, out HashSet<ChrSpecializationDb2> specializations))
            {
                specializations = ChrSpecializationsBySpellID[spell.ID] = new HashSet<ChrSpecializationDb2>();
            }
            specializations.Add(spec);

            // Spells by specialization
            if (!SpellsByChrSpecializationID.TryGetValue(spec.ID, out HashSet<SpellDb2> spells))
            {
                spells = SpellsByChrSpecializationID[spec.ID] = new HashSet<SpellDb2>();
            }
            spells.Add(spell);
        }

        public ChrSpecializationDb2[] GetChrSpecializationsByClass(Class cls) => GetChrSpecializationsByClassID((int)cls);

        public ChrSpecializationDb2[] GetChrSpecializationsByClassID(int classID)
        {
            ChrSpecializationsByClassID.TryGetValue(classID, out ChrSpecializationDb2[] ret);
            return ret ?? ChrSpecializationDb2.EmptyArray;
        }

        public HashSet<ChrSpecializationDb2> GetChrSpecializationsBySpell(SpellDb2 spell) => GetChrSpecializationsBySpellID(spell.ID);

        public HashSet<ChrSpecializationDb2> GetChrSpecializationsBySpellID(int spellID)
        {
            ChrSpecializationsBySpellID.TryGetValue(spellID, out HashSet<ChrSpecializationDb2> ret);
            return ret ?? new HashSet<ChrSpecializationDb2>();
        }

        internal PowerTypeDb2 GetPowerType(int powerType)
        {
            PowerTypesByEnum.TryGetValue(powerType, out PowerTypeDb2 ret);
            return ret;
        }

        public SpellPowerDb2[] GetSpellPowersBySpell(SpellDb2 spell) => GetSpellPowersBySpellID(spell.ID);

        public SpellPowerDb2[] GetSpellPowersBySpellID(int spellID)
        {
            SpellPowersBySpellID.TryGetValue(spellID, out SpellPowerDb2[] ret);
            return ret ?? SpellPowerDb2.EmptyArray;
        }

        // Related spells
        public void AddRelatedSpell(SpellDb2 spell, SpellDb2 relatedSpell)
        {
            if (!RelatedSpellsBySpellID.TryGetValue(spell.ID, out Dictionary<int, ParserRelatedSpell> relatedSpells))
            {
                relatedSpells = RelatedSpellsBySpellID[spell.ID] = new Dictionary<int, ParserRelatedSpell>();
            }
            if (!relatedSpells.ContainsKey(relatedSpell.ID))
            {
                relatedSpells[relatedSpell.ID] = new ParserRelatedSpell
                {
                    Spell = spell,
                    RelatedSpell = relatedSpell,
                };
            }
        }

        private static readonly ParserRelatedSpell[] _emptyParserRelatedSpellArray = new ParserRelatedSpell[] { };

        public ParserRelatedSpell[] GetRelatedSpellsBySpell(SpellDb2 spell) => GetRelatedSpellsBySpellID(spell.ID);

        public ParserRelatedSpell[] GetRelatedSpellsBySpellID(int spellID)
        {
            RelatedSpellsBySpellID.TryGetValue(spellID, out Dictionary<int, ParserRelatedSpell> relatedSpells);
            return relatedSpells?.Values.ToArray() ?? _emptyParserRelatedSpellArray;
        }

        // ChrSpecialization => Spells
        public HashSet<SpellDb2> GetSpellsByChrSpecialization(ChrSpecializationDb2 chrSpecialization) => GetSpellsByChrSpecializationID(chrSpecialization.ID);

        public HashSet<SpellDb2> GetSpellsByChrSpecializationID(int chrSpecializationID)
        {
            SpellsByChrSpecializationID.TryGetValue(chrSpecializationID, out HashSet<SpellDb2> ret);
            return ret ?? new HashSet<SpellDb2>();
        }
    }
}
