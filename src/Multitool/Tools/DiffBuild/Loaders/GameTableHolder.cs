﻿using OpenWoW.Multitool.Entities;

namespace OpenWoW.Multitool.Tools.DiffBuild.Loaders
{
    internal class GameTableHolder
    {
        public GameTableLoader<int, CombatRatingsMultByILvlGameTable> CombatRatingsMultByILvl { get; private set; }
        public GameTableLoader<int, SpellScalingGameTable> SpellScaling { get; private set; }
        public GameTableLoader<int, StaminaMultByILvlGameTable> StaminaMultByILvl { get; private set; }
    }
}
