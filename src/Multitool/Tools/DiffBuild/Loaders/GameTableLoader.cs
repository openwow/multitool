﻿using OpenWoW.Common;
using OpenWoW.Multitool.Entities;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Text.RegularExpressions;

namespace OpenWoW.Multitool.Tools.DiffBuild.Loaders
{
    internal class GameTableLoader<TKey, TGameTable> where TGameTable : IGameTable<TKey>, new()
    {
        private static NLog.Logger _logger = NLog.LogManager.GetCurrentClassLogger();

        private readonly string FilePath;
        private readonly Dictionary<TKey, TGameTable> Storage;

        public GameTableLoader(BuildLoader buildLoader)
        {
            string typeName = typeof(TGameTable).Name;
            FilePath = FileUtilities.GetFilenameCaseInsensitive(buildLoader.BasePath, "GameTables", $"{typeName.Substring(0, typeName.Length - "GameTable".Length)}.txt");
            Storage = new Dictionary<TKey, TGameTable>();
            //_logger.Debug("Created <{0}> -> {1}", typeof(TGameTable).Name, FilePath);
        }

        public TGameTable Get(TKey key)
        {
            Storage.TryGetValue(key, out TGameTable ret);
            return ret;
        }

        public void Load()
        {
            List<Action<TGameTable, string>> actions = null;

            foreach (string line in File.ReadAllLines(FilePath))
            {
                var parts = line.Split('\t');
                if (parts.Length == 0)
                {
                    continue;
                }

                // The first line requires some magic
                if (actions == null)
                {
                    actions = BuildActions(parts);
                }
                else
                {
                    var instance = new TGameTable();
                    for (int i = 0; i < parts.Length; i++)
                    {
                        actions[i](instance, parts[i]);
                    }
                    Storage.Add(instance.Key, instance);
                }
            }
        }

        private List<Action<TGameTable, string>> BuildActions(string[] parts)
        {
            var actions = new List<Action<TGameTable, string>>();
            foreach (string colName in parts)
            {
                Action<TGameTable, string> action = (obj, value) => { };
                string tryName = GetPropertyName(colName);
                var propertyInfo = typeof(TGameTable).GetProperty(tryName);
                if (propertyInfo == null)
                {
                    _logger.Warn("Property {0}.{1} doesn't exist!", typeof(TGameTable).Name, tryName);
                }
                else
                {
                    //_logger.Debug("{0} => {1}", tryName, propertyInfo.PropertyType.Name);

                    var methodInfo = propertyInfo.GetSetMethod(true);
                    switch (propertyInfo.PropertyType.Name)
                    {
                        case "Double":
                            var setDouble = (Action<TGameTable, double>)Delegate.CreateDelegate(typeof(Action<TGameTable, double>), methodInfo);
                            action = (obj, value) => setDouble(obj, double.Parse(value));
                            break;

                        case "String":
                            var setString = (Action<TGameTable, string>)Delegate.CreateDelegate(typeof(Action<TGameTable, string>), methodInfo);
                            action = (obj, value) => setString(obj, value);
                            break;

                        case "Int32":
                            var setInt32 = (Action<TGameTable, int>)Delegate.CreateDelegate(typeof(Action<TGameTable, int>), methodInfo);
                            action = (obj, value) => setInt32(obj, int.Parse(value));
                            break;

                        default:
                            _logger.Warn("No idea how to set '{0}' of type '{1}'!", tryName, propertyInfo.PropertyType.Name);
                            break;
                    }
                }

                actions.Add(action);
            }
            return actions;
        }

        private static readonly Regex[] _fixers = new Regex[]
        {
            new Regex(@"[-_\s\.]", RegexOptions.Compiled), // annoying punctuation
            new Regex(@"^\d+", RegexOptions.Compiled), // leading digits
        };
        private string GetPropertyName(string name)
        {
            string ret = name;
            foreach (var regex in _fixers)
            {
                ret = regex.Replace(ret, String.Empty);
            }
            //_logger.Debug("GetPropertyName: {0} => {1}", name, ret);
            return ret;
        }
    }
}
