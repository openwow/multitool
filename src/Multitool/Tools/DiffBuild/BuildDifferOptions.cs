﻿using OpenWoW.Common;
using System;
using System.Collections.Generic;
using System.Text;

namespace OpenWoW.Multitool.Tools.DiffBuild
{
    public class BuildDifferOptions
    {
        public string OldBuildPath;
        public string NewBuildPath;
        public WowBranch OldBranch;
        public WowBranch NewBranch;
        public int OldBuild;
        public int NewBuild;

        public Action<string> NotificationAction = null;
    }
}
