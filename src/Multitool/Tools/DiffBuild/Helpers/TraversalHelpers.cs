﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OpenWoW.Multitool.Tools.DiffBuild
{
    public static class TraversalHelpers
    {
        public static List<string> GetParentNames<TObject>(TObject current, Func<TObject, TObject> parentAccessor, Func<TObject, string> nameAccessor)
        {
            var parts = new List<string>();
            while (current != null)
            {
                string name = nameAccessor(current);
                if (name == null || parts.Contains(name))
                {
                    break;
                }
                parts.Add(name);
                current = parentAccessor(current);
            }
            parts.Reverse();
            return parts;
        }
    }
}
