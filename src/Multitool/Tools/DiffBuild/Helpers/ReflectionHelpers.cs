﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Text;

namespace OpenWoW.Multitool.Tools.DiffBuild
{
    public static class ReflectionHelpers
    {
        public static Func<TObject, TValue> CreateGetDelegate<TObject, TValue>(PropertyInfo propInfo)
        {
            return (Func<TObject, TValue>)Delegate.CreateDelegate(typeof(Func<TObject, TValue>), propInfo.GetGetMethod(true));
        }

        public static Action<TObject, TValue> CreateSetDelegate<TObject, TValue>(PropertyInfo propInfo)
        {
            return (Action<TObject, TValue>)Delegate.CreateDelegate(typeof(Action<TObject, TValue>), propInfo.GetSetMethod(true));
        }

        /// <summary>
        /// Create a fast function to call ToString() on [TObject].[propInfo]
        /// </summary>
        /// <typeparam name="TObject"></typeparam>
        /// <param name="propInfo"></param>
        /// <returns></returns>
        public static Func<TObject, string> CreateToString<TObject>(PropertyInfo propInfo)
        {
            var paramObj = Expression.Parameter(typeof(object), "obj");
            var compiledLambda = Expression.Lambda<Func<object, string>>(
                Expression.Call(
                    Expression.Property(
                        Expression.TypeAs(paramObj, typeof(TObject)),
                        propInfo.Name
                    ),
                    "ToString",
                    null
                ),
                paramObj
            ).Compile();
            return (obj) => compiledLambda(obj);
        }

        public static IEnumerable<DiffableProperty> GetDiffableProperties(this Type t)
        {
            return t
                .GetProperties(BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.Public)
                .Select(p => new
                {
                    Property = p,
                    Attribute = p.GetCustomAttribute<DiffableAttribute>(),
                })
                .Where(
                    x => x.Attribute != null
                )
                .OrderBy(x => x.Attribute.Order)
                .ThenBy(x => x.Attribute.Name ?? x.Property.Name)
                .Select(x => new DiffableProperty
                {
                    Property = x.Property,
                    Name = x.Attribute.Name ?? x.Property.Name
                });
        }

        public static MethodCallExpression GetCallExpression<T>(Expression<Func<T>> e)
        {
            return e.Body as MethodCallExpression;
        }

        public struct DiffableProperty
        {
            public PropertyInfo Property;
            public string Name;
        }
    }
}
