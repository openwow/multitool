﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;

namespace OpenWoW.Multitool.Tools.DiffBuild
{
    internal static class CompareHelpers
    {
        public static (List<T>, List<T>, List<T>) CompareKeys<T>(T[] oldKeys, T[] newKeys) where T : IComparable
        {
            var added = new List<T>();
            var removed = new List<T>();
            var inBoth = new List<T>();

            int iOld = 0, iNew = 0;
            while (iOld < oldKeys.Length && iNew < newKeys.Length)
            {
                T valueOld = oldKeys[iOld];
                T valueNew = newKeys[iNew];
                int result = valueOld.CompareTo(valueNew);
                
                if (result < 0)
                {
                    removed.Add(valueOld);
                    iOld++;
                }
                else if (result == 0)
                {
                    inBoth.Add(valueOld);
                    iOld++;
                    iNew++;
                }
                else
                {
                    added.Add(valueNew);
                    iNew++;
                }
            }
            while (iOld < oldKeys.Length)
            {
                removed.Add(oldKeys[iOld++]);
            }
            while (iNew < newKeys.Length)
            {
                added.Add(newKeys[iNew++]);
            }

            return (added, removed, inBoth);
        }
    }
}
