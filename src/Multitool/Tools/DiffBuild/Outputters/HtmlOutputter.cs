﻿using OpenWoW.Common;
using OpenWoW.Multitool.Tools.DiffBuild.DiffStuff;
using System;
using System.Collections.Generic;
using System.IO;

namespace OpenWoW.Multitool.Tools.DiffBuild.Outputters
{
    public class HtmlOutputter : OutputterBase, IOutputter
    {
        public HtmlOutputter(StreamWriter writer, Dictionary<DiffCategory, DiffResult[]> results, WowBranch oldBranch, int oldBuild, WowBranch newBranch, int newBuild)
            : base(writer, results, oldBranch, oldBuild, newBranch, newBuild)
        {
        }

        public string Name => "HTML";

        public override void StartCategory(DiffCategory category)
        {
            throw new NotImplementedException();
        }

        public override void EndCategory()
        {
            throw new NotImplementedException();
        }

        public override void StartSubCategory(string subCategory)
        {
            throw new NotImplementedException();
        }

        public override void EndSubCategory()
        {
            throw new NotImplementedException();
        }

        public override void Result(DiffResult result)
        {
            throw new NotImplementedException();
        }
    }
}
