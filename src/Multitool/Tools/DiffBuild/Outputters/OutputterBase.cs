﻿using OpenWoW.Common;
using OpenWoW.Multitool.Tools.DiffBuild.DiffStuff;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace OpenWoW.Multitool.Tools.DiffBuild.Outputters
{
    public abstract class OutputterBase
    {
        protected readonly StreamWriter Writer;
        protected readonly Dictionary<DiffCategory, DiffResult[]> Results;
        protected readonly WowBranch OldBranch;
        protected readonly WowBranch NewBranch;
        protected readonly int OldBuild;
        protected readonly int NewBuild;

        protected DiffCategory ActiveCategory;

        protected OutputterBase(StreamWriter writer, Dictionary<DiffCategory, DiffResult[]> results, WowBranch oldBranch, int oldBuild, WowBranch newBranch, int newBuild)
        {
            Writer = writer;
            Results = results;
            OldBranch = oldBranch;
            OldBuild = oldBuild;
            NewBranch = newBranch;
            NewBuild = newBuild;
        }

        public void Run()
        {
            Start();

            foreach (var categoryGroup in Results.OrderBy(r => (int)r.Key))
            {
                if (categoryGroup.Value.Length == 0)
                {
                    continue;
                }

                ActiveCategory = categoryGroup.Key;
                StartCategory(categoryGroup.Key);

                foreach (var subCategoryGroup in categoryGroup.Value.GroupBy(x => x.SubCategories).OrderBy(g => g.Key))
                {
                    if (!string.IsNullOrWhiteSpace(subCategoryGroup.Key))
                    {
                        StartSubCategory(subCategoryGroup.Key);
                    }

                    foreach (var result in subCategoryGroup.OrderBy(r => (int)r.ChangeType).ThenBy(r => r.Name))
                    {
                        Result(result);
                    }

                    if (!string.IsNullOrWhiteSpace(subCategoryGroup.Key))
                    {
                        EndSubCategory();
                    }
                }

                EndCategory();

                Writer.Flush();
            }

            End();
        }

        public virtual void Start()
        { }

        public virtual void End()
        { }

        public abstract void StartCategory(DiffCategory category);
        public abstract void EndCategory();
        public abstract void StartSubCategory(string subCategory);
        public abstract void EndSubCategory();
        public abstract void Result(DiffResult result);
    }
}
