﻿using OpenWoW.Multitool.Tools.DiffBuild.DiffStuff;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace OpenWoW.Multitool.Tools.DiffBuild.Outputters
{
    public interface IOutputter
    {
        string Name { get; }
        void Run();
        void StartCategory(DiffCategory category);
        void EndCategory();
        void StartSubCategory(string subCategory);
        void EndSubCategory();
        void Result(DiffResult result);
    }
}
