﻿using EnumsNET;
using OpenWoW.Common;
using OpenWoW.Multitool.Tools.DiffBuild.DiffStuff;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace OpenWoW.Multitool.Tools.DiffBuild.Outputters
{
    public class TextOutputter : OutputterBase, IOutputter
    {
        public string Name => "Text";

        private static readonly string[] INDENTS = new string[] { "", "  ", "    ", "      ", "        ", "          " };
        private int _indent = 0;

        public TextOutputter(StreamWriter writer, Dictionary<DiffCategory, DiffResult[]> results, WowBranch oldBranch, int oldBuild, WowBranch newBranch, int newBuild)
            : base(writer, results, oldBranch, oldBuild, newBranch, newBuild)
        { }

        private void WriteLine(string s)
        {
            Writer.WriteLine($"{ INDENTS[_indent] }{ s }");
        }

        public override void StartCategory(DiffCategory category)
        {
            WriteLine($"[{ category.AsString(EnumFormat.DisplayName, EnumFormat.Name) }]");
            _indent++;
        }

        public override void EndCategory()
        {
            Writer.WriteLine();
            _indent--;
        }

        public override void StartSubCategory(string subCategory)
        {
            Writer.WriteLine();
            WriteLine($"[{ subCategory }]");
            _indent++;
        }

        public override void EndSubCategory()
        {
            _indent--;
        }

        public override void Result(DiffResult result)
        {
            WriteLine($"{ result.ChangeType.AsString() }! { result.Name } ({ result.ID.ToString() })");
            foreach (var change in result.FieldChanges ?? Enumerable.Empty<DiffFieldChange>())
            {
                if (result.ChangeType == DiffChangeType.Changed)
                {
                    WriteLine($"- { change.Name } changed from '{ FixValue(change.OldValue) }' to '{ FixValue(change.NewValue) }'");
                }
                else
                {
                    WriteLine($"- { change.Name }: { FixValue(change.NewValue) }");
                }
            }
        }

        private string FixValue(string value)
        {
            return value.Replace("&nbsp;", " ");
        }
    }
}
