﻿using OpenWoW.Common;
using OpenWoW.Common.Extensions;
using OpenWoW.Multitool.Tools.DiffBuild.DiffStuff;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace OpenWoW.Multitool.Tools.DiffBuild.Outputters
{
    public class MmocOutputter : OutputterBase, IOutputter
    {
        public string Name => "MMOC";

        private string _activeSubCategory = null;
        private bool _isAddedImages = false;
        private Dictionary<DiffCategory, string> _categoryToUrlPath = new Dictionary<DiffCategory, string>();

        private static readonly NLog.Logger _logger = NLog.LogManager.GetCurrentClassLogger();

        public MmocOutputter(StreamWriter writer, Dictionary<DiffCategory, DiffResult[]> results, WowBranch oldBranch, int oldBuild, WowBranch newBranch, int newBuild)
            : base(writer, results, oldBranch, oldBuild, newBranch, newBuild)
        {
            foreach (DiffCategory category in Enum.GetValues(typeof(DiffCategory)))
            {
                var linkableAttribute = category.GetAttribute<LinkableAttribute>();
                _categoryToUrlPath[category] = linkableAttribute?.UrlPath;
            }
        }

        public override void Start()
        {
            Writer.WriteLine("[size=3][b]Patch - {0} Build {1}[/b][/size]", NewBranch.GetName(), NewBuild);
            Writer.WriteLine("Build {0} will be deployed to the {1} realms soon.", NewBuild, NewBranch.GetName());
            Writer.WriteLine("[size=-1](changes from {0} build {1})[/size]", OldBranch.GetName(), OldBuild);
        }

        public override void StartCategory(DiffCategory category)
        {
            //_logger.Debug("New category: {0}", category.GetName());

            Writer.WriteLine();
            Writer.WriteLine("[size=3][b]{0}[/b][/size]", category.GetName());

            if (IsImageCategory)
            {
                //Writer.Write("[center]");
            }
            else
            {
                Writer.Write("[mmoquote]");
            }
        }

        public override void EndCategory()
        {
            if (IsImageCategory)
            {
                if (_isAddedImages)
                {
                    Writer.WriteLine("[/center]");
                }
            }
            else
            {
                Writer.WriteLine("[/mmoquote]");
            }
        }

        public override void StartSubCategory(string subCategory)
        {
            //_logger.Debug("New sub-category: {0}", subCategory);

            var parts = subCategory.Split(" - ", 2);
            if (_activeSubCategory != parts[0])
            {
                _activeSubCategory = parts[0];
                Writer.WriteLine("[size=3][b][color=#FFF3A5]{0}[/color][/b][/size]", parts[0]);
            }

            if (parts.Length == 2 && !string.IsNullOrWhiteSpace(parts[1]))
            {
                Writer.WriteLine("[b]{0}[/b]", parts[1]);
            }
            Writer.WriteLine("[list]");
        }

        public override void EndSubCategory()
        {
            Writer.WriteLine("[/list]");
            Writer.WriteLine();
        }

        private Dictionary<DiffChangeType, string> _changeTypeDisplay = new Dictionary<DiffChangeType, string>
        {
            { DiffChangeType.Added, "[color=#00FF00]Added![/color]" },
            { DiffChangeType.Changed, "[color=#0088FF]Changed![/color]" },
            { DiffChangeType.Removed, "[color=#FF2222]Removed![/color]" },
        };
        public override void Result(DiffResult result)
        {
            if (IsImageCategory)
            {
                if (result.ChangeType == DiffChangeType.Added)
                {
                    if (!_isAddedImages)
                    {
                        Writer.Write("[center]");
                        _isAddedImages = true;
                    }

                    if (ActiveCategory == DiffCategory.Icons)
                    {
                        Writer.Write("<img class=\"icon-large\" src=\"{0}/icons/56/{1}.png\">", ToolConfig.MediaUrl, result.Name);
                    }
                    else if (ActiveCategory == DiffCategory.LoadingScreens)
                    {
                        Writer.Write("<a href=\"{0}/loadingscreens/{1}.jpg\" target=\"_blank\"><img src=\"{0}/loadingscreens/{1}.jpg\" height=\"450\"></a>", ToolConfig.MediaUrl, result.Name);
                    }
                    else if (ActiveCategory == DiffCategory.Maps)
                    {
                        Writer.Write("<a href=\"{0}/maps/{1}.jpg\" target=\"_blank\"><img src=\"{0}/maps/{1}.jpg\" width=\"668\" height=\"446\"></a>", ToolConfig.MediaUrl, result.Name);
                    }
                }
                else
                {
                    if (_isAddedImages)
                    {
                        Writer.WriteLine("[/center]");
                        _isAddedImages = false;
                    }

                    // TODO: show changed images if we want that
                }
            }
            else
            {
                Writer.WriteLine("[*] {0} {1}", _changeTypeDisplay[result.ChangeType], GetNameLink(result.ID, result.Name));
                if (result.ChangeType != DiffChangeType.Removed)
                {
                    foreach (var change in result.FieldChanges ?? Enumerable.Empty<DiffFieldChange>())
                    {
                        if (result.ChangeType == DiffChangeType.Changed)
                        {
                            Writer.WriteLine($"- { change.Name } changed from '{ change.OldValue }' to '{ change.NewValue }'");
                        }
                        else if (change.Name != "Name")
                        {
                            Writer.WriteLine($"- { change.Name }: { change.NewValue }");
                        }
                    }
                }
            }
        }

        private string GetNameLink(int id, string name)
        {
            if (_categoryToUrlPath[ActiveCategory] != null)
            {
                return $"<a href=\"URL{ _categoryToUrlPath[ActiveCategory] }/{ id }\">{ name }</a>";
            }
            else
            {
                return name;
            }
        }

        private bool IsImageCategory => ActiveCategory == DiffCategory.Icons || ActiveCategory == DiffCategory.LoadingScreens || ActiveCategory == DiffCategory.Maps;
    }
}
