﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;

namespace OpenWoW.Multitool.Tools
{
    [AttributeUsage(AttributeTargets.Field, AllowMultiple = true)]
    public class MatchRegexAttribute : Attribute
    {
        public Regex CompiledRegex { get; private set; }

        public MatchRegexAttribute(string regex)
        {
            if (Path.DirectorySeparatorChar != '\\')
            {
                regex = regex.Replace(@"\\", Path.DirectorySeparatorChar.ToString());
            }
            this.CompiledRegex = new Regex(regex, RegexOptions.Compiled | RegexOptions.CultureInvariant | RegexOptions.IgnoreCase);
        }
    }
}
