﻿using System.IO;
using System.Linq;

namespace OpenWoW.Multitool.Tools
{
    public class BuildExtractorOptions
    {
        public bool SkipExtracted = true;
        public string BuildPath;
        public ExtractType Extract = ExtractType.All;

        private string _directoryName;

        public string DirectoryName
        {
            get
            {
                if (_directoryName == null)
                {
                    _directoryName = Path.GetFileName(MiscUtilities.GetSanePath(BuildPath));
                }
                return _directoryName;
            }
        }
    }
}
