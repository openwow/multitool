﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OpenWoW.Multitool.Tools
{
    [Flags]
    public enum ExtractType
    {
        None = 0,

        [MatchRegex(@"^DBFilesClient\\.*?\.db2$")]
        DBFilesClient = 0x01,

        [MatchRegex(@"^GameTables\\.*?$")]
        GameTables = 0x02,

        [MatchRegex(@"^Interface\\Icons\\.*?$")]
        [MatchRegex(@"^Interface\\Glues\\CharacterCreate\\.*?$")]
        Icons = 0x04,

        [MatchRegex(@"^Interface\\Glues\\LoadingScreens\\.*?$")]
        LoadingScreens = 0x08,

        [MatchRegex(@"^Interface\\AdventureMap\\.*?$")]
        [MatchRegex(@"^Interface\\WorldMap\\.*?$")]
        Maps = 0x10,

        [MatchRegex(@"^Sound\\Music\\.*?\.mp3$")]
        Music = 0x20,

        [MatchRegex(@"^Sound\\.*?\.ogg$")]
        Sounds = 0x40,

        All = DBFilesClient | GameTables | Icons | LoadingScreens | Maps | Music | Sounds,
        DiffsAll = DBFilesClient | GameTables | Icons | LoadingScreens | Maps,
    }
}
