﻿using OpenWoW.Common;
using OpenWoW.Common.Extensions;
using OpenWoW.Common.Utilities;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;

namespace OpenWoW.Multitool.Tools
{
    public class BuildExtractor
    {
        public readonly BuildExtractorOptions Options;
        public BLTE.Scanner Scanner;

        private const int DOWNLOAD_THREADS = 8;

        private static readonly NLog.Logger _logger = NLog.LogManager.GetCurrentClassLogger();

        public BuildExtractor(BuildExtractorOptions options)
        {
            Options = options;

            BLTE.Constants.NoSoundKit = !Options.Extract.HasFlag(ExtractType.Sounds);
            BLTE.Constants.NoWriteHashlist = true;
            BLTE.Constants.ForceGC = true;
        }

        public void Run()
        {
            string previousDirectory = Directory.GetCurrentDirectory();
            Directory.SetCurrentDirectory(Options.BuildPath);

            CheckIfAlreadyExtracted();
            if (Options.Extract == ExtractType.None)
            {
                _logger.Info("Nothing to extract!");
                return;
            }

            // Redirect console output
            // (maybe don't do this unless you enjoy infinite loops, hm)
            //Console.SetOut(new ConsoleRedirect(_logger));

            GetScanner();
            LoadInstallation();
            BuildExtractionList();
            ExtractFiles();

            Directory.SetCurrentDirectory(previousDirectory);
        }

        private void CheckIfAlreadyExtracted()
        {
            if (!Options.SkipExtracted)
            {
                return;
            }

            // Check for the existence of extraction flag files
            foreach (ExtractType flag in Enum.GetValues(typeof(ExtractType)))
            {
                if (((int)flag).CountBits() == 1 && Options.Extract.HasFlag(flag))
                {
                    if (File.Exists($".extract_{ flag.ToString().ToLowerInvariant() }"))
                    {
                        _logger.Info("Skipping extract type {0}, already done", flag.ToString());
                        Options.Extract &= ~flag;
                    }
                }
            }
        }

        private void GetScanner()
        {
            Scanner = null;

            (WowBranch branch, int build) = BuildUtilities.ParseDirectory(Options.DirectoryName);

            BLTE.Constants.GameString = branch.GetGameString();

            bool buildFound = false;
            foreach (var buildMatch in branch.GetAttributes<BuildMatchAttribute>())
            {
                string prefix = buildMatch.BuildPrefix.Replace("[build]", build.ToString()).Replace("[patch]", "*");
                _logger.Debug("Trying to match using {0} => {1}", buildMatch.BuildPrefix, prefix);

                try
                {
                    Scanner = CreateScanner(prefix);
                    buildFound = true;
                }
                catch (BLTE.BuildNotFoundException)
                { }
            }

            if (!buildFound)
            {
                throw new BLTE.BuildNotFoundException("No matching builds found");
            }

            Scanner.ParseCDNInfo(Scanner.CDNsFile);
            Scanner.ParseVersionInfo(Scanner.VersionsFile, Scanner.CDNsFile);
            Scanner.ParseConfigs(Scanner.VersionsFile, Scanner.CDNsFile);
            //LoadCacheTactKeys(scanner); // TODO: load TactKey/TactKeyLookup from database
        }

        private BLTE.Scanner CreateScanner(string version)
        {
            var scanner = new BLTE.Scanner();
            scanner.ReadFromBuildFile(version);
            return scanner;
        }

        private void LoadInstallation()
        {
            Scanner.LoadInstallation();
        }

        private void BuildExtractionList()
        {
            // Gather regular expressions to use
            var allRegexes = new List<Regex>();
            foreach (ExtractType flag in Enum.GetValues(typeof(ExtractType)))
            {
                if (Options.Extract.HasFlag(flag))
                {
                    var flagRegexes = flag.GetAttributes<MatchRegexAttribute>().Select(x => x.CompiledRegex).ToArray();
                    if (flagRegexes.Length > 0)
                    {
                        _logger.Debug("Extract has flag {0}", flag);
                        allRegexes.AddRange(flagRegexes);
                    }
                }
            }

            using (var timer = GetTimer("Installation searched in {0}"))
            {
                Scanner.DownloadQueue = new Queue<string>(Scanner.FileNameToAssetId.Keys.Where(filename => allRegexes.Any(regex => regex.Match(filename).Success)));
            }
            _logger.Info("Found {0} files to extract", Scanner.DownloadQueue.Count);
        }

        private void ExtractFiles()
        {
            _logger.Info("Extracting {0} files", Scanner.DownloadQueue.Count);
            var threads = new List<Thread>();
            for (int cdnIndex = 0; cdnIndex < BLTE.Constants.AdditionalCDNs.Count; cdnIndex++)
            {
                for (int threadIndex = 0; threadIndex < DOWNLOAD_THREADS; threadIndex++)
                {
                    Thread thread = Scanner.LaunchDownloadThread(Options.BuildPath, cdnIndex, threadIndex);
                    threads.Add(thread);
                }
            }

            foreach (Thread t in threads)
            {
                t.Join();
            }

            // Create extraction flag files
            foreach (ExtractType flag in Enum.GetValues(typeof(ExtractType)))
            {
                if (((int)flag).CountBits() == 1 && Options.Extract.HasFlag(flag))
                {
                    File.Create($".extract_{ flag.ToString().ToLowerInvariant() }").Close();
                }
            }
        }

        #region Utilities
        private StopwatchTimer GetTimer(string format)
        {
            return new StopwatchTimer(_logger, format);
        }
        #endregion
    }
}
