﻿using OpenWoW.Common;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading.Tasks;

namespace OpenWoW.Multitool.Tools
{
    public class JobWorker
    {
        private static readonly NLog.Logger _logger = NLog.LogManager.GetCurrentClassLogger();

        public async Task Run()
        {
            try
            {
                var redis = ToolConfig.Redis.GetDatabase(ToolConfig.RedisDatabase);
                while (true)
                {
                    var jobData = await RedisUtilities.PopJob();
                    if (jobData != null)
                    {
                        switch (jobData["type"])
                        {
                            case "diff":
                                RunDiff(jobData["old"], jobData["new"]);
                                break;

                            default:
                                _logger.Warn("Don't know what to do about job type `{0}`!", jobData["type"]);
                                break;
                        }
                    }

                    await Task.Delay(1000);
                }
            }
            catch (Exception e)
            {
                _logger.Error(e, "JobWorker exploded");
                throw;
            }
        }

        private void RunDiff(string fromGameString, string buildString)
        {
            /*
             * gamestrings => branches
             * buildstring => (branch, build)
             * make sure that the from branch has a build and that it isn't the to build
             * create the to directory if it doesn't exist
             * diff
             */
            
            // TODO: support "live", "12345_live", "wowt" for both `old` and `new`

            WowBranch fromBranch = BuildUtilities.GetBranchFromGameString(fromGameString);
            if (fromBranch == WowBranch.None)
            {
                _logger.Error("Unable to parse gamestring `{0}`", fromGameString);
                return;
            }

            WowBranchMatch wbm = BuildUtilities.GetBranchMatchFromBuildString(buildString);
            if (wbm == null)
            {
                _logger.Error("Unable to parse buildstring `{0}`", buildString);
                return;
            }

            var branchBuilds = BuildUtilities.ParseAllDirectories(ToolConfig.ExtractPath);

            // Make sure the `from` branch has a build and that it isn't the `to` build
            if (!branchBuilds.TryGetValue(fromBranch, out List<(int, string)> fromBuilds))
            {
                _logger.Error("No existing builds found for branch `{0}`", fromBranch.ToString());
                return;
            }

            string fromPath = null;
            foreach ((int build, string path) in fromBuilds)
            {
                if (build != wbm.Build)
                {
                    fromPath = Path.Combine(ToolConfig.ExtractPath, path);
                    break;
                }
            }

            if (fromPath == null)
            {
                _logger.Error("No diffable build found for `{0}`", fromBranch.ToString());
            }

            // Create the `to` build directory if it doesn't exist
            string toPath = MiscUtilities.GetSanePath(Path.Combine(ToolConfig.ExtractPath, $"{ wbm.Build }_{ wbm.Branch.ToString().ToLowerInvariant() }"));
            if (!Directory.Exists(toPath))
            {
                Directory.CreateDirectory(toPath);
            }

            _logger.Info("Diffing between `{0}` and `{1}`", fromPath, toPath);
        }
    }
}
