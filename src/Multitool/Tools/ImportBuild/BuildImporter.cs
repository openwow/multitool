﻿using Jil;
using Microsoft.EntityFrameworkCore;
using NpgsqlTypes;
using OpenWoW.Common;
using OpenWoW.Database.Web.Entities;
using OpenWoW.LibWowData;
using OpenWoW.LibWowData.DBConfig;
using SauceControl.Blake2Fast;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OpenWoW.Multitool.BuildImport
{
    public class BuildImporter
    {
        private readonly string BasePath;
        private readonly WowLocale Locale = WowLocale.enUS; // TODO: multi-locale support
        private BuildData Build;
        private readonly DBConfigParser DBConfig;
        private readonly ConcurrentBag<FileData> AllResults = new ConcurrentBag<FileData>();

        private static readonly NLog.Logger _logger = NLog.LogManager.GetCurrentClassLogger();

        public BuildImporter(string basePath, DBConfigParser dbConfig)
        {
            BasePath = basePath;
            DBConfig = dbConfig;
        }

        public void Run()
        {
            var sw = Stopwatch.StartNew();

            (WowBranch branch, int build) = BuildUtilities.ParseDirectory(BasePath);
            Build = BuildUtilities.GetBuildDataByBranchAndBuild(branch, build);
            if (Build == null)
            {
                _logger.Error("No build found for {0}/{1}", branch, build);
                return;
            }

            string dbfcPath = FileUtilities.GetFilenameCaseInsensitive(BasePath, "DBFilesClient");
            var di = new DirectoryInfo(dbfcPath);
            var filenames = di.GetFiles("*.db2")
                .OrderByDescending(fi => fi.Length)
                .Select(fi => fi.FullName)
                .ToArray();

            Parallel.ForEach(SingleItemPartitioner.Create(filenames), (filename) => {
                try
                {
                    ProcessFile(filename);
                }
                catch (Exception e)
                {
                    _logger.Warn("oh no {0}", filename);
                    _logger.Error(e);
                    throw;
                }
            });

            sw.Stop();
            _logger.Info("Load/serialize took {0}ms", sw.ElapsedMilliseconds);
            sw.Restart();

            // This is incredibly slow on postgres, bleh
            /*using (var webDbContext = ToolConfig.WebDatabaseContext)
            {
                webDbContext.Database.SetCommandTimeout(300);
                webDbContext.Database.ExecuteSqlCommand($"UPDATE raw_metadata SET seen_count = 0, active = false WHERE locale = { (int)Locale } AND build_id = { Build.Id }");
            }

            sw.Stop();
            _logger.Info("Reset seen_count/active took {0}ms", sw.ElapsedMilliseconds);
            sw.Restart();*/

            Task.WaitAll(new Task[]
            {
                Task.Run(() => ProcessMetadata()),
                Task.Run(() => ProcessData()),
            });
        }

        private void ProcessFile(string filename)
        {
            var sw = Stopwatch.StartNew();

            var bytes = File.ReadAllBytes(filename);

            var lwd = new WowDataFile(bytes);
            lwd.Load(onlyLoadHeader: true);

            if (lwd.Header.TotalRecordCount == 0)
            {
                _logger.Warn("No records in {0}", filename);
                return;
            }

            var layout = DBConfig.GetByTableHashAndLayoutHash(lwd.Header.TableHashHex, lwd.Header.LayoutHashHex);
            if (layout == null)
            {
                _logger.Warn("No matching config for {0} TableHash={1} LayoutHash={2}", filename, lwd.Header.TableHashHex, lwd.Header.LayoutHashHex);
                return;
            }

            List<FieldType> fieldLayouts = null;
            try
            {
                fieldLayouts = layout.FixFields(lwd);
            }
            catch (ArgumentOutOfRangeException e)
            {
                _logger.Warn("what the fuck {0}", filename);
                _logger.Warn("{0} layout fields, {1} header fields", layout.Fields.Count, lwd.Header.Fields.Count);
                _logger.Error(e);
                return;
            }

            try
            {
                lwd = new WowDataFile(bytes, fieldLayouts, layout.IdColumn);
                lwd.Load();
            }
            catch (Exception e)
            {
                _logger.Debug("{0} id={1} fields={2}", filename, layout.IdColumn, string.Join(",", fieldLayouts.Select(x => x.ToString())));
                _logger.Error(e);
                return;
            }

            var msLoad = sw.ElapsedMilliseconds;
            sw.Restart();

            var fileData = new FileData
            {
                TableHash = lwd.Header.TableHash,
                LayoutHash = lwd.Header.LayoutHash,
                Records = new RecordData[lwd.Records.Count],
                Name = Path.GetFileName(filename),
            };

            int fieldCount = layout.Fields.Count - (layout.IdColumn == -1 ? 0 : 1);
            for (int recordIndex = 0; recordIndex < lwd.Records.Count; recordIndex++)
            {
                var record = lwd.Records[recordIndex];
                var recordArray = new object[fieldCount];

                int fieldIndex = 0, finalIndex = 0;
                for (int i = 0; i < layout.Fields.Count; i++)
                {
                    object field = null;

                    var fieldInfo = layout.Fields[i];
                    if (fieldInfo.Name.Equals("ID", StringComparison.InvariantCultureIgnoreCase))
                    {
                        fieldIndex++;
                        continue;
                    }

                    if (fieldInfo.ArraySize > 1)
                    {
                        var fieldArray = new object[fieldInfo.ArraySize];
                        if (fieldInfo.Type == FieldType.String)
                        {
                            for (int arrayIndex = 0; arrayIndex < fieldInfo.ArraySize; arrayIndex++)
                            {
                                fieldArray[arrayIndex] = lwd.GetString(record.Fields[fieldIndex].UIntVal);
                                fieldIndex++;
                            }
                        }
                        else
                        {
                            for (int arrayIndex = 0; arrayIndex < fieldInfo.ArraySize; arrayIndex++)
                            {
                                fieldArray[arrayIndex] = record.Fields[fieldIndex].GetValueByFieldType(fieldInfo.Type);
                                fieldIndex++;
                            }
                        }
                        field = fieldArray;
                    }
                    else
                    {
                        if (fieldInfo.Type == FieldType.String)
                        {
                            field = lwd.GetString(record.Fields[fieldIndex].UIntVal);
                        }
                        else
                        {
                            field = record.Fields[fieldIndex].GetValueByFieldType(fieldInfo.Type);
                        }
                        fieldIndex++;
                    }

                    recordArray[finalIndex] = field;
                    finalIndex++;
                }

                /*string recordJson = JsonConvert.SerializeObject(recordArray);
                Guid guid;
                using (var md5 = MD5.Create())
                {
                    byte[] hash = md5.ComputeHash(Encoding.Default.GetBytes(recordJson));
                    guid = new Guid(hash);
                }*/

                string recordJson = JSON.SerializeDynamic(recordArray);
                var hash = Blake2b.ComputeHash(16, Encoding.Default.GetBytes(recordJson));
                Guid guid = new Guid(hash);

                fileData.Records[recordIndex] = new RecordData
                {
                    Id = unchecked((int)record.ID),
                    HashGuid = guid,
                    Json = recordJson,
                };
            }

            sw.Stop();
            _logger.Debug("{0} load={1}ms json={2}ms", fileData.Name, msLoad, sw.ElapsedMilliseconds);

            AllResults.Add(fileData);
        }

        /*
    Column    |            Type             | Collation | Nullable | Default
--------------+-----------------------------+-----------+----------+---------
 row_hash     | uuid                        |           | not null |
 build_id     | integer                     |           | not null |
 row_id       | integer                     |           | not null |
 __table_hash | integer                     |           | not null |
 first_seen   | timestamp without time zone |           | not null |
 last_seen    | timestamp without time zone |           | not null |
 seen_count   | integer                     |           | not null |
 locale       | smallint                    |           | not null |
 from_cache   | boolean                     |           | not null |
 active       | boolean                     |           | not null |

 "PK_raw_metadata" PRIMARY KEY, btree (locale, build_id, __table_hash, row_id, row_hash, from_cache)
         */

        private const string METADATA_CREATE_QUERY = "CREATE TEMP TABLE temp_metadata ON COMMIT DROP AS (SELECT * FROM raw_metadata LIMIT 0)";

        private const string METADATA_COPY_QUERY = "COPY temp_metadata (locale, build_id, __table_hash, row_id, row_hash, from_cache, first_seen, last_seen, seen_count, active) FROM STDIN (FORMAT BINARY)";

        private const string METADATA_INSERT_QUERY = @"
INSERT INTO raw_metadata (locale, build_id, __table_hash, row_id, row_hash, from_cache, first_seen, last_seen, seen_count, active)
SELECT                    locale, build_id, __table_hash, row_id, row_hash, from_cache, first_seen, last_seen, seen_count, active
FROM temp_metadata
ON CONFLICT (locale, build_id, __table_hash, row_id, row_hash, from_cache)
DO UPDATE SET
    last_seen = EXCLUDED.last_seen,
    seen_count = EXCLUDED.seen_count,
    active = EXCLUDED.active
";

        private const string DATA_CREATE_QUERY = "CREATE TEMP TABLE temp_data ON COMMIT DROP AS (SELECT * FROM raw_data LIMIT 0)";

        private const string DATA_COPY_QUERY = "COPY temp_data (row_hash, json_data) FROM STDIN (FORMAT BINARY)";

        private const string DATA_INSERT_QUERY = @"
INSERT INTO raw_data (row_hash, json_data)
SELECT                row_hash, json_data
FROM temp_data
ON CONFLICT (row_hash)
DO NOTHING
";

        private void ProcessMetadata()
        {
            using (var webDbContext = ToolConfig.WebDatabaseContext)
            {
                var sw = Stopwatch.StartNew();
                var now = DateTime.Now;

                //webDbContext.ChangeTracker.AutoDetectChangesEnabled = false;
                webDbContext.Database.OpenConnection();
                webDbContext.Database.SetCommandTimeout(600);

                // Set up
                var conn = webDbContext.GetConnection();
                var transaction = conn.BeginTransaction();

                // Create temp table
                webDbContext.Database.ExecuteSqlCommand(METADATA_CREATE_QUERY);

                // Copy into temp table
                using (var importer = conn.BeginBinaryImport(METADATA_COPY_QUERY))
                {
                    foreach (var fileData in AllResults)
                    {
                        // This has to be done outside of the query or EF throws a fit, awkward
                        int tableHash = unchecked((int)fileData.TableHash);

                        foreach (var recordData in fileData.Records)
                        {
                            importer.StartRow();
                            importer.Write((int)Locale, NpgsqlDbType.Smallint); // locale
                            importer.Write(Build.Id, NpgsqlDbType.Integer); // build_id
                            importer.Write(tableHash, NpgsqlDbType.Integer); // __table_hash
                            importer.Write(recordData.Id, NpgsqlDbType.Integer); // row_id
                            importer.Write(recordData.HashGuid, NpgsqlDbType.Uuid); // row_hash
                            importer.Write(false, NpgsqlDbType.Boolean); // from_cache
                            importer.Write(now, NpgsqlDbType.Timestamp); // first_seen
                            importer.Write(now, NpgsqlDbType.Timestamp); // last_seen
                            importer.Write(100, NpgsqlDbType.Integer); // seen_count
                            importer.Write(true, NpgsqlDbType.Boolean); // active
                        }
                    }
                    importer.Complete();
                }

                sw.Stop();
                var msTemp = sw.ElapsedMilliseconds;
                sw.Restart();

                // Insert into main table
                webDbContext.Database.ExecuteSqlCommand(METADATA_INSERT_QUERY);

                transaction.Commit();

                sw.Stop();
                var msInsert = sw.ElapsedMilliseconds;
                sw.Restart();

                // Cleanup
                webDbContext.Database.ExecuteSqlCommand("ANALYZE raw_metadata");

                sw.Stop();
                var msCleanup = sw.ElapsedMilliseconds;

                _logger.Debug("ProcessMetadata temp={0}ms insert={1}ms cleanup={2}ms", msTemp, msInsert, msCleanup);
            }
        }

        private void ProcessData()
        {
            using (var webDbContext = ToolConfig.WebDatabaseContext)
            {
                var sw = Stopwatch.StartNew();
                var now = DateTime.Now;

                //webDbContext.ChangeTracker.AutoDetectChangesEnabled = false;
                webDbContext.Database.OpenConnection();
                webDbContext.Database.SetCommandTimeout(600);

                // Set up
                var conn = webDbContext.GetConnection();
                var transaction = conn.BeginTransaction();

                // Create temp table
                webDbContext.Database.ExecuteSqlCommand(DATA_CREATE_QUERY);

                // Copy into temp table
                var seenHashes = new HashSet<Guid>();
                using (var importer = conn.BeginBinaryImport(DATA_COPY_QUERY))
                {
                    foreach (var fileData in AllResults)
                    {
                        // This has to be done outside of the query or EF throws a fit, awkward
                        //int tableHash = unchecked((int)fileData.TableHash);

                        foreach (var recordData in fileData.Records)
                        {
                            if (seenHashes.Contains(recordData.HashGuid))
                            {
                                continue;
                            }
                            seenHashes.Add(recordData.HashGuid);

                            importer.StartRow();
                            importer.Write(recordData.HashGuid, NpgsqlDbType.Uuid); // row_hash
                            importer.Write(recordData.Json, NpgsqlDbType.Text); // json_data
                        }
                    }
                    importer.Complete();
                }

                sw.Stop();
                var msTemp = sw.ElapsedMilliseconds;
                sw.Restart();

                // Insert into main table
                webDbContext.Database.ExecuteSqlCommand(DATA_INSERT_QUERY);

                transaction.Commit();

                sw.Stop();
                var msInsert = sw.ElapsedMilliseconds;
                sw.Restart();

                // Cleanup
                webDbContext.Database.ExecuteSqlCommand("ANALYZE raw_data");

                sw.Stop();
                var msCleanup = sw.ElapsedMilliseconds;

                _logger.Debug("ProcessData temp={0}ms insert={1}ms cleanup={2}ms", msTemp, msInsert, msCleanup);
            }
        }

        public struct FileData
        {
            public uint TableHash;
            public uint LayoutHash;
            public RecordData[] Records;
            public string Name;
        }

        public struct RecordData
        {
            public int Id;
            public Guid HashGuid;
            public string Json;
        }
    }
}
