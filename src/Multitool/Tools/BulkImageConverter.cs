﻿using Microsoft.IO;
using OpenWoW.Common;
using SauceControl.Blake2Fast;
using SereniaBLPLib;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace OpenWoW.Multitool.Tools
{
    public static class BulkImageConverter
    {
        private static readonly string[] IMAGE_TYPES = new string[] { "*.blp", "*.png" };

        private static readonly NLog.Logger _logger = NLog.LogManager.GetCurrentClassLogger();

        public static void Convert(string srcDirectory, string dstDirectory, ImageFormat format, bool skipExisting = false, bool generateHashes = false,
            Func<Bitmap, Bitmap> processFunc = null)
        {
            Directory.CreateDirectory(dstDirectory);

            string hashDirectory = Path.Combine(dstDirectory, "hash");
            Directory.CreateDirectory(hashDirectory);

            string nameDirectory = Path.Combine(dstDirectory, "name");
            Directory.CreateDirectory(nameDirectory);

            var existing = new HashSet<string>();
            if (skipExisting)
            {
                existing = new HashSet<string>(Directory.GetFiles(hashDirectory));
                existing.UnionWith(Directory.GetFiles(nameDirectory));
            }

            var di = new DirectoryInfo(srcDirectory);
            FileInfo[] imageFiles = IMAGE_TYPES.SelectMany(t => di.GetFiles(t, SearchOption.AllDirectories)).ToArray();
            var msManager = new RecyclableMemoryStreamManager();
            var hashes = new ConcurrentDictionary<string, (string, string)>(); // original filename => (sanitized filename, hash)

            string extension;
            ImageCodecInfo encoder;
            var parameters = new EncoderParameters(1);
            parameters.Param[0] = new EncoderParameter(Encoder.Quality, 75L);
            if (format == ImageFormat.Jpeg)
            {
                extension = ".jpg";
                encoder = ImageCodecInfo.GetImageEncoders().First(e => e.MimeType == "image/jpeg");
            }
            else if (format == ImageFormat.Png)
            {
                extension = ".png";
                encoder = ImageCodecInfo.GetImageEncoders().First(e => e.MimeType == "image/png");
                //parameters = new EncoderParameters(0);
            }
            else
            {
                throw new ArgumentException(nameof(format));
            }

            Parallel.ForEach(imageFiles, (imageFile) =>
            {
                try
                {
                    byte[] bytes = File.ReadAllBytes(imageFile.FullName);
                    string originalExtension = Path.GetExtension(imageFile.Name);
                    string originalBasename = Path.GetFileNameWithoutExtension(imageFile.Name);
                    string sanitizedBasename = FileUtilities.SanitizeFilename(originalBasename);
                    string hash = Blake2b.ComputeHash(16, new ReadOnlySpan<byte>(bytes)).ToHexString();

                    hashes.TryAdd(originalBasename, (sanitizedBasename, hash));

                    string outSanitized = sanitizedBasename + extension;
                    string outHash = hash + extension;
                    if (existing.Contains(outSanitized) && existing.Contains(outHash))
                    {
                        return;
                    }

                    Bitmap bmp;
                    using (var stream = msManager.GetStream("BulkImageConverter", bytes, 0, bytes.Length))
                    {
                        if (originalExtension.Equals(".blp", StringComparison.InvariantCultureIgnoreCase))
                        {
                            using (var blp = new BlpFile(stream))
                            {
                                bmp = blp.GetBitmap(0);
                            }
                        }
                        else if (originalExtension.Equals(".png", StringComparison.InvariantCultureIgnoreCase))
                        {
                            bmp = new Bitmap(stream);
                        }
                        else
                        {
                            throw new InvalidDataException($"{originalExtension} not supported");
                        }
                    }

                    if (processFunc != null)
                    {
                        bmp = processFunc(bmp);
                    }

                    if (!existing.Contains(outHash))
                    {
                        bmp.Save(Path.Combine(hashDirectory, outHash), encoder, parameters);
                    }
                    if (!existing.Contains(outSanitized))
                    {
                        bmp.Save(Path.Combine(nameDirectory, outSanitized), encoder, parameters);
                    }

                    bmp.Dispose();

                    //Console.WriteLine("- {0} => {1}.png {2}.png", blpFile.Name, sanitizedFilename, hash);
                }
                catch (Exception e)
                {
                    _logger.Error(e, imageFile.FullName);
                    // todo: log? save for later?
                }
            });

            if (generateHashes && hashes.Count > 0)
            {
                using (var w = File.CreateText(Path.Combine(dstDirectory, "_hashes.txt")))
                {
                    foreach (var kvp in hashes.OrderBy(kvp => kvp.Key))
                    {
                        w.WriteLine($"{ kvp.Key }:{ kvp.Value.Item1 }:{ kvp.Value.Item2 }");
                    }
                }
            }
        }

        public static void ConvertIcons(string srcDirectory, string dstDirectory, bool skipExisting = true, bool generateHashes = true)
        {
            Convert(srcDirectory, dstDirectory, ImageFormat.Png, skipExisting: skipExisting, generateHashes: generateHashes, processFunc: ProcessIcon);
        }

        public static void ConvertLoadingScreens(string srcDirectory, string dstDirectory, bool skipExisting = true, bool generateHashes = true)
        {
            // These don't need any further processing and can just be JPEG now
            Convert(srcDirectory, dstDirectory, ImageFormat.Jpeg, skipExisting: skipExisting, generateHashes: generateHashes, processFunc: ProcessLoadingScreen);
        }

        private static readonly Rectangle _iconCropRectangle = new Rectangle(4, 4, 56, 56);
        private static readonly Rectangle _iconTargetRectangle = new Rectangle(0, 0, 56, 56);
        private static readonly Rectangle _iconResizeRectangle = new Rectangle(0, 0, 64, 64);
        private static Bitmap ProcessIcon(Bitmap src)
        {
            // Resize icons to 64x64 first, some are 256x256 for whatever reason
            if (!(src.Width == 64 && src.Height == 64))
            {
                var tempImage = new Bitmap(64, 64);

                using (var graphics = Graphics.FromImage(tempImage))
                {
                    graphics.CompositingMode = CompositingMode.SourceCopy;
                    graphics.CompositingQuality = CompositingQuality.HighQuality;
                    graphics.InterpolationMode = InterpolationMode.HighQualityBicubic;
                    graphics.SmoothingMode = SmoothingMode.HighQuality;
                    graphics.PixelOffsetMode = PixelOffsetMode.HighQuality;

                    using (var wrapMode = new ImageAttributes())
                    {
                        wrapMode.SetWrapMode(WrapMode.TileFlipXY); // this fixes a weird GDI+ blending bug
                        graphics.DrawImage(src, _iconResizeRectangle, 0, 0, src.Width, src.Height, GraphicsUnit.Pixel, wrapMode);
                    }

                    src.Dispose();
                    src = tempImage;
                }
            }

            // Crop the icon, removing 4 pixels of border on each side
            Bitmap ret = new Bitmap(_iconCropRectangle.Width, _iconCropRectangle.Height);
            using (Graphics g = Graphics.FromImage(ret))
            {
                g.DrawImage(src, _iconTargetRectangle, _iconCropRectangle, GraphicsUnit.Pixel);
            }

            src.Dispose();

            return ret;
        }

        private static Dictionary<(int, int), ResizeData> _loadingScreenData = new Dictionary<(int, int), ResizeData>
        {
            { (512, 512), new ResizeData(512, 512, 1440, 1080) },
            { (1024, 1024), new ResizeData(1024, 1024, 1440, 1080) },
            { (2048, 1024), new ResizeData(2048, 1024, 1920, 1080) },
        };
        private static Bitmap ProcessLoadingScreen(Bitmap src)
        {
            var resizeData = _loadingScreenData[(src.Width, src.Height)];

            Bitmap ret = new Bitmap(resizeData.ResizedRectangle.Width, resizeData.ResizedRectangle.Height);
            using (Graphics g = Graphics.FromImage(ret))
            {
                g.CompositingMode = CompositingMode.SourceCopy;
                g.CompositingQuality = CompositingQuality.HighQuality;
                g.InterpolationMode = InterpolationMode.HighQualityBicubic;
                g.PixelOffsetMode = PixelOffsetMode.HighQuality;
                g.SmoothingMode = SmoothingMode.HighQuality;

                g.DrawImage(src, resizeData.ResizedRectangle, resizeData.SourceRectangle, GraphicsUnit.Pixel);
            }

            src.Dispose();

            return ret;
        }

        private struct ResizeData
        {
            public Rectangle SourceRectangle;
            public Rectangle ResizedRectangle;

            public ResizeData(int sourceWidth, int sourceHeight, int resizedWidth, int resizedHeight)
            {
                SourceRectangle = new Rectangle(0, 0, sourceWidth, sourceHeight);
                ResizedRectangle = new Rectangle(0, 0, resizedWidth, resizedHeight);
            }
        }
    }

    public struct ConversionResults
    {
        public int TotalCount;
        public int SuccessCount;
        public int FailCount;
        public ConcurrentDictionary<string, string> FileHashes;
    }
}
