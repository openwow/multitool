﻿using System.IO;

namespace OpenWoW.Multitool
{
    public static class MiscUtilities
    {
        public static string GetSanePath(string path)
        {
            return Path.GetFullPath(path).TrimEnd(Path.DirectorySeparatorChar);
        }
    }
}
