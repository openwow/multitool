﻿using StackExchange.Redis;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OpenWoW.Multitool
{
    public static class RedisUtilities
    {
        private const string JOB_HASH_KEY = "jobs:{0}";
        private const string JOB_QUEUE_KEY = "job_queue";
        private const string NOTIFICATION_HASH_KEY = "notifications:{0}";
        private const string NOTIFICATION_QUEUE_KEY = "notification_queue";

        public static void CreateDiffJob(string fromGameString, string buildString)
        {
            CreateJob("diff", new Dictionary<string, string>
            {
                { "old", fromGameString },
                { "new", buildString },
            });
        }

        public static void CreatePatchNotification(string product, string patch, string configHash)
        {
            CreateNotification("patch", new Dictionary<string, string>
            {
                { "product", product },
                { "patch", patch },
                { "config_hash", configHash },
            });
        }

        private static void CreateJob(string type, Dictionary<string, string> extraData) => CreateHash(JOB_HASH_KEY, JOB_QUEUE_KEY, type, extraData);

        private static void CreateNotification(string type, Dictionary<string, string> extraData) => CreateHash(NOTIFICATION_HASH_KEY, NOTIFICATION_QUEUE_KEY, type, extraData);

        private static void CreateHash(string hashKey, string queueKey, string type, Dictionary<string, string> extraData)
        {
            var database = GetRedisDatabase();

            var guid = Guid.NewGuid().ToString("N");
            var key = string.Format(hashKey, guid);
            var hashData = new List<HashEntry>
            {
                new HashEntry("type", type),
            };
            if (extraData != null)
            {
                hashData.AddRange(extraData.Select(kvp => new HashEntry(kvp.Key, kvp.Value)));
            }
            database.HashSet(key, hashData.ToArray());
            database.ListRightPush(queueKey, guid);
        }

        public static async Task<Dictionary<string, string>> PopJob()
        {
            var redis = GetRedisDatabase();

            var key = await redis.ListLeftPopAsync(JOB_QUEUE_KEY);
            if (key.HasValue)
            {
                var hashKey = string.Format(JOB_HASH_KEY, key);
                var data = await redis.HashGetAllAsync(hashKey);
                if (data.Length > 0)
                {
                    // FIXME: delete hash data
                    return data.ToDictionary(k => k.Name.ToString(), v => v.Value.ToString());
                }
            }

            return null;
        }

        private static IDatabase GetRedisDatabase()
        {
            return ToolConfig.Redis.GetDatabase(ToolConfig.RedisDatabase);
        }
    }
}
