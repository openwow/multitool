﻿using Microsoft.EntityFrameworkCore;
using OpenWoW.Common;
using OpenWoW.Common.Extensions;
using OpenWoW.Database.Web.Contexts;
using OpenWoW.Database.Web.Entities;
using OpenWoW.LibWowData;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;

namespace OpenWoW.Multitool
{
    public static class BuildUtilities
    {
        private static readonly NLog.Logger _logger = NLog.LogManager.GetCurrentClassLogger();

        // Maps the trailing string in a build directory name to a specific branch
        private static readonly Regex _buildRegex = new Regex(@"^(\d{5})_([a-z]+)$", RegexOptions.Compiled | RegexOptions.CultureInvariant | RegexOptions.IgnoreCase);

        public static (WowBranch, int) ParseDirectory(string directory)
        {
            string lastPart = Path.GetFileName(directory.TrimEnd(Path.DirectorySeparatorChar));

            var m = _buildRegex.Match(lastPart);
            if (!m.Success)
            {
                throw new BLTE.BuildNotFoundException($"Unable to match build and branch from '{lastPart}'");
            }

            int build = int.Parse(m.Groups[1].ToString());
            string branchType = m.Groups[2].ToString();
            if (!Enum.TryParse(branchType, true, out WowBranch branch))
            {
                throw new BLTE.BuildNotFoundException($"Invalid branch '{branchType}'");
            }

            return (branch, build);
        }

        public static Dictionary<WowBranch, List<(int, string)>> ParseAllDirectories(string baseDirectory)
        {
            var ret = new Dictionary<WowBranch, List<(int, string)>>();

            var baseDi = new DirectoryInfo(baseDirectory);
            foreach (var di in baseDi.GetDirectories().OrderByDescending(d => d.Name))
            {
                try
                {
                    (WowBranch branch, int build) = ParseDirectory(di.FullName);
                    if (!ret.TryGetValue(branch, out List<(int, string)> builds))
                    {
                        builds = ret[branch] = new List<(int, string)>();
                    }
                    builds.Add((build, di.Name));
                }
                catch (BLTE.BuildNotFoundException)
                {
                }
            }

            return ret;
        }

        public static BuildData GetBuildDataByBranchAndBuild(WowBranch branch, int build)
        {
            using (var webDbContext = ToolConfig.WebDatabaseContext)
            {
                return webDbContext.BuildData.SingleOrDefault(bd => bd.Branch == branch && bd.Build == build);
            }
        }

        private static Dictionary<string, WowBranch> _gameStringCache = null;
        private static List<(WowBranch, BuildMatchAttribute)> _matchAttributeCache = null;

        private static void Initialize()
        {
            if (_gameStringCache == null)
            {
                _gameStringCache = new Dictionary<string, WowBranch>();
                foreach (WowBranch branch in Enum.GetValues(typeof(WowBranch)))
                {
                    var gameStringAttribute = branch.GetAttribute<GameStringAttribute>();
                    if (gameStringAttribute != null)
                    {
                        _gameStringCache[gameStringAttribute.GameString] = branch;
                    }
                }

            }

            if (_matchAttributeCache == null)
            {
                _matchAttributeCache = new List<(WowBranch, BuildMatchAttribute)>();
                foreach (WowBranch branch in Enum.GetValues(typeof(WowBranch)))
                {
                    var buildMatchAttribute = branch.GetAttribute<BuildMatchAttribute>();
                    if (buildMatchAttribute != null)
                    {
                        _matchAttributeCache.Add((branch, buildMatchAttribute));
                    }
                }
            }
        }

        public static WowBranch GetBranchFromBuildString(string buildString)
        {
            var match = GetBranchMatchFromBuildString(buildString);
            return match?.Branch ?? WowBranch.None;
        }

        public static WowBranchMatch GetBranchMatchFromBuildString(string buildString)
        {
            Initialize();

            foreach ((var branch, var buildMatchAttribute) in _matchAttributeCache)
            {
                var match = buildMatchAttribute.RegexMatch(branch, buildString);
                if (match != null)
                {
                    return match;
                }
            }

            return null;
        }

        public static WowBranch GetBranchFromGameString(string gameString)
        {
            Initialize();

            if (!_gameStringCache.TryGetValue(gameString, out WowBranch branch))
            {
                branch = WowBranch.None;
            }
            return branch;
        }
    }
}
