﻿namespace OpenWoW.Multitool.Commands
{
    public interface ICommand
    {
        string Name { get; }
    }
}
