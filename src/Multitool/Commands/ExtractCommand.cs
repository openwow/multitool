﻿using McMaster.Extensions.CommandLineUtils;
using OpenWoW.Multitool.Tasks;
using OpenWoW.Multitool.Tools;
using System.IO;

namespace OpenWoW.Multitool.Commands
{
    public class ExtractCommand : ICommand
    {
        public string Name => "extract";

        public static void Configure(CommandLineApplication command)
        {
            command.Description = "Extract a WoW build";

            var build = command.Argument("build", "Path to build").IsRequired();

            var extractAll = command.Option<bool>("--all", "Extract everything", CommandOptionType.NoValue);
            var extractNone = command.Option<bool>("--none", "Extract nothing, weird", CommandOptionType.NoValue);

            var extractDb2s = command.Option<bool>("--db2s", "Extract DB2s", CommandOptionType.NoValue);
            var extractGameTables = command.Option<bool>("--gametables", "Extract gametables", CommandOptionType.NoValue);
            var extractIcons = command.Option<bool>("--icons", "Extract icons", CommandOptionType.NoValue);
            var extractLoadingScreens = command.Option<bool>("--loading-screens", "Extract loading screens", CommandOptionType.NoValue);
            var extractMaps = command.Option<bool>("--maps", "Extract maps", CommandOptionType.NoValue);
            var extractMusic = command.Option<bool>("--music", "Extract music", CommandOptionType.NoValue);
            var extractSounds = command.Option<bool>("--sound", "Extract sounds", CommandOptionType.NoValue);

            var forceExtract = command.Option<bool>("-f|--force-extract", "Force extraction even if it was already done", CommandOptionType.NoValue);

            command.OnExecute(() =>
            {
                var buildPath = MiscUtilities.GetSanePath(build.Value);
                if (!Directory.Exists(buildPath))
                {
                    Directory.CreateDirectory(buildPath);
                }

                ExtractType extract = ExtractType.None;

                if (!extractNone.HasValue())
                {
                    if (extractAll.HasValue())
                    {
                        extract = ExtractType.All;
                    }
                    if (extractDb2s.HasValue())
                    {
                        extract |= ExtractType.DBFilesClient;
                    }
                    if (extractGameTables.HasValue())
                    {
                        extract |= ExtractType.GameTables;
                    }
                    if (extractIcons.HasValue())
                    {
                        extract |= ExtractType.Icons;
                    }
                    if (extractLoadingScreens.HasValue())
                    {
                        extract |= ExtractType.LoadingScreens;
                    }
                    if (extractMaps.HasValue())
                    {
                        extract |= ExtractType.Maps;
                    }
                    if (extractMusic.HasValue())
                    {
                        extract |= ExtractType.Music;
                    }
                    if (extractSounds.HasValue())
                    {
                        extract |= ExtractType.Sounds;
                    }

                    if (extract == ExtractType.None)
                    {
                        extract = ExtractType.DiffsAll;
                    }
                }

                var options = new BuildExtractorOptions
                {
                    BuildPath = buildPath,
                    Extract = extract,
                    SkipExtracted = !forceExtract.HasValue(),
                };
                var task = new ExtractTask(options);
                task.RunAll();
            });
        }
    }
}
