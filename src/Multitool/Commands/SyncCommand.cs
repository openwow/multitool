﻿using McMaster.Extensions.CommandLineUtils;
using OpenWoW.Common;
using OpenWoW.Database.Web.Contexts;
using OpenWoW.Database.Web.Entities;
using OpenWoW.Multitool.Tasks;
using System;
using System.IO;
using System.Linq;

namespace OpenWoW.Multitool.Commands
{
    public class SyncCommand : ICommand
    {
        public string Name => "sync";

        private static readonly NLog.Logger _logger = NLog.LogManager.GetCurrentClassLogger();

        public static void Configure(CommandLineApplication command)
        {
            command.Description = "Sync a build";

            var buildPath = command.Argument("build", "Path to build").IsRequired();

            command.OnExecute(() =>
            {
                var task = new SyncTask(MiscUtilities.GetSanePath(buildPath.Value));
                task.RunAll();
            });
        }
    }
}
