﻿using McMaster.Extensions.CommandLineUtils;
using Microsoft.EntityFrameworkCore;
using OpenWoW.Common;
using OpenWoW.Common.Extensions;
using OpenWoW.Database.Web.Contexts;
using OpenWoW.Database.Web.Entities;
using OpenWoW.Multitool.Tasks;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;

namespace OpenWoW.Multitool.Commands
{
    public class AddBuildsCommand : ICommand
    {
        public string Name => "addbuilds";

        public static void Configure(CommandLineApplication command)
        {
            command.Description = "Add builds from BLTE to the BuildData table";

            command.OnExecute(() =>
            {
                var task = new BuildDataTask("");
                task.RunAll();
            });
        }
    }
}
