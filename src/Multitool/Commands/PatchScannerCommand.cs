﻿using McMaster.Extensions.CommandLineUtils;
using OpenWoW.BLTE;
using OpenWoW.Common;
using OpenWoW.Common.Extensions;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace OpenWoW.Multitool.Commands
{
    public class PatchScannerCommand : ICommand
    {
        public string Name => "patchscanner";

        private static readonly NLog.Logger _logger = NLog.LogManager.GetCurrentClassLogger();

        public static void Configure(CommandLineApplication command)
        {
            command.Description = "Scan for patches?";

            command.OnExecute(async () =>
            {
                var buildFiles = Directory.EnumerateFiles(Path.Combine(BLTE.Constants.BasePath, "builds"))
                    .Select(p => Path.GetFileName(p).Replace(".txt", ""))
                    .OrderByDescending(p => p);

                _logger.Info("Initializing patch scanners...");

                var scanners = new Dictionary<string, ScannerInfo>();
                foreach (WowBranch branch in Enum.GetValues(typeof(WowBranch)))
                {
                    var buildMatchAttribute = branch.GetAttribute<BuildMatchAttribute>();
                    var gameStringAttribute = branch.GetAttribute<GameStringAttribute>();
                    if (buildMatchAttribute == null || gameStringAttribute == null)
                    {
                        continue;
                    }

                    string gameString = gameStringAttribute.GameString;
                    string matchedBuild = null;

                    foreach (string buildFile in buildFiles)
                    {
                        if (buildMatchAttribute.RegexMatch(branch, buildFile) != null)
                        {
                            matchedBuild = buildFile;
                            break;
                        }
                    }

                    Scanner lastScanner = null;
                    if (matchedBuild != null)
                    {
                        try
                        {
                            lastScanner = new Scanner();
                            lastScanner.ReadFromBuildFile(matchedBuild);
                            lastScanner.ParseCDNInfo(lastScanner.CDNsFile);
                            lastScanner.ParseVersionInfo(lastScanner.VersionsFile, lastScanner.CDNsFile);
                            lastScanner.ParseConfigs(lastScanner.VersionsFile, lastScanner.CDNsFile);
                        }
                        catch (Exception e)
                        {
                            _logger.Error(e);
                        }
                    }

                    var scanner = new Scanner
                    {
                        LastVersionScanner = lastScanner
                    };

                    scanners.Add(gameString, new ScannerInfo
                    {
                        Scanner = scanner,
                        MatchedBuild = matchedBuild,
                    });
                }

                _logger.Info("Scanning for patches: {0}", string.Join(", ", scanners.Keys));
                foreach (var kvp in scanners)
                {
                    _logger.Info("{0} => {1}", kvp.Key, kvp.Value.MatchedBuild);
                }

                while (true)
                {
                    foreach (var kvp in scanners)
                    {
                        try
                        {
                            var scanner = kvp.Value.Scanner;

                            BLTE.Constants.GameString = kvp.Key;
                            if (scanner.CheckNewVersionData(false))
                            {
                                //_logger.Warn("{0}: OH SHIT", kvp.Key);
                                scanner.ParseCDNInfo(scanner.CDNsFile);
                                scanner.ParseVersionInfo(scanner.VersionsFile, scanner.CDNsFile);
                                if (scanner.ParseConfigs(scanner.VersionsFile, scanner.CDNsFile) && scanner.BuildChangedFromLast)
                                {
                                    // Useful BuildConfig entries:
                                    //   build-name    => WOW-29981patch8.1.5_Retail
                                    //   build-uid     => wow
                                    //   build-product => WoW
                                    FoundNewBuild(kvp.Key, scanner.BuildConfig["build-name"], scanner.ConfigRow["BuildConfig"].ToLowerInvariant());

                                    // Create a new canner and move the current one to last
                                    kvp.Value.Scanner = new Scanner
                                    {
                                        LastVersionScanner = scanner
                                    };
                                }
                            }
                        }
                        catch
                        { }
                    }

                    await Task.Delay(30000);
                }
            });
        }

        private static void FoundNewBuild(string gameString, string buildString, string configHash)
        {
            _logger.Warn("{0}: New build! {1}", gameString, buildString);

            RedisUtilities.CreatePatchNotification(gameString, buildString, configHash);

            if (!gameString.Contains("classic"))
            {
                // Create a job to diff this branch against the previous patch in the same branch
                RedisUtilities.CreateDiffJob(gameString, buildString);

                // Create a job to diff this branch against Live ("wow") if required
                if (gameString != "wow")
                {
                    RedisUtilities.CreateDiffJob("wow", buildString);
                }
            }
        }

        private class ScannerInfo
        {
            public Scanner Scanner;
            public string MatchedBuild;
        }
    }
}
