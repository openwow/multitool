﻿using McMaster.Extensions.CommandLineUtils;
using Microsoft.EntityFrameworkCore;
using OpenWoW.Common;
using OpenWoW.Database.Web.Contexts;
using OpenWoW.Database.Web.Entities;
using OpenWoW.Multitool.Tasks;
using OpenWoW.Multitool.Tools;
using System;
using System.Diagnostics;
using System.IO;
using System.Linq;

namespace OpenWoW.Multitool.Commands
{
    public class DiffCommand : ICommand
    {
        public string Name => "diff";

        private static readonly NLog.Logger _logger = NLog.LogManager.GetCurrentClassLogger();

        public static void Configure(CommandLineApplication command)
        {
            command.Description = "Compare two WoW builds to find any differences";

            var oldBuildArgument = command.Argument("old_build", "Path to old build").IsRequired();
            var newBuildArgument = command.Argument("new_build", "Path to new build").IsRequired();

            command.OnExecute(() =>
            {
                var oldBuildPath = MiscUtilities.GetSanePath(oldBuildArgument.Value);
                var newBuildPath = MiscUtilities.GetSanePath(newBuildArgument.Value);

                var task = new DiffTask(oldBuildPath, newBuildPath);
                task.RunAll();
            });
        }
    }
}
