﻿using McMaster.Extensions.CommandLineUtils;
using OpenWoW.Multitool.Tools;
using System;
using System.Collections.Generic;
using System.Text;

namespace OpenWoW.Multitool.Commands
{
    public class JobWorkerCommand : ICommand
    {
        public string Name => "jobworker";

        public static void Configure(CommandLineApplication command)
        {
            command.Description = "Run a worker to process jobs";

            command.OnExecute(async () =>
            {
                var worker = new JobWorker();
                await worker.Run();
            });
        }
    }
}
