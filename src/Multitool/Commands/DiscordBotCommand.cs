﻿using McMaster.Extensions.CommandLineUtils;
using OpenWoW.Discord;
using System;
using System.Collections.Generic;
using System.Text;

namespace OpenWoW.Multitool.Commands
{
    public class DiscordBotCommand : ICommand
    {
        public string Name => "discordbot";

        private static readonly NLog.Logger _logger = NLog.LogManager.GetCurrentClassLogger();

        public static void Configure(CommandLineApplication command)
        {
            command.Description = "Run the Discord bot";

            command.OnExecute(async () =>
            {
                var bot = new DiscordBot(ToolConfig.Root);
                await bot.RunAsync();
            });
        }
    }
}
