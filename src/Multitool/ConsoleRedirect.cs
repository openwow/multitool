﻿using System.IO;
using System.Text;

namespace OpenWoW.Multitool
{
    internal class ConsoleRedirect : TextWriter
    {
        private NLog.Logger _logger;

        public ConsoleRedirect(NLog.Logger logger)
        {
            _logger = logger;
        }

        public override Encoding Encoding => Encoding.UTF8;

        public override void Write(string value)
        {
            _logger.Debug(value);
        }

        public override void WriteLine(string value)
        {
            Write(value);
        }
    }
}
