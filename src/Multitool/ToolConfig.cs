﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using NLog.Extensions.Logging;
using OpenWoW.Database.Web.Contexts;
using OpenWoW.LibWowData.DBConfig;
using StackExchange.Redis;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace OpenWoW.Multitool
{
    public static class ToolConfig
    {
        public static string BasePath { get; private set; }
        public static IConfigurationRoot Root { get; private set; }

        public static readonly LoggerFactory ToolLoggerFactory = new LoggerFactory(new[] { new NLogLoggerProvider() });

        public static void Initialize()
        {
            BasePath = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData), "openwow");
            if (!Directory.Exists(BasePath))
            {
                Directory.CreateDirectory(BasePath);
            }

            Console.WriteLine($"Base config folder: { BasePath }");

            // Load Multitool config
            Root = new ConfigurationBuilder()
                .SetBasePath(BasePath)
                .AddIniFile("multitool.ini", optional: true)
                .Build();

            // Load BLTE config
            string bltePath = Path.Combine(BasePath, "blte.ini");
            if (!BLTE.Scanner.TryReadConfig(bltePath, false))
            {
                //Log.Intance.Log(LogLevel.Info, "blte.ini load failed :(");
                BLTE.Scanner.TryReadConfig(bltePath, true);
            }
        }

        public static string DbconfigPath => MiscUtilities.GetSanePath(Root.GetValue<string>("paths:dbconfig"));
        public static string DiffsOutputPath => MiscUtilities.GetSanePath(Root.GetValue<string>("paths:diff_output") ?? ".");
        public static string DiffsUrl => Root.GetValue<string>("urls:diffs").TrimEnd('/');
        public static string ExtractPath => MiscUtilities.GetSanePath(Root.GetValue<string>("paths:extraction"));
        public static string MediaPath => MiscUtilities.GetSanePath(Root.GetValue<string>("paths:media"));
        public static string MediaUrl => Root.GetValue<string>("urls:media").TrimEnd('/');
        public static string RedisConnectionString => Root.GetValue<string>("redis:connection_string");
        public static int RedisDatabase => Root.GetValue<int>("redis:database", -1);
        public static string WebDatabaseConnectionString => Root.GetValue<string>("database:web");

        private static DBConfigParser _dbconfig;
        public static DBConfigParser DBConfig
        {
            get
            {
                if (_dbconfig == null)
                {
                    _dbconfig = new DBConfigParser();
                    _dbconfig.LoadAllFromPath(DbconfigPath);
                }
                return _dbconfig;
            }
        }

        private static ConnectionMultiplexer _redis = null;
        public static ConnectionMultiplexer Redis
        {
            get
            {
                if (_redis == null)
                {
                    _redis = ConnectionMultiplexer.Connect(RedisConnectionString);
                }
                return _redis;
            }
        }

        public static WebDbContext WebDatabaseContext
        {
            get
            {
                if (Root.GetValue<bool>("debug:database"))
                {
                    return new WebDbContext(WebDatabaseConnectionString, ToolLoggerFactory);
                }
                else
                {
                    return new WebDbContext(WebDatabaseConnectionString);
                }
            }
        }
    }
}
