﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OpenWoW.Multitool
{
    [AttributeUsage(AttributeTargets.Class)]
    internal class CategoryAttribute : Attribute
    {
        public DiffCategory Category;

        public CategoryAttribute(DiffCategory category)
        {
            Category = category;
        }
    }
}
