﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OpenWoW.Multitool
{
    [AttributeUsage(AttributeTargets.Field)]
    public class LinkableAttribute : Attribute
    {
        public readonly string UrlPath;

        public LinkableAttribute(string urlPath)
        {
            UrlPath = urlPath;
        }
    }
}
