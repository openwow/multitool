﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OpenWoW.Multitool
{
    [AttributeUsage(AttributeTargets.Property)]
    internal class DiffableAttribute : Attribute
    {
        public DiffOutputType Type;
        public int Order;
        public string FormatString;
        public string Name;

        public DiffableAttribute(DiffOutputType outputType = DiffOutputType.Changed, string formatString = null, string overrideName = null, int order = int.MaxValue)
        {
            Type = outputType;
            FormatString = formatString;
            Name = overrideName;
            Order = order;
        }
    }
}
