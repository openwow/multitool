﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OpenWoW.Multitool
{
    internal class SubCategoryAttribute : Attribute
    {
        public string Name { get; set; }

        public SubCategoryAttribute(string name)
        {
            Name = name;
        }
    }
}
