﻿using System;

namespace OpenWoW.Multitool
{
    [AttributeUsage(AttributeTargets.Class)]
    public class FilenameAttribute : Attribute
    {
        public string Filename { get; private set; }

        public FilenameAttribute(string filename)
        {
            Filename = filename;
        }
    }
}
