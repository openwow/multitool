﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace OpenWoW.Multitool
{
    /// <summary>
    /// Diff categories. Order here determines order in the final diff output!
    /// </summary>
    public enum DiffCategory : byte
    {
        // Images
        Icons,

        [Display(Name = "Loading Screens")]
        LoadingScreens,

        Maps,

        // DBFilesClient
        [Linkable("/achievements")]
        Achievements,

        [Linkable("/spells")]
        Spells,

        [Linkable("/items")]
        Items,

        Miscellaneous,

        // GameTables
        [Display(Name = "Game Tables")]
        GameTables,

        // Oops
        [Display(Name = "Someone forgot to add CategoryAttribute to the IDiffable")]
        Oops,
    }
}
