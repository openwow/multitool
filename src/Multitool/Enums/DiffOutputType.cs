﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OpenWoW.Multitool
{
    public enum DiffOutputType : byte
    {
        Changed, // Field: changed from 'a' to 'b'
        IncreasedDecreased, // Field: (increased|decreased) from 3 to 4
        TextDiff, // Field: Pants on <red>head</red><green>legs</green>
        Image, // Weird magic
    }
}
