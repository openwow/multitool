﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OpenWoW.Multitool
{
    public enum DiffChangeType : byte
    {
        Added,
        Changed,
        Removed,
    }
}
