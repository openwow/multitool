﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OpenWoW.Multitool.Entities
{
    internal interface IGameTable<TKey>
    {
        TKey Key { get; }
    }
}
