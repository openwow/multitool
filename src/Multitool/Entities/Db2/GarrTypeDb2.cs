using System;
using System.Collections.Generic;
using System.Text;

namespace OpenWoW.Multitool.Entities
{
    internal class GarrTypeDb2 : Db2Base<GarrTypeDb2>, IDb2
    {
        public int ExpansionID { get; private set; }
        public int Flags { get; private set; }
        public int PrimaryCurrencyTypesID { get; private set; }
        public int SecondaryCurrencyTypesID { get; private set; }
        public int[] MapIDs { get; private set; }
    }
}
