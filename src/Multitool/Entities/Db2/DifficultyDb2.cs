using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace OpenWoW.Multitool.Entities
{
    [Category(DiffCategory.Miscellaneous)]
    [SubCategory("Difficulties")]
    internal class DifficultyDb2 : Db2Base<DifficultyDb2>, IDb2, IDiffable
    {
        public int Flags { get; private set; }
        public int GroupSizeDmgCurveID { get; private set; }
        public int GroupSizeHealthCurveID { get; private set; }
        public int GroupSizeSpellPointsCurveID { get; private set; }
        public int InstanceType { get; private set; }
        public int ItemContext { get; private set; }
        public int MaxPlayers { get; private set; }
        public int MinPlayers { get; private set; }
        public int OldEnumValue { get; private set; }
        public int OrderIndex { get; private set; }
        [Diffable(Order = 0)]
        public string Name { get; private set; }

        public DifficultyDb2 FallbackDifficulty { get; private set; }
        public DifficultyDb2 ToggleDifficulty { get; private set; }

        #region IDiffable
        public IEnumerable<string> SubCategories => Enumerable.Empty<string>();

        [Diffable]
        public string Players
        {
            get
            {
                if (MinPlayers > 0 && MaxPlayers > 0 && MinPlayers != MaxPlayers)
                {
                    return $"{ MinPlayers }-{ MaxPlayers }";
                }
                else if (MaxPlayers > 0)
                {
                    return MaxPlayers.ToString();
                }
                return null;
            }
        }
        #endregion
    }
}
