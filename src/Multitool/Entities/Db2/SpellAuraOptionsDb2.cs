using OpenWoW.LibSpellParser.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace OpenWoW.Multitool.Entities
{
    internal class SpellAuraOptionsDb2 : Db2Base<SpellAuraOptionsDb2>, IDb2, IParserSpellAuraOptions
    {
        public int CumulativeAura { get; private set; }
        public int DifficultyID { get; private set; }
        public int ProcCategoryRecovery { get; private set; }
        public int ProcChance { get; private set; }
        public int ProcCharges { get; private set; }
        public int[] ProcTypeMask { get; private set; }

        public SpellDb2 Spell { get; private set; }
        public SpellProcsPerMinuteDb2 SpellProcsPerMinute { get; private set; }

        public float ProcsPerMin => SpellProcsPerMinute?.BaseProcRate ?? 0;
    }
}
