using System;
using System.Collections.Generic;
using System.Text;

namespace OpenWoW.Multitool.Entities
{
    internal class SpellLevelsDb2 : Db2Base<SpellLevelsDb2>, IDb2
    {
        public int BaseLevel { get; private set; }
        public int DifficultyID { get; private set; }
        public int MaxLevel { get; private set; }
        public int MaxPassiveAuraLevel { get; private set; }
        public int SpellLevel { get; private set; }

        public SpellDb2 Spell { get; private set; }
    }
}
