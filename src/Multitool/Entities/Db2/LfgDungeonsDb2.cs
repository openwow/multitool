using System;
using System.Collections.Generic;
using System.Text;

namespace OpenWoW.Multitool.Entities
{
    internal class LfgDungeonsDb2 : Db2Base<LfgDungeonsDb2>, IDb2
    {
        public float RequiredItemlevel { get; private set; }
        public int BonusReputationAmount { get; private set; }
        public int CountDamage { get; private set; }
        public int CountHealer { get; private set; }
        public int CountTank { get; private set; }
        public int DifficultyID { get; private set; }
        public int ExpansionLevel { get; private set; }
        public int Faction { get; private set; }
        public int FinalJournalEncounterID { get; private set; }
        public int Flags { get; private set; }
        public int GroupID { get; private set; }
        public int IconTextureFileDataID { get; private set; }
        public int MapID { get; private set; }
        public int MaxLevel { get; private set; }
        public int MentorCharLevel { get; private set; }
        public int MentorItemLevel { get; private set; }
        public int MinCountDamage { get; private set; }
        public int MinCountHealer { get; private set; }
        public int MinCountTank { get; private set; }
        public int MinLevel { get; private set; }
        public int OrderIndex { get; private set; }
        public int PopupBgTextureFileDataID { get; private set; }
        public int RandomID { get; private set; }
        public int RequiredPlayerConditionID { get; private set; }
        public int RewardsBgTextureFileDataID { get; private set; }
        public int ScenarioID { get; private set; }
        public int SubType { get; private set; }
        public int TargetLevel { get; private set; }
        public int TargetLevelMax { get; private set; }
        public int TargetLevelMin { get; private set; }
        public int TypeID { get; private set; }
        public string Description { get; private set; }
        public string Name { get; private set; }
    }
}
