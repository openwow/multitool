using System;
using System.Collections.Generic;
using System.Text;

namespace OpenWoW.Multitool.Entities
{
    internal class ArtifactTierDb2 : Db2Base<ArtifactTierDb2>, IDb2
    {
        public int ArtifactTier { get; private set; }
        public int KnowledgePlayerConditionID { get; private set; }
        public int MaxArtifactKnowledge { get; private set; }
        public int MaxNumTraits { get; private set; }
        public int MinimumEmpowerKnowledge { get; private set; }
    }
}
