using System;
using System.Collections.Generic;
using System.Text;

namespace OpenWoW.Multitool.Entities
{
    internal class CurrencyCategoryDb2 : Db2Base<CurrencyCategoryDb2>, IDb2
    {
        public int ExpansionID { get; private set; }
        public int Flags { get; private set; }
        public string Name { get; private set; }
    }
}
