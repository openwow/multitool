using System;
using System.Collections.Generic;
using System.Text;

namespace OpenWoW.Multitool.Entities
{
    internal class CriteriaDb2 : Db2Base<CriteriaDb2>, IDb2
    {
        public int Asset { get; private set; }
        public int EligibilityWorldStateID { get; private set; }
        public int EligibilityWorldStateValue { get; private set; }
        public int FailAsset { get; private set; }
        public int FailEvent { get; private set; }
        public int Flags { get; private set; }
        public int StartAsset { get; private set; }
        public int StartEvent { get; private set; }
        public int StartTimer { get; private set; }
        public int Type { get; private set; }

        public ModifierTreeDb2 ModifierTree { get; private set; }
    }
}
