using System;
using System.Collections.Generic;
using System.Text;

namespace OpenWoW.Multitool.Entities
{
    internal class SpellProcsPerMinuteDb2 : Db2Base<SpellProcsPerMinuteDb2>, IDb2
    {
        public float BaseProcRate { get; private set; }
        public int Flags { get; private set; }
    }
}
