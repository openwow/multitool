using System;
using System.Collections.Generic;
using System.Text;

namespace OpenWoW.Multitool.Entities
{
    internal class AzeritePowerSetMemberDb2 : Db2Base<AzeritePowerSetMemberDb2>, IDb2
    {
        public int AzeritePowerID { get; private set; }
        public int AzeritePowerSetID { get; private set; }
        public int ClassID { get; private set; }
        public int OrderIndex { get; private set; }
        public int Tier { get; private set; }
    }
}
