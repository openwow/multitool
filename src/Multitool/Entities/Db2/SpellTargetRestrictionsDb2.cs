using OpenWoW.LibSpellParser.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace OpenWoW.Multitool.Entities
{
    internal class SpellTargetRestrictionsDb2 : Db2Base<SpellTargetRestrictionsDb2>, IDb2, IParserSpellTargetRestrictions
    {
        public float ConeDegrees { get; private set; }
        public float Width { get; private set; }
        public int DifficultyID { get; private set; }
        public int MaxTargetLevel { get; private set; }
        public int MaxTargets { get; private set; }
        public int TargetCreatureType { get; private set; }
        public int Targets { get; private set; }

        public SpellDb2 Spell { get; private set; }
    }
}
