using System;
using System.Collections.Generic;
using System.Text;

namespace OpenWoW.Multitool.Entities
{
    internal class ItemSubClassDb2 : Db2Base<ItemSubClassDb2>, IDb2
    {
        public int AuctionHouseSortOrder { get; private set; }
        public int ClassID { get; private set; }
        public int DisplayFlags { get; private set; }
        public int Flags { get; private set; }
        public int PostrequisiteProficiency { get; private set; }
        public int PrerequisiteProficiency { get; private set; }
        public int SubClassID { get; private set; }
        public int WeaponSwingSize { get; private set; }
        public string DisplayName { get; private set; }
        public string VerboseName { get; private set; }
    }
}
