using System;
using System.Collections.Generic;
using System.Text;

namespace OpenWoW.Multitool.Entities
{
    internal class AlliedRaceDb2 : Db2Base<AlliedRaceDb2>, IDb2
    {
        public int BannerColor { get; private set; }
        public int ChrRaceID { get; private set; }
        public int CrestTextureID { get; private set; }
        public int FemaleCreatureDisplayInfoID { get; private set; }
        public int MaleCreatureDisplayInfoID { get; private set; }
        public int ModelBackgroundTextureID { get; private set; }
        public int UIUnlockAchievementID { get; private set; }
    }
}
