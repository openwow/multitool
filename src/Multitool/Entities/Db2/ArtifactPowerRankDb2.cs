using System;
using System.Collections.Generic;
using System.Text;

namespace OpenWoW.Multitool.Entities
{
    internal class ArtifactPowerRankDb2 : Db2Base<ArtifactPowerRankDb2>, IDb2
    {
        public float AuraPointsOverride { get; private set; }
        public int ArtifactPowerID { get; private set; }
        public int ItemBonusListID { get; private set; }
        public int RankIndex { get; private set; }
        public int SpellID { get; private set; }
    }
}
