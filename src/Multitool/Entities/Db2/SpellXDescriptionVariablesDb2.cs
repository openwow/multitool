using System;
using System.Collections.Generic;
using System.Text;

namespace OpenWoW.Multitool.Entities
{
    internal class SpellXDescriptionVariablesDb2 : Db2Base<SpellXDescriptionVariablesDb2>, IDb2
    {
        public SpellDb2 Spell { get; private set; }
        public SpellDescriptionVariablesDb2 SpellDescriptionVariables { get; private set; }
    }
}
