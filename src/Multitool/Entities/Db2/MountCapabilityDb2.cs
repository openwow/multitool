using System;
using System.Collections.Generic;
using System.Text;

namespace OpenWoW.Multitool.Entities
{
    internal class MountCapabilityDb2 : Db2Base<MountCapabilityDb2>, IDb2
    {
        public int Flags { get; private set; }
        public int ModAuraSpellID { get; private set; }
        public int ReqAreaID { get; private set; }
        public int ReqKnownSpellID { get; private set; }
        public int ReqMapID { get; private set; }
        public int ReqRidingSkill { get; private set; }
        public int RequiredAuraSpellID { get; private set; }
    }
}
