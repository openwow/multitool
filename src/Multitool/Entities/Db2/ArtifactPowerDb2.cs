using System;
using System.Collections.Generic;
using System.Text;

namespace OpenWoW.Multitool.Entities
{
    internal class ArtifactPowerDb2 : Db2Base<ArtifactPowerDb2>, IDb2
    {
        public float[] DisplayPosition { get; private set; }
        public int ArtifactID { get; private set; }
        public int Flags { get; private set; }
        public int MaxPurchasableRank { get; private set; }
        public int RelicType { get; private set; }
        public int Tier { get; private set; }
    }
}
