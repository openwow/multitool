using OpenWoW.Common;
using OpenWoW.LibSpellParser.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace OpenWoW.Multitool.Entities
{
    internal class ChrSpecializationDb2 : Db2Base<ChrSpecializationDb2>, IDb2, IParserChrSpecialization
    {
        public int AnimReplacementSetID { get; private set; }
        public int Flags { get; private set; }
        public int IconFileDataID { get; private set; }
        public int OrderIndex { get; private set; }
        public int PetTalentType { get; private set; }
        public int PrimaryStatPriority { get; private set; }
        public int Role { get; private set; }
        public int[] MasterySpellID { get; private set; }
        public string Description { get; private set; }
        public string FemaleName { get; private set; }
        public string Name { get; private set; }

        public Class Class { get; private set; }

        int IParserChrSpecialization.ExternalID => ID;

        private int[] _specializationSpellIDs = null;
        IEnumerable<int> IParserChrSpecialization.SpecializationSpellIDs
        {
            get
            {
                if (_specializationSpellIDs == null)
                {
                    _specializationSpellIDs = BuildLoader.Relationships.GetSpellsByChrSpecialization(this).Select(s => s.ID).ToArray();
                }
                return _specializationSpellIDs;
            }
        }

        private int[] _specializationSpellPassiveInternalIDs = null;
        IEnumerable<int> IParserChrSpecialization.SpecializationSpellPassiveInternalIDs
        {
            get
            {
                if (_specializationSpellPassiveInternalIDs == null)
                {
                    _specializationSpellPassiveInternalIDs = BuildLoader.Relationships.GetSpellsByChrSpecialization(this)
                        .Where(x => x.Misc?.Flags1.HasFlag(SpellFlags1.Passive) ?? false)
                        .Select(x => x.ID)
                        .ToArray();
                }
                return _specializationSpellPassiveInternalIDs;

            }
        }

    }
}
