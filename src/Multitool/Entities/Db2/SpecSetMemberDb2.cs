using System;
using System.Collections.Generic;
using System.Text;

namespace OpenWoW.Multitool.Entities
{
    internal class SpecSetMemberDb2 : Db2Base<SpecSetMemberDb2>, IDb2
    {
        public int AzeriteSpecSetID { get; private set; }
        public int ChrSpecializationID { get; private set; }
    }
}
