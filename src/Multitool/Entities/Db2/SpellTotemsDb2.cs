using System;
using System.Collections.Generic;
using System.Text;

namespace OpenWoW.Multitool.Entities
{
    internal class SpellTotemsDb2 : Db2Base<SpellTotemsDb2>, IDb2
    {
        public int SpellID { get; private set; }
        public int[] RequiredTotemCategoryID { get; private set; }
        public int[] Totem { get; private set; }
    }
}
