using OpenWoW.Common;
using OpenWoW.LibSpellParser.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace OpenWoW.Multitool.Entities
{
    internal class SpellRangeDb2 : Db2Base<SpellRangeDb2>, IDb2, IParserSpellRange
    {
        public float[] RangeMax { get; private set; }
        public float[] RangeMin { get; private set; }
        public int Flags { get; private set; }
        public string DisplayName { get; private set; }
        public string DisplayNameShort { get; private set; }

        public float MinRangeEnemies => RangeMin.SafeGet(0);
        public float MaxRangeEnemies => RangeMax.SafeGet(0);
        public float MinRangeFriends => RangeMin.SafeGet(1);
        public float MaxRangeFriends => RangeMax.SafeGet(1);
    }
}
