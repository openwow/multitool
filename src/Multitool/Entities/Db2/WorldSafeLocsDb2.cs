using System;
using System.Collections.Generic;
using System.Text;

namespace OpenWoW.Multitool.Entities
{
    internal class WorldSafeLocsDb2 : Db2Base<WorldSafeLocsDb2>, IDb2
    {
        public float Facing { get; private set; }
        public float[] Location { get; private set; }
        public int MapID { get; private set; }
        public string AreaName { get; private set; }
    }
}
