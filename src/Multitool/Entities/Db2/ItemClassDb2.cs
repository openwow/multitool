using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace OpenWoW.Multitool.Entities
{
    [Category(DiffCategory.Miscellaneous)]
    [SubCategory("Item Classes")]
    internal class ItemClassDb2 : Db2Base<ItemClassDb2>, IDb2, IDiffable
    {
        public float PriceModifier { get; private set; }
        public int ClassID { get; private set; }
        public int Flags { get; private set; }
        public string ClassName { get; private set; }

        [Diffable]
        public string Name => ClassName;

        public IEnumerable<string> SubCategories => Enumerable.Empty<string>();
    }
}
