using System;
using System.Collections.Generic;
using System.Text;

namespace OpenWoW.Multitool.Entities
{
    internal class RewardPackDb2 : Db2Base<RewardPackDb2>, IDb2
    {
        public float ArtifactXPMultiplier { get; private set; }
        public int ArtifactXPCategoryID { get; private set; }
        public int ArtifactXPDifficulty { get; private set; }
        public int CharTitleID { get; private set; }
        public int Money { get; private set; }
        public int TreasurePickerID { get; private set; }
    }
}
