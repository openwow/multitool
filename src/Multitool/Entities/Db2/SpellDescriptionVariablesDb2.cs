using System;
using System.Collections.Generic;
using System.Text;

namespace OpenWoW.Multitool.Entities
{
    internal class SpellDescriptionVariablesDb2 : Db2Base<SpellDescriptionVariablesDb2>, IDb2
    {
        public string Variables { get; private set; }
    }
}
