using System;
using System.Collections.Generic;
using System.Text;

namespace OpenWoW.Multitool.Entities
{
    internal class ZoneMusicDb2 : Db2Base<ZoneMusicDb2>, IDb2
    {
        public int[] SilenceIntervalMax { get; private set; }
        public int[] SilenceIntervalMin { get; private set; }
        public int[] SoundID { get; private set; }
        public string SetName { get; private set; }
    }
}
