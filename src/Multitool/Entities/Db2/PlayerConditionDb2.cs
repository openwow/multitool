using System;
using System.Collections.Generic;
using System.Text;

namespace OpenWoW.Multitool.Entities
{
    internal class PlayerConditionDb2 : Db2Base<PlayerConditionDb2>, IDb2
    {
        public int AchievementLogic { get; private set; }
        public int AreaLogic { get; private set; }
        public int AuraSpellLogic { get; private set; }
        public int ChrSpecializationIndex { get; private set; }
        public int ChrSpecializationRole { get; private set; }
        public int ClassMask { get; private set; }
        public int CurrQuestLogic { get; private set; }
        public int CurrencyLogic { get; private set; }
        public int CurrentCompletedQuestLogic { get; private set; }
        public int CurrentPvpFaction { get; private set; }
        public int Flags { get; private set; }
        public int Gender { get; private set; }
        public int ItemFlags { get; private set; }
        public int ItemLogic { get; private set; }
        public int LanguageID { get; private set; }
        public int LfgLogic { get; private set; }
        public int LifetimeMaxPvpRank { get; private set; }
        public int MaxAvgEquippedItemLevel { get; private set; }
        public int MaxAvgItemLevel { get; private set; }
        public int MaxExpansionLevel { get; private set; }
        public int MaxExpansionTier { get; private set; }
        public int MaxFactionID { get; private set; }
        public int MaxGuildLevel { get; private set; }
        public int MaxLanguage { get; private set; }
        public int MaxLevel { get; private set; }
        public int MaxPvpRank { get; private set; }
        public int MaxReputation { get; private set; }
        public int MinAvgEquippedItemLevel { get; private set; }
        public int MinAvgItemLevel { get; private set; }
        public int MinExpansionLevel { get; private set; }
        public int MinExpansionTier { get; private set; }
        public int MinGuildLevel { get; private set; }
        public int MinLanguage { get; private set; }
        public int MinLevel { get; private set; }
        public int MinPvpRank { get; private set; }
        public int ModifierTreeID { get; private set; }
        public int NativeGender { get; private set; }
        public int PartyStatus { get; private set; }
        public int PhaseGroupID { get; private set; }
        public int PhaseID { get; private set; }
        public int PhaseUseFlags { get; private set; }
        public int PowerTypeComp { get; private set; }
        public int PowerTypeID { get; private set; }
        public int PowerTypeValue { get; private set; }
        public int PrevQuestLogic { get; private set; }
        public int PvpMedal { get; private set; }
        public int QuestKillID { get; private set; }
        public int QuestKillLogic { get; private set; }
        public int ReputationLogic { get; private set; }
        public int SkillLogic { get; private set; }
        public int SpellLogic { get; private set; }
        public int WeaponSubclassMask { get; private set; }
        public int WeatherID { get; private set; }
        public int WorldStateExpressionID { get; private set; }
        public int[] Achievement { get; private set; }
        public int[] AreaID { get; private set; }
        public int[] AuraSpellID { get; private set; }
        public int[] AuraStacks { get; private set; }
        public int[] CurrQuestID { get; private set; }
        public int[] CurrencyCount { get; private set; }
        public int[] CurrencyTypesID { get; private set; }
        public int[] CurrentCompletedQuestID { get; private set; }
        public int[] Explored { get; private set; }
        public int[] ItemCount { get; private set; }
        public int[] ItemID { get; private set; }
        public int[] LfgCompare { get; private set; }
        public int[] LfgStatus { get; private set; }
        public int[] LfgValue { get; private set; }
        public int[] MaxSkill { get; private set; }
        public int[] MinFactionID { get; private set; }
        public int[] MinReputation { get; private set; }
        public int[] MinSkill { get; private set; }
        public int[] MovementFlags { get; private set; }
        public int[] PrevQuestID { get; private set; }
        public int[] QuestKillMonster { get; private set; }
        public int[] SkillID { get; private set; }
        public int[] SpellID { get; private set; }
        public int[] Time { get; private set; }
        public long RaceMask { get; private set; }
        public string FailureDescription { get; private set; }
    }
}
