using System;
using System.Collections.Generic;
using System.Text;

namespace OpenWoW.Multitool.Entities
{
    internal class ItemSubClassMaskDb2 : Db2Base<ItemSubClassMaskDb2>, IDb2
    {
        public int ClassID { get; private set; }
        public int Mask { get; private set; }
        public string Name { get; private set; }
    }
}
