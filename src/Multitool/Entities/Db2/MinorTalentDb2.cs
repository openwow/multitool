using System;
using System.Collections.Generic;
using System.Text;

namespace OpenWoW.Multitool.Entities
{
    internal class MinorTalentDb2 : Db2Base<MinorTalentDb2>, IDb2
    {
        public int ChrSpecializationID { get; private set; }
        public int OrderIndex { get; private set; }
        public int SpellID { get; private set; }
    }
}
