using System;
using System.Collections.Generic;
using System.Text;

namespace OpenWoW.Multitool.Entities
{
    internal class ModifierTreeDb2 : Db2Base<ModifierTreeDb2>, IDb2
    {
        public int Amount { get; private set; }
        public int Asset { get; private set; }
        public int Operator { get; private set; }
        public int Parent { get; private set; }
        public int SecondaryAsset { get; private set; }
        public int TertiaryAsset { get; private set; }
        public int Type { get; private set; }
    }
}
