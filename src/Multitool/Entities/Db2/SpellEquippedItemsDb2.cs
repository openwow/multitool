using System;
using System.Collections.Generic;
using System.Text;

namespace OpenWoW.Multitool.Entities
{
    internal class SpellEquippedItemsDb2 : Db2Base<SpellEquippedItemsDb2>, IDb2
    {
        public int EquippedItemClass { get; private set; }
        public int EquippedItemInvTypes { get; private set; }
        public int EquippedItemSubclass { get; private set; }
        public int SpellID { get; private set; }
    }
}
