using System;
using System.Collections.Generic;
using System.Text;

namespace OpenWoW.Multitool.Entities
{
    internal class TalentDb2 : Db2Base<TalentDb2>, IDb2
    {
        public int ClassID { get; private set; }
        public int ColumnIndex { get; private set; }
        public int Flags { get; private set; }
        public int OverridesSpellID { get; private set; }
        public int TierID { get; private set; }
        public int[] CategoryMask { get; private set; }
        public string Description { get; private set; }

        public ChrSpecializationDb2 Spec { get; private set; }
        public SpellDb2 Spell { get; private set; }
    }
}
