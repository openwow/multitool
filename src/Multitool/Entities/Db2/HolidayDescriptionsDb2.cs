using System;
using System.Collections.Generic;
using System.Text;

namespace OpenWoW.Multitool.Entities
{
    internal class HolidayDescriptionsDb2 : Db2Base<HolidayDescriptionsDb2>, IDb2
    {
        public string Description { get; private set; }
    }
}
