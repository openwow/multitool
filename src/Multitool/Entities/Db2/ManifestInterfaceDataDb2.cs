using System;
using System.Collections.Generic;
using System.Text;

namespace OpenWoW.Multitool.Entities
{
    internal class ManifestInterfaceDataDb2 : Db2Base<ManifestInterfaceDataDb2>, IDb2
    {
        public string FileName { get; private set; }
        public string FilePath { get; private set; }
    }
}
