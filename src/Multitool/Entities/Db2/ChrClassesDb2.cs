using System;
using System.Collections.Generic;
using System.Text;

namespace OpenWoW.Multitool.Entities
{
    internal class ChrClassesDb2 : Db2Base<ChrClassesDb2>, IDb2
    {
        public int AttackPowerPerAgility { get; private set; }
        public int AttackPowerPerStrength { get; private set; }
        public int CinematicSequenceID { get; private set; }
        public int CreateScreenFileDataID { get; private set; }
        public int DefaultSpec { get; private set; }
        public int DisplayPowerTypeID { get; private set; }
        public int Flags { get; private set; }
        public int IconFileDataID { get; private set; }
        public int LowResScreenFileDataID { get; private set; }
        public int PrimaryStatPriority { get; private set; }
        public int RangedAttackPowerPerAgility { get; private set; }
        public int SelectScreenFileDataID { get; private set; }
        public int SpellClassSet { get; private set; }
        public int SpellTexturesFileDataID { get; private set; }
        public int StartingLevel { get; private set; }
        public string FileName { get; private set; }
        public string Name { get; private set; }
        public string NameFemale { get; private set; }
        public string NameMale { get; private set; }
        public string PetNameToken { get; private set; }
    }
}
