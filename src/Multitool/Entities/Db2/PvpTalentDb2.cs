using System;
using System.Collections.Generic;
using System.Text;

namespace OpenWoW.Multitool.Entities
{
    internal class PvpTalentDb2 : Db2Base<PvpTalentDb2>, IDb2
    {
        public int ActionBarSpellID { get; private set; }
        public int ChrSpecializationID { get; private set; }
        public int Flags { get; private set; }
        public int LevelRequired { get; private set; }
        public int OverridesSpellID { get; private set; }
        public int PvpTalentCategoryID { get; private set; }
        public int SpellID { get; private set; }
        public string Description { get; private set; }
    }
}
