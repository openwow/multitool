using System;
using System.Collections.Generic;
using System.Text;

namespace OpenWoW.Multitool.Entities
{
    internal class ArtifactCategoryDb2 : Db2Base<ArtifactCategoryDb2>, IDb2
    {
        public int XPMultiplierCurrencyTypesID { get; private set; }
        public int XPMultiplierCurveID { get; private set; }
    }
}
