using System;
using System.Collections.Generic;
using System.Text;

namespace OpenWoW.Multitool.Entities
{
    internal class SpellNameDb2 : Db2Base<SpellNameDb2>, IDb2
    {
        public string Name { get; private set; }
    }
}
