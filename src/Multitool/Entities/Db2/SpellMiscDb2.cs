using OpenWoW.Common;
using System;
using System.Collections.Generic;
using System.Text;

namespace OpenWoW.Multitool.Entities
{
    internal class SpellMiscDb2 : Db2Base<SpellMiscDb2>, IDb2
    {
        public float LaunchDelay { get; private set; }
        public float MinDuration { get; private set; }
        public float Speed { get; private set; }
        public int ActiveIconFileDataID { get; private set; }
        public int DifficultyID { get; private set; }
        public int SchoolMask { get; private set; }
        public int SpellIconFileDataID { get; private set; }
        public int[] Attributes { get; private set; }

        public SpellFlags1 Flags1 => (SpellFlags1)Attributes.SafeGet(0);
        public SpellFlags2 Flags2 => (SpellFlags2)Attributes.SafeGet(1);
        public SpellFlags3 Flags3 => (SpellFlags3)Attributes.SafeGet(2);
        public SpellFlags4 Flags4 => (SpellFlags4)Attributes.SafeGet(3);
        public SpellFlags5 Flags5 => (SpellFlags5)Attributes.SafeGet(4);
        public SpellFlags6 Flags6 => (SpellFlags6)Attributes.SafeGet(5);
        public SpellFlags7 Flags7 => (SpellFlags7)Attributes.SafeGet(6);
        public SpellFlags8 Flags8 => (SpellFlags8)Attributes.SafeGet(7);
        public SpellFlags9 Flags9 => (SpellFlags9)Attributes.SafeGet(8);
        public SpellFlags10 Flags10 => (SpellFlags10)Attributes.SafeGet(9);
        public SpellFlags11 Flags11 => (SpellFlags11)Attributes.SafeGet(10);
        public SpellFlags12 Flags12 => (SpellFlags12)Attributes.SafeGet(11);
        public SpellFlags13 Flags13 => (SpellFlags13)Attributes.SafeGet(12);
        public SpellFlags14 Flags14 => (SpellFlags14)Attributes.SafeGet(13);

        public SpellDb2 Spell { get; private set; }
        public SpellCastTimesDb2 CastingTimeIndex { get; private set; }
        public SpellDurationDb2 DurationIndex { get; private set; }
        public SpellRangeDb2 RangeIndex { get; private set; }
    }
}
