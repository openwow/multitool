using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace OpenWoW.Multitool.Entities
{
    [Category(DiffCategory.Miscellaneous)]
    [SubCategory("Character Titles")]
    internal class CharTitlesDb2 : Db2Base<CharTitlesDb2>, IDb2, IDiffable
    {
        public int Flags { get; private set; }
        public int MaskID { get; private set; }
        public string FemaleName { get; private set; }
        public string MaleName { get; private set; }

        [Diffable]
        string IDiffable.Name
        {
            get
            {
                if (MaleName == FemaleName)
                {
                    return MaleName;
                }
                else
                {
                    return $"{ MaleName } / { FemaleName }";
                }
            }
        }

        public IEnumerable<string> SubCategories => Enumerable.Empty<string>();
    }
}
