using System;
using System.Collections.Generic;
using System.Text;

namespace OpenWoW.Multitool.Entities
{
    internal class SkillLineAbilityDb2 : Db2Base<SkillLineAbilityDb2>, IDb2
    {
        public int AcquireMethod { get; private set; }
        public int ClassMask { get; private set; }
        public int Flags { get; private set; }
        public int MinSkillLineRank { get; private set; }
        public int NumSkillUps { get; private set; }
        public int SkillLineID { get; private set; }
        public int SkillupSkillLineID { get; private set; }
        public int SpellID { get; private set; }
        public int SupersedesSpellID { get; private set; }
        public int TradeSkillCategoryID { get; private set; }
        public int TrivialSkillLineRankHigh { get; private set; }
        public int TrivialSkillLineRankLow { get; private set; }
        public int UniqueBit { get; private set; }
        public long RaceMask { get; private set; }
    }
}
