using System;
using System.Collections.Generic;
using System.Text;

namespace OpenWoW.Multitool.Entities
{
    internal class GarrTalentDb2 : Db2Base<GarrTalentDb2>, IDb2
    {
        public int DurationSecs { get; private set; }
        public int Flags { get; private set; }
        public int GarrAbilityID { get; private set; }
        public int GarrTalentTreeID { get; private set; }
        public int IconFileDataID { get; private set; }
        public int PerkPlayerConditionID { get; private set; }
        public int PerkSpellID { get; private set; }
        public int PlayerConditionID { get; private set; }
        public int ResearchCost { get; private set; }
        public int ResearchCostCurrencyTypesID { get; private set; }
        public int ResearchGoldCost { get; private set; }
        public int RespecCost { get; private set; }
        public int RespecCostCurrencyTypesID { get; private set; }
        public int RespecDurationSecs { get; private set; }
        public int RespecGoldCost { get; private set; }
        public int Tier { get; private set; }
        public int UIOrder { get; private set; }
        public string Description { get; private set; }
        public string Name { get; private set; }
    }
}
