using System;
using System.Collections.Generic;
using System.Text;

namespace OpenWoW.Multitool.Entities
{
    internal class GemPropertiesDb2 : Db2Base<GemPropertiesDb2>, IDb2
    {
        public int EnchantID { get; private set; }
        public int MinItemLevel { get; private set; }
        public int Type { get; private set; }
    }
}
