using EnumsNET;
using OpenWoW.Common;
using OpenWoW.LibSpellParser;
using OpenWoW.LibSpellParser.Interfaces;
using OpenWoW.Multitool.Tools.DiffBuild;
using System;
using System.Collections.Generic;
using System.Linq;

namespace OpenWoW.Multitool.Entities
{
    [Category(DiffCategory.Spells)]
    internal class SpellDb2 : Db2Base<SpellDb2>, IDb2, IDiffable, ICategorizable, IParserSpell, ITooltipSpell
    {
        public string AuraDescription { get; private set; }
        public string Description { get; private set; }
        public string NameSubtext { get; private set; }

        // These are hooked up magically
        public SpellAuraOptionsDb2 AuraOptions { get; set; }
        public SpellCategoriesDb2 Categories { get; set; }
        public SpellClassOptionsDb2 ClassOptions { get; set; }
        public SpellCooldownsDb2 Cooldowns { get; set; }
        public SpellDescriptionVariablesDb2 DescriptionVariables { get; set; }
        public SpellInterruptsDb2 Interrupts { get; set; }
        public SpellLevelsDb2 Levels { get; set; }
        public SpellMiscDb2 Misc { get; set; }
        public SpellScalingDb2 Scaling { get; set; }
        public SpellTargetRestrictionsDb2 TargetRestrictions { get; set; }

        public SpellEffectDb2[] Effects { get; set; } = SpellEffectDb2.EmptyArray;
        public SpellPowerDb2[] Powers { get; set; } = SpellPowerDb2.EmptyArray;
        public TalentDb2[] Talents { get; set; } = TalentDb2.EmptyArray;

        // Categorize calculates these
        public SpellCategory Category { get; private set; } = SpellCategory.Uncategorized;
        public int SubCategory { get; private set; }
        public int SubSubCategory { get; private set; }
        public int SubSubSubCategory { get; private set; }

        public Class RequiredClass { get; private set; } = Class.None;

        // ?
        public int MinLevel => Levels == null ? 0 : (Levels.BaseLevel == 0 ? Levels.SpellLevel : Levels.BaseLevel);
        public int MaxLevel => Levels == null ? 0 : (Levels.MaxLevel == 0 ? BuildLoader.MaxLevel : Levels.MaxLevel);

        public int ComputedLevel
        {
            get
            {
                if (Levels == null)
                {
                    return 0;
                }
                if (Levels.SpellLevel != 0)
                {
                    return Levels.SpellLevel;
                }
                return Levels.BaseLevel;
            }
        }

        private string _name = null;
        [Diffable(Order = 0)]
        public string Name
        {
            get
            {
                if (_name == null)
                {
                    var spellName = BuildLoader.Db2.SpellName.Get(ID);
                    _name = spellName?.Name ?? $"Spell #{ ID.ToString() }";
                }
                return _name;
            }
        }

        private string _parsedDescription = null;
        [Diffable(Name = "Description")]
        public string ParsedDescription
        {
            get
            {
                if (_parsedDescription == null)
                {
                    _parsedDescription = DiffSpellParser.SimpleParse(BuildLoader, this, Description);
                }
                return _parsedDescription;
            }
            set => _parsedDescription = value;
        }

        public SpellEffectDb2 GetEffect(int orderId, bool returnEmptyIfNotFound = true)
        {
            SpellEffectDb2 ret = Effects.Where(x => x.Index == orderId).OrderBy(x => x.DifficultyID).FirstOrDefault();

            // If we didn't find this specific effect, return the first one
            if (ret == null)
            {
                ret = Effects.FirstOrDefault();
            }

            if (ret == null && returnEmptyIfNotFound)
            {
                ret = new SpellEffectDb2 { Spell = this };
            }

            return ret;
        }

        #region Affected Spells pain
        private SpellDb2[] _affectedSpells = null;
        public SpellDb2[] AffectedSpells
        {
            get
            {
                if (_affectedSpells == null)
                {
                    var lookupSpells = new HashSet<int>();
                    var allAffectedSpells = new HashSet<int>();

                    foreach (SpellEffectDb2 effect in Effects)
                    {
                        var affected = GetFilteredAffected(effect.AffectedSpells);
                        lookupSpells.UnionWith(affected.Select(x => x.ID));

                        allAffectedSpells.UnionWith(effect.AffectedSpells.Select(x => x.ID));
                    }

                    //Console.WriteLine("AS!1 spell={0} lookupSpells={1} allAffectedSpells={2}", ID, String.Join(",", lookupSpells), String.Join(",", allAffectedSpells));

                    if (allAffectedSpells.Count > 0)
                    {
                        // Check for related spells
                        var relatedSpells = allAffectedSpells.SelectMany(x => BuildLoader.Relationships.GetRelatedSpellsBySpellID(x))
                            .Select(x => (SpellDb2)x.RelatedSpell)
                            .Distinct()
                            .ToArray();
                        if (relatedSpells.Length > 0)
                        {
                            lookupSpells.UnionWith(GetFilteredAffected(relatedSpells).Select(x => x.ID));
                        }

                        // Check for triggered spell effects
                        var triggeringSpells = BuildLoader.Db2.SpellEffect.All
                            .Where(x =>
                                (x.Effect == SpellEffectEffect.TriggerSpell || x.Effect == SpellEffectEffect.TriggerSpell2) &&
                                x.TriggerSpell != null &&
                                allAffectedSpells.Contains(x.TriggerSpell.ID)
                            )
                            .ToArray()
                            .GroupBy(x => x.Spell.ID)
                            .ToDictionary(k => k.Key, v => v.Select(x => x.TriggerSpell.ID).ToArray());

                        var affectedTriggeringIDs = GetFilteredAffected(BuildLoader.Db2.Spell.All.Where(x => triggeringSpells.ContainsKey(x.ID))).Select(x => x.ID).ToArray();
                        var affectedTriggeredIDs = triggeringSpells.Where(x => affectedTriggeringIDs.Contains(x.Key)).SelectMany(x => x.Value).ToArray();
                        if (affectedTriggeredIDs.Length > 0)
                        {
                            lookupSpells.UnionWith(affectedTriggeredIDs);
                        }
                    }

                    //Console.WriteLine("AS!2 spell={0} lookupSpells={1} allAffectedSpells={2}", ID, String.Join(",", lookupSpells), String.Join(",", allAffectedSpells));

                    //using (var w = System.IO.File.AppendText(@"c:\temp\gar.txt"))
                    //    w.WriteLine("spell={0} affected={1}", ID, String.Join(",", lookupSpells));

                    _affectedSpells = BuildLoader.Db2.Spell.GetAll(lookupSpells).ToArray();
                    //Console.WriteLine("AS!uncached spell={0} affected={1}", ID, String.Join(",", _affectedSpells.Select(x => x.ID)));
                }
                /*else
                {
                    Console.WriteLine("AS!cached spell={0} affected={1}", ID, String.Join(",", _affectedSpells.Select(x => x.ID)));
                }*/
                return _affectedSpells;
            }
        }

        private IEnumerable<SpellDb2> GetFilteredAffected(IEnumerable<SpellDb2> affectedSpells)
        {
            HashSet<int> specSpellIDs = null;
            //var specSpellIDs = new HashSet<int>(ComplexDbcRelationships.Instance.GetSpellsByChrSpecializationID(spell.SubSubSubCategory).Select(x => x.ID));
            if (Category == SpellCategory.ClassAbilities && SubSubCategory == (int)SpellClassAbilityCategory.Specialization)
            {
                specSpellIDs = new HashSet<int>(BuildLoader.Relationships.GetSpellsByChrSpecializationID(SubSubSubCategory).Select(x => x.ID));
            }
            return ((IParserSpell)this).GetFilteredAffected(affectedSpells, specSpellIDs).Cast<SpellDb2>();
        }
        #endregion

        #region ICategorizable
        public bool ShouldDiff => Category != SpellCategory.Uncategorized;

        // Slightly hacky, but if we're going to run diffs on this we should pre-generate the spell description
        public void Categorize()
        {
            CategorizeInternal();
            if (ShouldDiff)
            {
                string foo = ParsedDescription;
            }
        }

        private void CategorizeInternal()
        {
            // Azerite Traits

            // Class Abilities - Baseline

            // Class Abilities - PvP Talent

            // Class Abilities - Specialization

            // Class Abilities - Talent
            var classTalents = Talents.Where(x => x.ClassID != 0).ToArray();
            if (classTalents.Length > 0)
            {
                var firstTalent = classTalents[0];
                Category = SpellCategory.ClassAbilities;
                SubCategory = firstTalent.ClassID;
                SubSubCategory = (int)SpellClassAbilityCategory.Specialization;
                if (classTalents.Length == 1 && firstTalent.Spec != null)
                {
                    SubSubSubCategory = firstTalent.Spec.ID;
                }
                RequiredClass = (Class)firstTalent.ClassID;
                return;
            }

            // Mounts

            // Pet Abilities

            // Pet Talents

            // Professions

            // Racial Abilities
        }
        #endregion

        #region IDiffable
        public IEnumerable<string> SubCategories
        {
            get
            {
                yield return Category.AsString(EnumFormat.DisplayName, EnumFormat.Name);
                if (Category == SpellCategory.ClassAbilities)
                {
                    yield return ((Class)SubCategory).AsString(EnumFormat.DisplayName, EnumFormat.Name);
                    //yield return ((SpellClassAbilityCategory)SubSubCategory).AsString();
                    if (SubSubSubCategory > 0)
                    {
                        yield return BuildLoader.Db2.ChrSpecialization.Get(SubSubSubCategory).Name;
                    }
                }
            }
        }
        #endregion

        #region IParserSpell
        // Yeah, these are dumb, but type mismatches otherwise :|
        IParserSpellAuraOptions IParserSpell.AuraOptions => AuraOptions;
        IParserSpellDuration IParserSpell.Duration => Misc?.DurationIndex;
        IParserSpellRange IParserSpell.Range => Misc?.RangeIndex;
        IParserSpellTargetRestrictions IParserSpell.TargetRestrictions => TargetRestrictions;

        IEnumerable<IParserSpellEffect> IParserSpell.Effects => Effects;
        IEnumerable<IParserSpellPower> IParserSpell.Powers => Powers;

        int IParserSpell.ExternalID => ID;
        int IParserSpell.InternalID => ID;

        string IParserSpell.DescriptionCode => DescriptionVariables?.Variables;

        SpellFlags1 IParserSpell.Flags1 => Misc?.Flags1 ?? 0;
        SpellFlags2 IParserSpell.Flags2 => Misc?.Flags2 ?? 0;
        SpellFlags3 IParserSpell.Flags3 => Misc?.Flags3 ?? 0;
        SpellFlags4 IParserSpell.Flags4 => Misc?.Flags4 ?? 0;
        SpellFlags5 IParserSpell.Flags5 => Misc?.Flags5 ?? 0;
        SpellFlags6 IParserSpell.Flags6 => Misc?.Flags6 ?? 0;
        SpellFlags7 IParserSpell.Flags7 => Misc?.Flags7 ?? 0;
        SpellFlags8 IParserSpell.Flags8 => Misc?.Flags8 ?? 0;
        SpellFlags9 IParserSpell.Flags9 => Misc?.Flags9 ?? 0;
        SpellFlags10 IParserSpell.Flags10 => Misc?.Flags10 ?? 0;
        SpellFlags11 IParserSpell.Flags11 => Misc?.Flags11 ?? 0;
        //SpellFlags12 IParserSpell.Flags12 => Misc.Flags12;
        //SpellFlags13 IParserSpell.Flags13 => Misc.Flags13;
        //SpellFlags14 IParserSpell.Flags14 => Misc.Flags14;

        IEnumerable<IParserSpell> IParserSpell.AffectedByPerks => Enumerable.Empty<IParserSpell>(); // FIXME

        IEnumerable<IParserChrSpecialization> IParserSpell.ChrSpecializations => BuildLoader.Relationships.GetChrSpecializationsBySpell(this);

        IEnumerable<IParserSpell> IParserSpell.GetAffectedRelated(IParserSpellEffect effect)
        {
            var effectAffected = Effects.SelectMany(x => x.AffectedSpells).Distinct();
            var effectAffectedIDs = new HashSet<int>(effectAffected.Select(x => x.ID));
            var spellAffected = AffectedSpells.Select(x => x.ID).ToArray();
            var relatedSpells = spellAffected.SelectMany(x => BuildLoader.Relationships.GetRelatedSpellsBySpellID(x)).Distinct().ToArray();

            //Console.WriteLine("GAR! spell={0} effectAffected={1} spellAffected={2} relatedSpells={3}", ID, String.Join(",", effectAffectedIDs), String.Join(",", spellAffected),
            //    String.Join(", ", relatedSpells.Select(x => String.Format("{0} => {1}", x.Spell.ID, x.RelatedSpell.ID))));

            return relatedSpells.Where(x => effectAffectedIDs.Contains(x.Spell.InternalID) || effectAffectedIDs.Contains(x.RelatedSpell.InternalID)).Select(x => x.Spell)
                .Union(GetFilteredAffected(effectAffected))
                .Distinct();
        }

        double IParserSpell.GetArtifactEffectPoints(int rank)
        {
            throw new NotImplementedException();
        }

        double IParserSpell.GetEffectPointsPerCombo(int effectID)
        {
            if (Scaling == null)
            {
                return GetEffect(effectID).PointsPerResource;
            }

            var spellEffect = GetEffect(effectID, false);
            if (spellEffect == null)
            {
                return 0;
            }

            return Scaling.GetPointsPerCombo(spellEffect, BuildLoader.MaxLevel);
        }

        IParserSpell IParserSpell.GetRelatedSpellWithClass()
        {
            return BuildLoader.Relationships.GetRelatedSpellsBySpell(this)
                .Where(x => x.RelatedSpell.RequiredClass != Class.None)
                .Select(x => x.RelatedSpell)
                .FirstOrDefault();
        }
        #endregion

        #region ITooltipSpell
        public int SpellID => ID;

        private int CastTimeMs
        {
            get
            {
                if (Misc == null)
                {
                    return 0;
                }

                // Broken class hookups, uh-oh
                if ((!Channeled && Misc?.CastingTimeIndex?.Base < 0) || (Channeled && Misc?.DurationIndex == null))
                {
                    return 0;
                }

                if (Channeled)
                {
                    return Math.Min(Misc.DurationIndex.MaxDuration, Misc.DurationIndex.Duration);
                }
                else
                {
                    return Math.Max(Misc.CastingTimeIndex.Minimum, Misc.CastingTimeIndex.Base);
                }
            }
        }

        private bool Channeled => Interrupts?.ChannelInterruptFlags.SafeGet(0) != 0;

        public string CastTimeText
        {
            get
            {
                if (!Channeled && CastTimeMs < 0)
                {
                    return "";
                }

                if (Misc?.Flags1.HasFlag(SpellFlags1.Passive) == true)
                {
                    return "";
                }

                string ret = TimeUtilities.GetTooltipTime(CastTimeMs, "Instant");
                if (ret != "Instant")
                {
                    ret += " cast";
                }
                if (Channeled)
                {
                    ret += " (Channeled)";
                }

                return ret.ToString();
            }
        }

        public string CooldownText
        {
            get
            {
                string ret = null;
                if (Categories?.ChargeCategory?.MaxCharges > 0)
                {
                    ret = TimeUtilities.GetTooltipTime(Categories.ChargeCategory.ChargeRecoveryTime, null);
                    if (ret != null)
                    {
                        ret += " recharge";
                    }
                }
                else if (Cooldowns != null)
                {
                    ret = TimeUtilities.GetTooltipTime(Math.Max(Cooldowns.RecoveryTime, Cooldowns.CategoryRecoveryTime), null);
                    if (ret != null)
                    {
                        ret += " cooldown";
                    }
                }
                return ret ?? "";
            }
        }

        public string GlobalCooldownText
        {
            get
            {
                string ret = "";
                if (Cooldowns != null && Cooldowns.StartRecoveryTime > 0 && Cooldowns.StartRecoveryTime != 1500)
                {
                    ret = $"{ TimeUtilities.GetTooltipTime(Cooldowns.StartRecoveryTime, null) } global cooldown";
                }
                return ret;
            }
        }

        public string PowerText
        {
            get
            {
                List<string> ret = new List<string>();
                foreach (var spellPower in Powers)
                {
                    if (Misc?.Flags2.HasFlag(SpellFlags2.UseAllPower) ?? false)
                    {
                        ret.Add($"100% { spellPower.GetPowerType().Name }");
                        continue;
                    }

                    string text = spellPower.ToString();
                    if (!string.IsNullOrEmpty(text))
                    {
                        ret.Add(text);
                    }
                }
                return string.Join(" / ", ret);
            }
        }

        public string ProcsPerMinuteText
        {
            get
            {
                string ret = "";
                float ppm = AuraOptions?.ProcsPerMin ?? 0;
                if (ppm > 0)
                {
                    ret = string.Format("Approximately {0:0.##} proc{1} per minute", ppm, ppm == 1 ? "" : "s");
                }
                return ret;
            }
        }

        public string RangeText
        {
            get
            {
                SpellRangeDb2 range = Misc?.RangeIndex;
                if (range == null || (range.MaxRangeEnemies == 0 && range.MaxRangeFriends == 0))
                {
                    return "";
                }

                if (range.MaxRangeEnemies == 5)
                {
                    return "Melee range";
                }

                if (range.MaxRangeEnemies == 50000)
                {
                    return "Unlimited range";
                }

                string friends;
                string enemies;

                if (range.MinRangeEnemies != 0)
                {
                    enemies = String.Format("{0}-{1}", range.MinRangeEnemies.ToTooltipNumber(), range.MaxRangeEnemies.ToTooltipNumber());
                }
                else
                {
                    enemies = range.MaxRangeEnemies.ToTooltipNumber();
                }

                if (range.MinRangeFriends != 0)
                {
                    friends = String.Format("{0}-{1}", range.MinRangeFriends.ToTooltipNumber(), range.MaxRangeFriends.ToTooltipNumber());
                }
                else
                {
                    friends = range.MaxRangeFriends.ToTooltipNumber();
                }

                if (friends == enemies)
                {
                    return friends == "0" ? null : String.Format("{0} yd range", friends);
                }

                return String.Format("{0} yd range ({1} for friends)", enemies, friends);
            }
        }

        public string RankText
        {
            get
            {
                return "";
            }
        }
        #endregion
    }
}
