using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace OpenWoW.Multitool.Entities
{
    [Category(DiffCategory.Miscellaneous)]
    [SubCategory("Currency Types")]
    internal class CurrencyTypesDb2 : Db2Base<CurrencyTypesDb2>, IDb2, IDiffable
    {
        public int Flags { get; private set; }
        public int IconFileDataID { get; private set; }
        [Diffable(Name = "Max Per Week")]
        public int MaxEarnablePerWeek { get; private set; }
        [Diffable(Name = "Max Quantity")]
        public int MaxQuantity { get; private set; }
        public int Quality { get; private set; }
        public int SpellCategory { get; private set; }
        public int SpellWeight { get; private set; }
        [Diffable]
        public string Description { get; private set; }
        [Diffable(Order = 0)]
        public string Name { get; private set; }

        public CurrencyCategoryDb2 Category { get; private set; }
        public FactionDb2 Faction { get; private set; }

        #region IDiffable
        public IEnumerable<string> SubCategories => new string[] { Category?.Name ?? "Unknown" };

        [Diffable(Name = "Faction")]
        public string FactionName => Faction?.Name;
        #endregion
    }
}
