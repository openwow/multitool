using System;
using System.Collections.Generic;
using System.Text;

namespace OpenWoW.Multitool.Entities
{
    internal class HolidayNamesDb2 : Db2Base<HolidayNamesDb2>, IDb2
    {
        public string Name { get; private set; }
    }
}
