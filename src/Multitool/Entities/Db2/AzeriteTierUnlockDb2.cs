using System;
using System.Collections.Generic;
using System.Text;

namespace OpenWoW.Multitool.Entities
{
    internal class AzeriteTierUnlockDb2 : Db2Base<AzeriteTierUnlockDb2>, IDb2
    {
        public int AzeriteLevel { get; private set; }
        public int AzeriteTierUnlockSetID { get; private set; }
        public int ItemCreationContext { get; private set; }
        public int Tier { get; private set; }
    }
}
