using OpenWoW.Common;
using System;
using System.Collections.Generic;
using System.Text;

namespace OpenWoW.Multitool.Entities
{
    internal class RandPropPointsDb2 : Db2Base<RandPropPointsDb2>, IDb2
    {
        public int DamageReplaceStat { get; private set; }
        public int[] Epic { get; private set; }
        public int[] Good { get; private set; }
        public int[] Superior { get; private set; }

        public int Epic1 => Epic.SafeGet(0);
        public int Epic2 => Epic.SafeGet(1);
        public int Epic3 => Epic.SafeGet(2);
        public int Epic4 => Epic.SafeGet(3);
        public int Epic5 => Epic.SafeGet(4);

        public int Good1 => Good.SafeGet(0);
        public int Good2 => Good.SafeGet(1);
        public int Good3 => Good.SafeGet(2);
        public int Good4 => Good.SafeGet(3);
        public int Good5 => Good.SafeGet(4);

        public int Superior1 => Superior.SafeGet(0);
        public int Superior2 => Superior.SafeGet(1);
        public int Superior3 => Superior.SafeGet(2);
        public int Superior4 => Superior.SafeGet(3);
        public int Superior5 => Superior.SafeGet(4);
    }
}
