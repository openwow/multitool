using System;
using System.Collections.Generic;
using System.Text;

namespace OpenWoW.Multitool.Entities
{
    internal class LfgRoleRequirementDb2 : Db2Base<LfgRoleRequirementDb2>, IDb2
    {
        public int LFGDungeonsID { get; private set; }
        public int PlayerConditionID { get; private set; }
        public int RoleType { get; private set; }
    }
}
