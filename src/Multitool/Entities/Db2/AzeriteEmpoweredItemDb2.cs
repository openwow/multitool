using System;
using System.Collections.Generic;
using System.Text;

namespace OpenWoW.Multitool.Entities
{
    internal class AzeriteEmpoweredItemDb2 : Db2Base<AzeriteEmpoweredItemDb2>, IDb2
    {
        public int AzeritePowerSetID { get; private set; }
        public int AzeriteTierUnlockSetID { get; private set; }
        public int ItemID { get; private set; }
    }
}
