using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace OpenWoW.Multitool.Entities
{
    [Category(DiffCategory.Items)]
    internal class ItemSparseDb2 : Db2Base<ItemSparseDb2>, IDb2, IDiffable
    {
        public float DmgVariance { get; private set; }
        public float ItemRange { get; private set; }
        public float PriceRandomValue { get; private set; }
        public float PriceVariance { get; private set; }
        public float QualityModifier { get; private set; }
        public float[] StatPercentageOfSocket { get; private set; }
        public int AllowableClass { get; private set; }
        public int AlternateFactionItemID { get; private set; }
        public int ArtifactID { get; private set; }
        public int BagFamily { get; private set; }
        public int Bonding { get; private set; }
        public int BuyPrice { get; private set; }
        public int ContainerSlots { get; private set; }
        public int DamageType { get; private set; }
        public int DurationInInventory { get; private set; }
        public int ExpansionID { get; private set; }
        public int GemPropertiesID { get; private set; }
        public int InstanceBoundMapID { get; private set; }
        public int InventoryType { get; private set; }
        public int ItemDelay { get; private set; }
        public int ItemLevel { get; private set; }
        public int ItemLimitCategoryID { get; private set; }
        public int ItemNameDescriptionID { get; private set; }
        public int ItemSet { get; private set; }
        public int LanguageID { get; private set; }
        public int LockID { get; private set; }
        public int Material { get; private set; }
        public int MaxCount { get; private set; }
        public int MinFactionID { get; private set; }
        public int MinReputation { get; private set; }
        public int OverallQualityID { get; private set; }
        public int PageID { get; private set; }
        public int PageMaterialID { get; private set; }
        public int RequiredAbility { get; private set; }
        public int RequiredHolidayID { get; private set; }
        public int RequiredLevel { get; private set; }
        public int RequiredPvpMedal { get; private set; }
        public int RequiredPvpRank { get; private set; }
        public int RequiredSkill { get; private set; }
        public int RequiredSkillRank { get; private set; }
        public int RequiredTransmogHolidayID { get; private set; }
        public int ScalingStatDistributionID { get; private set; }
        public int SellPrice { get; private set; }
        public int SheatheType { get; private set; }
        public int SocketMatchSpellEnchantmentID { get; private set; }
        public int SpellWeight { get; private set; }
        public int SpellWeightCategory { get; private set; }
        public int Stackable { get; private set; }
        public int StartQuestID { get; private set; }
        public int TotemCategoryID { get; private set; }
        public int VendorStackCount { get; private set; }
        public int ZoneBoundAreaID { get; private set; }
        public int[] Flags { get; private set; }
        public int[] SocketType { get; private set; }
        public int[] StatModifierBonusStat { get; private set; }
        public int[] StatPercentEditor { get; private set; }
        public long AllowableRace { get; private set; }

        [Diffable]
        public string Description { get; private set; }
        public string Display { get; private set; }
        public string Display1 { get; private set; }
        public string Display2 { get; private set; }
        public string Display3 { get; private set; }

        #region IDiffable
        [Diffable(Order = 0)]
        string IDiffable.Name => Display;

        public IEnumerable<string> SubCategories => Enumerable.Empty<string>();
        #endregion
    }
}
