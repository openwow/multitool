using System;
using System.Collections.Generic;
using System.Text;

namespace OpenWoW.Multitool.Entities
{
    internal class ItemEffectDb2 : Db2Base<ItemEffectDb2>, IDb2
    {
        public int CategoryCooldownMs { get; private set; }
        public int Charges { get; private set; }
        public int ChrSpecializationID { get; private set; }
        public int CooldownMs { get; private set; }
        public int LegacySlotIndex { get; private set; }
        public int ParentItemID { get; private set; }
        public int SpellCategoryID { get; private set; }
        public int SpellID { get; private set; }
        public int TriggerType { get; private set; }
    }
}
