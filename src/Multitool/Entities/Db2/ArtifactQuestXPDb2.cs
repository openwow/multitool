using System;
using System.Collections.Generic;
using System.Text;

namespace OpenWoW.Multitool.Entities
{
    internal class ArtifactQuestXPDb2 : Db2Base<ArtifactQuestXPDb2>, IDb2
    {
        public int[] Difficulty { get; private set; }
    }
}
