using System;
using System.Collections.Generic;
using System.Text;

namespace OpenWoW.Multitool.Entities
{
    internal class SpellReagentsDb2 : Db2Base<SpellReagentsDb2>, IDb2
    {
        public int SpellID { get; private set; }
        public int[] ItemID { get; private set; }
        public int[] Quantity { get; private set; }
    }
}
