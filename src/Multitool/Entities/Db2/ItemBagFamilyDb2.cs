using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace OpenWoW.Multitool.Entities
{
    [Category(DiffCategory.Miscellaneous)]
    [SubCategory("Item Bag Families")]
    internal class ItemBagFamilyDb2 : Db2Base<ItemBagFamilyDb2>, IDb2, IDiffable
    {
        [Diffable]
        public string Name { get; private set; }

        public IEnumerable<string> SubCategories => Enumerable.Empty<string>();
    }
}
