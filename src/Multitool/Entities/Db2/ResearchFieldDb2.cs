using System;
using System.Collections.Generic;
using System.Text;

namespace OpenWoW.Multitool.Entities
{
    internal class ResearchFieldDb2 : Db2Base<ResearchFieldDb2>, IDb2
    {
        public int Slot { get; private set; }
        public string Name { get; private set; }
    }
}
