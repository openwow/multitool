using System;
using System.Collections.Generic;
using System.Text;

namespace OpenWoW.Multitool.Entities
{
    internal class ScalingStatDistributionDb2 : Db2Base<ScalingStatDistributionDb2>, IDb2
    {
        public int MaxLevel { get; private set; }
        public int MinLevel { get; private set; }
        public int PlayerLevelToItemLevelCurveID { get; private set; }
    }
}
