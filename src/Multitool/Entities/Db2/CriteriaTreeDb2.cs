using OpenWoW.Common;
using System;
using System.Collections.Generic;
using System.Text;

namespace OpenWoW.Multitool.Entities
{
    internal class CriteriaTreeDb2 : Db2Base<CriteriaTreeDb2>, IDb2
    {
        public int Amount { get; private set; }
        public int Operator { get; private set; }
        public int OrderIndex { get; private set; }
        public int Parent { get; private set; }
        public string Description { get; private set; }

        public CriteriaTreeFlags Flags { get; private set; }

        public CriteriaDb2 Criteria { get; private set; }
    }
}
