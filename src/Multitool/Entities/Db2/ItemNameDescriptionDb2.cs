using System;
using System.Collections.Generic;
using System.Text;

namespace OpenWoW.Multitool.Entities
{
    internal class ItemNameDescriptionDb2 : Db2Base<ItemNameDescriptionDb2>, IDb2
    {
        public int Color { get; private set; }
        public string Description { get; private set; }
    }
}
