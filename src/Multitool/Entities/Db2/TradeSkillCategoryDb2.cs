using System;
using System.Collections.Generic;
using System.Text;

namespace OpenWoW.Multitool.Entities
{
    internal class TradeSkillCategoryDb2 : Db2Base<TradeSkillCategoryDb2>, IDb2
    {
        public int Flags { get; private set; }
        public int OrderIndex { get; private set; }
        public int ParentTradeSkillCategoryID { get; private set; }
        public int SkillLineID { get; private set; }
        public string HordeName { get; private set; }
        public string Name { get; private set; }
    }
}
