using System;
using System.Collections.Generic;
using System.Text;

namespace OpenWoW.Multitool.Entities
{
    internal class SpellDispelTypeDb2 : Db2Base<SpellDispelTypeDb2>, IDb2
    {
        public int ImmunityPossible { get; private set; }
        public int Mask { get; private set; }
        public string InternalName { get; private set; }
        public string Name { get; private set; }
    }
}
