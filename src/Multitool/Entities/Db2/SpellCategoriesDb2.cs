using System;
using System.Collections.Generic;
using System.Text;

namespace OpenWoW.Multitool.Entities
{
    internal class SpellCategoriesDb2 : Db2Base<SpellCategoriesDb2>, IDb2
    {
        public int CategoryID { get; private set; }
        public int DefenseType { get; private set; }
        public int DifficultyID { get; private set; }
        public int DispelType { get; private set; }
        public int Mechanic { get; private set; }
        public int PreventionType { get; private set; }
        public int StartRecoveryCategoryID { get; private set; }

        public SpellDb2 Spell { get; private set; }
        public SpellCategoryDb2 ChargeCategory { get; private set; }
    }
}
