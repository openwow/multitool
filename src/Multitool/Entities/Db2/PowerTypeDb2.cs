
namespace OpenWoW.Multitool.Entities
{
    internal class PowerTypeDb2 : Db2Base<PowerTypeDb2>, IDb2
    {
        public float RegenCombat { get; private set; }
        public float RegenPeace { get; private set; }
        public int CenterPower { get; private set; }
        public int DefaultPower { get; private set; }
        public int DisplayModifier { get; private set; }
        public int Flags { get; private set; }
        public int MaxBasePower { get; private set; }
        public int MinPower { get; private set; }
        public int PowerTypeEnum { get; private set; }
        public int RegenInterruptTimeMs { get; private set; }
        public string CostGlobalStringTag { get; private set; }
        public string NameGlobalStringTag { get; private set; }

        private string _name = null;
        public string Name
        {
            get
            {
                if (_name == null)
                {
                    if (!BuildLoader.Relationships.GlobalStrings.TryGetValue(NameGlobalStringTag, out _name))
                    {
                        _name = NameGlobalStringTag;
                    }
                }
                return _name;
            }
        }

        private string _cost = null;
        public string Cost
        {
            get
            {
                if (_cost == null)
                {
                    if (!BuildLoader.Relationships.GlobalStrings.TryGetValue(CostGlobalStringTag, out _cost))
                    {
                        _cost = CostGlobalStringTag;
                    }
                }
                return _cost;
            }
        }
    }
}
