using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace OpenWoW.Multitool.Entities
{
    [Category(DiffCategory.Miscellaneous)]
    [SubCategory("Reserved Names")]
    internal class NamesReservedDb2 : Db2Base<NamesReservedDb2>, IDb2, IDiffable
    {
        [Diffable]
        public string Name { get; private set; }

        public IEnumerable<string> SubCategories => Enumerable.Empty<string>();
    }
}
