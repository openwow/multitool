using System;
using System.Collections.Generic;
using System.Text;

namespace OpenWoW.Multitool.Entities
{
    internal class AreaTableDb2 : Db2Base<AreaTableDb2>, IDb2
    {
        public float AmbientMultiplier { get; private set; }
        public int AmbienceID { get; private set; }
        public int AreaBit { get; private set; }
        public int ContinentID { get; private set; }
        public int ExplorationLevel { get; private set; }
        public int FactionGroupMask { get; private set; }
        public int IntroSound { get; private set; }
        public int MountFlags { get; private set; }
        public int ParentAreaID { get; private set; }
        public int PvpCombatWorldStateID { get; private set; }
        public int SoundProviderPref { get; private set; }
        public int UWAmbienceID { get; private set; }
        public int UWIntroSound { get; private set; }
        public int UWSoundProviderPref { get; private set; }
        public int UWZoneMusicID { get; private set; }
        public int WildBattlePetLevelMax { get; private set; }
        public int WildBattlePetLevelMin { get; private set; }
        public int WindSettingsID { get; private set; }
        public int ZoneMusicID { get; private set; }
        public int[] Flags { get; private set; }
        public int[] LiquidTypeID { get; private set; }
        public string AreaName { get; private set; }
        public string ZoneName { get; private set; }
    }
}
