using System;
using System.Collections.Generic;
using System.Text;

namespace OpenWoW.Multitool.Entities
{
    internal class MountTypeXCapabilityDb2 : Db2Base<MountTypeXCapabilityDb2>, IDb2
    {
        public int MountCapabilityID { get; private set; }
        public int MountTypeID { get; private set; }
        public int OrderIndex { get; private set; }
    }
}
