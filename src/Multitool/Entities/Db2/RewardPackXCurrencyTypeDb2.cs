using System;
using System.Collections.Generic;
using System.Text;

namespace OpenWoW.Multitool.Entities
{
    internal class RewardPackXCurrencyTypeDb2 : Db2Base<RewardPackXCurrencyTypeDb2>, IDb2
    {
        public int CurrencyTypesID { get; private set; }
        public int Quantity { get; private set; }
        public int RewardPackID { get; private set; }
    }
}
