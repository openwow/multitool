using System;
using System.Collections.Generic;
using System.Text;

namespace OpenWoW.Multitool.Entities
{
    internal class PvpTierDb2 : Db2Base<PvpTierDb2>, IDb2
    {
        public int AscendPvpTierID { get; private set; }
        public int AscendRating { get; private set; }
        public int DescendPvpTierID { get; private set; }
        public int DescendRating { get; private set; }
        public int PvpTierEnum { get; private set; }
        public int Rank { get; private set; }
        public int TierIconFileDataID { get; private set; }
        public string Name { get; private set; }
    }
}
