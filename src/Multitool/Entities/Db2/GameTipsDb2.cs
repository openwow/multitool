using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace OpenWoW.Multitool.Entities
{
    [Category(DiffCategory.Miscellaneous)]
    [SubCategory("Game Tips")]
    internal class GameTipsDb2 : Db2Base<GameTipsDb2>, IDb2, IDiffable
    {
        public int MaxLevel { get; private set; }
        public int MinLevel { get; private set; }
        public int SortIndex { get; private set; }
        [Diffable]
        public string Text { get; private set; }

        [Diffable(Order = 0)]
        string IDiffable.Name => $"Tip #{ID}";

        public IEnumerable<string> SubCategories => Enumerable.Empty<string>();
    }
}
