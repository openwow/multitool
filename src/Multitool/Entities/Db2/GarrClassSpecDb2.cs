using System;
using System.Collections.Generic;
using System.Text;

namespace OpenWoW.Multitool.Entities
{
    internal class GarrClassSpecDb2 : Db2Base<GarrClassSpecDb2>, IDb2
    {
        public int ClassUITextureAtlasMemberID { get; private set; }
        public int Flags { get; private set; }
        public int FollowerClassLimit { get; private set; }
        public int GarrFollItemSetID { get; private set; }
        public string ClassSpec { get; private set; }
        public string ClassSpecFemale { get; private set; }
        public string ClassSpecMale { get; private set; }
    }
}
