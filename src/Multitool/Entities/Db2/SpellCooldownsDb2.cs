using System;
using System.Collections.Generic;
using System.Text;

namespace OpenWoW.Multitool.Entities
{
    internal class SpellCooldownsDb2 : Db2Base<SpellCooldownsDb2>, IDb2
    {
        public int CategoryRecoveryTime { get; private set; }
        public int DifficultyID { get; private set; }
        public int RecoveryTime { get; private set; }
        public int StartRecoveryTime { get; private set; }

        public SpellDb2 Spell { get; private set; }
    }
}
