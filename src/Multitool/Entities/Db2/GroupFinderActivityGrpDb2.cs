using System;
using System.Collections.Generic;
using System.Text;

namespace OpenWoW.Multitool.Entities
{
    internal class GroupFinderActivityGrpDb2 : Db2Base<GroupFinderActivityGrpDb2>, IDb2
    {
        public int OrderIndex { get; private set; }
        public string Name { get; private set; }
    }
}
