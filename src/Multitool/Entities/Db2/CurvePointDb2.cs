using System;
using System.Collections.Generic;
using System.Text;

namespace OpenWoW.Multitool.Entities
{
    internal class CurvePointDb2 : Db2Base<CurvePointDb2>, IDb2
    {
        public float[] Position { get; private set; }
        public int CurveID { get; private set; }
        public int OrderIndex { get; private set; }
    }
}
