using System;
using System.Collections.Generic;
using System.Text;

namespace OpenWoW.Multitool.Entities
{
    internal class SpellShapeshiftDb2 : Db2Base<SpellShapeshiftDb2>, IDb2
    {
        public int SpellID { get; private set; }
        public int StanceBarOrder { get; private set; }
        public int[] ShapeshiftExclude { get; private set; }
        public int[] ShapeshiftMask { get; private set; }
    }
}
