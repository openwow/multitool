using System;
using System.Collections.Generic;
using System.Text;

namespace OpenWoW.Multitool.Entities
{
    internal class SpellRadiusDb2 : Db2Base<SpellRadiusDb2>, IDb2
    {
        public float Radius { get; private set; }
        public float RadiusMax { get; private set; }
        public float RadiusMin { get; private set; }
        public float RadiusPerLevel { get; private set; }
    }
}
