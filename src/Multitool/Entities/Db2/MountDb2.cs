using System;
using System.Collections.Generic;
using System.Text;

namespace OpenWoW.Multitool.Entities
{
    internal class MountDb2 : Db2Base<MountDb2>, IDb2
    {
        public float MountFlyRideHeight { get; private set; }
        public int Flags { get; private set; }
        public int MountTypeID { get; private set; }
        public int PlayerConditionID { get; private set; }
        public int SourceSpellID { get; private set; }
        public int SourceTypeEnum { get; private set; }
        public int UIModelSceneID { get; private set; }
        public string Description { get; private set; }
        public string Name { get; private set; }
        public string SourceText { get; private set; }
    }
}
