using OpenWoW.LibSpellParser.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace OpenWoW.Multitool.Entities
{
    internal class GarrAbilityEffectDb2 : Db2Base<GarrAbilityEffectDb2>, IDb2, IParserGarrAbilityEffect
    {
        public float ActionValueFlat { get; private set; }
        public float CombatWeightBase { get; private set; }
        public float CombatWeightMax { get; private set; }
        public int AbilityAction { get; private set; }
        public int AbilityTargetType { get; private set; }
        public int ActionHours { get; private set; }
        public int ActionRace { get; private set; }
        public int ActionRecordID { get; private set; }
        public int Flags { get; private set; }
        public int GarrAbilityID { get; private set; }
        public int GarrMechanicTypeID { get; private set; }
    }
}
