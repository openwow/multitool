using System;
using System.Collections.Generic;
using System.Text;

namespace OpenWoW.Multitool.Entities
{
    internal class GarrStringDb2 : Db2Base<GarrStringDb2>, IDb2
    {
        public string Text { get; private set; }
    }
}
