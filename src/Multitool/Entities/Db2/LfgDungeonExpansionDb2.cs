using System;
using System.Collections.Generic;
using System.Text;

namespace OpenWoW.Multitool.Entities
{
    internal class LfgDungeonExpansionDb2 : Db2Base<LfgDungeonExpansionDb2>, IDb2
    {
        public int ExpansionLevel { get; private set; }
        public int HardLevelMax { get; private set; }
        public int HardLevelMin { get; private set; }
        public int LFGID { get; private set; }
        public int RandomID { get; private set; }
        public int TargetLevelMax { get; private set; }
        public int TargetLevelMin { get; private set; }
    }
}
