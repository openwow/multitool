using OpenWoW.Common;
using OpenWoW.LibSpellParser.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace OpenWoW.Multitool.Entities
{
    internal class SpellEffectDb2 : Db2Base<SpellEffectDb2>, IDb2, IParserSpellEffect
    {
        public float Amplitude { get; private set; }
        public float BonusCoefficientFromAP { get; private set; }
        public float BonusCoefficientFromSP { get; private set; }
        public float ChainAmplitude { get; private set; }
        public float GroupSizeBasePointsCoefficient { get; private set; }
        public float PointsPerResource { get; private set; }
        public float PosFacing { get; private set; }
        public float PvpMultiplier { get; private set; }
        public float RealPoints { get; private set; }
        public float RealPointsPerLevel { get; private set; }
        public float ScalingCoefficient { get; private set; }
        public float ScalingResourceCoefficient { get; private set; }
        public float ScalingVariance { get; private set; }
        public int Attributes { get; private set; }
        public int AuraPeriod { get; private set; }
        public int ChainTargets { get; private set; }
        public int DifficultyID { get; private set; }
        public int Index { get; private set; }
        public int ItemID { get; private set; }
        public int Mechanic { get; private set; }
        public int[] ImplicitTarget { get; private set; }
        public int[] MiscValue { get; private set; }
        public int[] SpellClassMask { get; private set; }

        public SpellEffectAura Aura { get; private set; }
        public SpellEffectEffect Effect { get; private set; }

        public SpellDb2 Spell { get; internal set; }
        public SpellDb2 TriggerSpell { get; private set; }
        public SpellRadiusDb2[] RadiusIndex { get; private set; }

        public int Misc1 => MiscValue.SafeGet(0);
        public int Misc2 => MiscValue.SafeGet(1);
        public int SpellClassMask1 => SpellClassMask.SafeGet(0);
        public int SpellClassMask2 => SpellClassMask.SafeGet(1);
        public int SpellClassMask3 => SpellClassMask.SafeGet(2);
        public int SpellClassMask4 => SpellClassMask.SafeGet(3);

        private SpellDb2[] _affectedSpells = null;
        public IEnumerable<SpellDb2> AffectedSpells
        {
            get
            {
                if (_affectedSpells == null)
                {
                    if (SpellClassMask1 > 0 || SpellClassMask2 > 0 || SpellClassMask3 > 0 || SpellClassMask4 > 0)
                    {
                        int family = Spell?.ClassOptions?.SpellClassSet ?? 0;
                        _affectedSpells = BuildLoader.Db2.SpellClassOptions.All
                            .Where(x =>
                                x.Spell != null &&
                                (
                                    (x.SpellClassMask1 & SpellClassMask1) > 0 ||
                                    (x.SpellClassMask2 & SpellClassMask2) > 0 ||
                                    (x.SpellClassMask3 & SpellClassMask3) > 0 ||
                                    (x.SpellClassMask4 & SpellClassMask4) > 0
                                )
                            )
                            .Select(x => x.Spell)
                            .ToArray();
                    }
                    else
                    {
                        _affectedSpells = SpellDb2.EmptyArray;
                    }
                }
                return _affectedSpells;
            }
        }

        #region IParserSpellEffect
        float IParserSpellEffect.PointsPerLevel => RealPointsPerLevel;
        float IParserSpellEffect.Radius1 => RadiusIndex.SafeGet(0)?.Radius ?? 0;
        float IParserSpellEffect.Radius2 => RadiusIndex.SafeGet(1)?.Radius ?? 0;
        int IParserSpellEffect.BasePoints => (int)Math.Round(RealPoints); // FIXME: probably need to update everywhere to actually use the float
        int IParserSpellEffect.RandomPoints => 0;
        int IParserSpellEffect.TriggerSpellID => GetReflectID("TriggerSpell");

        public void GetEffectPoints(out double points, out double minPoints, out double maxPoints, int? itemLevel = null, bool doAbs = true, int itemSlot = 0, int? itemRarity = null, int? playerLevel = null)
        {
            double? multiplier = null;

            if (Effect == SpellEffectEffect.NormalizedWeaponDmg)
            {
                multiplier = Spell.Effects.Where(e => e.Effect == SpellEffectEffect.WeaponPercentDamage).Select(e => (double)e.RealPoints / 100).FirstOrDefault();
            }

            // FIXME
            if (Spell.Scaling != null && Spell.Scaling.GetPoints(this, BuildLoader.MaxLevel, itemLevel ?? 0, itemSlot, out points, out minPoints, out maxPoints))
            {
                if (multiplier != null)
                {
                    minPoints *= multiplier.Value;
                    maxPoints *= multiplier.Value;
                    points *= multiplier.Value;
                }

                minPoints = minPoints.SuperRound();
                maxPoints = maxPoints.SuperRound();
                points = points.SuperRound();

                return;
            }

            minPoints = RealPoints;
            maxPoints = RealPoints;

            /* Has a random component (10 to 25, and so on) */
            /*if (RandomPoints != 0)
            {
                minPoints += 1;
                maxPoints += RandomPoints;
            }*/

            /* Points per level component */
            if (RealPointsPerLevel != 0)
            {
                double delta = RealPointsPerLevel * (Spell.MaxLevel - Spell.MinLevel);
                //Console.WriteLine($"PPL2 Effect { ID } PointsPerLevel { PointsPerLevel } SpellMaxLevel { Spell.MaxLevel } SpellMinLevel { Spell.MinLevel } delta { delta }");

                minPoints += delta;
                maxPoints += delta;
            }


            if (doAbs)
            {
                minPoints = Math.Abs(minPoints);
                maxPoints = Math.Abs(maxPoints);
            }


            if (minPoints > maxPoints)
            {
                double tmp = minPoints;
                minPoints = maxPoints;
                maxPoints = tmp;
            }

            if (multiplier != null)
            {
                minPoints *= multiplier.Value;
                maxPoints *= multiplier.Value;
            }

            minPoints = minPoints.SuperRound();
            maxPoints = maxPoints.SuperRound();
            points = (minPoints + ((maxPoints - minPoints) / 2)).SuperRound();

            return;
        }

        public int GetModifierPoints(IEnumerable<int> specSpellIDs)
        {
            float ret = 0;
            if (specSpellIDs != null && Index <= 2)
            {
                int miscValue = Index == 0 ? (int)SpellAuraModifierMisc1.Effect1Value : (Index == 1 ? (int)SpellAuraModifierMisc1.Effect2Value : (int)SpellAuraModifierMisc1.Effect3Value);
                int spellID = GetReflectID("Spell");
                if (BuildLoader.Relationships.FlatModifierEffectsByMisc1.TryGetValue(miscValue, out SpellEffectDb2[] effects))
                {
                    foreach (var effect in effects)
                    {
                        var affectedSpellIDs = effect.AffectedSpells.Select(x => x.ID);
                        if (affectedSpellIDs.Contains(spellID))
                        {
                            ret += effect.RealPoints;
                        }
                    }
                }
            }
            return (int)Math.Round(ret);
        }
        #endregion
    }
}
