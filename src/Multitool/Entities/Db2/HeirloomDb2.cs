using System;
using System.Collections.Generic;
using System.Text;

namespace OpenWoW.Multitool.Entities
{
    internal class HeirloomDb2 : Db2Base<HeirloomDb2>, IDb2
    {
        public int Flags { get; private set; }
        public int ItemID { get; private set; }
        public int LegacyItemID { get; private set; }
        public int LegacyUpgradedItemID { get; private set; }
        public int SourceTypeEnum { get; private set; }
        public int StaticUpgradedItemID { get; private set; }
        public int[] UpgradeItemBonusListID { get; private set; }
        public int[] UpgradeItemID { get; private set; }
        public string SourceText { get; private set; }
    }
}
