using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace OpenWoW.Multitool.Entities
{
    [Category(DiffCategory.Miscellaneous)]
    [SubCategory("Factions")]
    internal class FactionDb2 : Db2Base<FactionDb2>, IDb2, IDiffable
    {
        public float[] ParentFactionMod { get; private set; }
        public int Expansion { get; private set; }
        public int Flags { get; private set; }
        public int FriendshipRepID { get; private set; }
        public int ReputationIndex { get; private set; }
        public int[] ParentFactionCap { get; private set; }
        public int[] ReputationBase { get; private set; }
        public int[] ReputationClassMask { get; private set; }
        public int[] ReputationFlags { get; private set; }
        public int[] ReputationMax { get; private set; }
        public long[] ReputationRaceMask { get; private set; }
        [Diffable]
        public string Description { get; private set; }
        [Diffable(Order = 0)]
        public string Name { get; private set; }

        public FactionDb2 ParagonFaction { get; private set; }
        public FactionDb2 ParentFaction { get; private set; }

        public IEnumerable<string> SubCategories
        {
            get
            {
                return Enumerable.Empty<string>();
            }
        }
    }
}
