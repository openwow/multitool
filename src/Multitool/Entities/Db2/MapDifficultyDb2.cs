using System;
using System.Collections.Generic;
using System.Text;

namespace OpenWoW.Multitool.Entities
{
    internal class MapDifficultyDb2 : Db2Base<MapDifficultyDb2>, IDb2
    {
        public int ContentTuningID { get; private set; }
        public int DifficultyID { get; private set; }
        public int Flags { get; private set; }
        public int ItemContext { get; private set; }
        public int ItemContextPickerID { get; private set; }
        public int LockID { get; private set; }
        public int MapID { get; private set; }
        public int MaxPlayers { get; private set; }
        public int ResetInterval { get; private set; }
        public string Message { get; private set; }
    }
}
