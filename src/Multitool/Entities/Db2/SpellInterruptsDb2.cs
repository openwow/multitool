using System;
using System.Collections.Generic;
using System.Text;

namespace OpenWoW.Multitool.Entities
{
    internal class SpellInterruptsDb2 : Db2Base<SpellInterruptsDb2>, IDb2
    {
        public int DifficultyID { get; private set; }
        public int InterruptFlags { get; private set; }
        public int[] AuraInterruptFlags { get; private set; }
        public int[] ChannelInterruptFlags { get; private set; }

        public SpellDb2 Spell { get; private set; }
    }
}
