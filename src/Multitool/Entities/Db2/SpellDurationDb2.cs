using OpenWoW.LibSpellParser.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace OpenWoW.Multitool.Entities
{
    internal class SpellDurationDb2 : Db2Base<SpellDurationDb2>, IDb2, IParserSpellDuration
    {
        public int Duration { get; private set; }
        public int DurationPerLevel { get; private set; }
        public int MaxDuration { get; private set; }
    }
}
