using OpenWoW.LibSpellParser.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace OpenWoW.Multitool.Entities
{
    internal class GarrAbilityDb2 : Db2Base<GarrAbilityDb2>, IDb2, IParserGarrAbility
    {
        public int FactionChangeGarrAbilityID { get; private set; }
        public int Flags { get; private set; }
        public int GarrAbilityCategoryID { get; private set; }
        public int GarrFollowerTypeID { get; private set; }
        public int IconFileDataID { get; private set; }
        public string Description { get; private set; }
        public string Name { get; private set; }
    }
}
