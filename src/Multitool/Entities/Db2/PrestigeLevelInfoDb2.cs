using System;
using System.Collections.Generic;
using System.Text;

namespace OpenWoW.Multitool.Entities
{
    internal class PrestigeLevelInfoDb2 : Db2Base<PrestigeLevelInfoDb2>, IDb2
    {
        public int AwardedAchievementID { get; private set; }
        public int BadgeTextureFileDataID { get; private set; }
        public int Flags { get; private set; }
        public int PrestigeLevel { get; private set; }
        public string Name { get; private set; }
    }
}
