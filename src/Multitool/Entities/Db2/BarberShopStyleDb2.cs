using System;
using System.Collections.Generic;
using System.Text;

namespace OpenWoW.Multitool.Entities
{
    internal class BarberShopStyleDb2 : Db2Base<BarberShopStyleDb2>, IDb2
    {
        public float CostModifier { get; private set; }
        public int Data { get; private set; }
        public int Race { get; private set; }
        public int Sex { get; private set; }
        public int Type { get; private set; }
        public string Description { get; private set; }
        public string DisplayName { get; private set; }
    }
}
