using System;
using System.Collections.Generic;
using System.Text;

namespace OpenWoW.Multitool.Entities
{
    internal class ArtifactAppearanceSetDb2 : Db2Base<ArtifactAppearanceSetDb2>, IDb2
    {
        public int AltHandUICameraID { get; private set; }
        public int ArtifactID { get; private set; }
        public int DisplayIndex { get; private set; }
        public int Flags { get; private set; }
        public int ForgeOverrideAttachmentPoint { get; private set; }
        public int UICameraID { get; private set; }
        public string Description { get; private set; }
        public string Name { get; private set; }
    }
}
