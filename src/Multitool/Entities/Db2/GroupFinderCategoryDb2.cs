using System;
using System.Collections.Generic;
using System.Text;

namespace OpenWoW.Multitool.Entities
{
    internal class GroupFinderCategoryDb2 : Db2Base<GroupFinderCategoryDb2>, IDb2
    {
        public int Flags { get; private set; }
        public int OrderIndex { get; private set; }
        public string Name { get; private set; }
    }
}
