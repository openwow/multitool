using System;
using System.Collections.Generic;
using System.Text;

namespace OpenWoW.Multitool.Entities
{
    internal class AuctionHouseDb2 : Db2Base<AuctionHouseDb2>, IDb2
    {
        public int ConsignmentRate { get; private set; }
        public int DepositRate { get; private set; }
        public int FactionID { get; private set; }
        public string Name { get; private set; }
    }
}
