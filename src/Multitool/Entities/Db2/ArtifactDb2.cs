using System;
using System.Collections.Generic;
using System.Text;

namespace OpenWoW.Multitool.Entities
{
    internal class ArtifactDb2 : Db2Base<ArtifactDb2>, IDb2
    {
        public int ArtifactCategoryID { get; private set; }
        public int ChrSpecializationID { get; private set; }
        public int ClassUITextureKitID { get; private set; }
        public int Flags { get; private set; }
        public int SpellVisualKitID { get; private set; }
        public int UIBarBackgroundColor { get; private set; }
        public int UIBarOverlayColor { get; private set; }
        public int UIModelSceneID { get; private set; }
        public int UINameColor { get; private set; }
        public string Name { get; private set; }
    }
}
