using OpenWoW.Common;
using System;
using System.Collections.Generic;
using System.Text;

namespace OpenWoW.Multitool.Entities
{
    internal class SpellScalingDb2 : Db2Base<SpellScalingDb2>, IDb2
    {
        public int Class { get; private set; }
        public int MaxScalingLevel { get; private set; }
        public int MinScalingLevel { get; private set; }
        public int ScalesFromItemLevel { get; private set; }

        public SpellDb2 Spell { get; private set; }

        public double GetPointsPerCombo(SpellEffectDb2 spellEffect, int level)
        {
            var spellScalingTable = BuildLoader.GameTable.SpellScaling.Get(level);
            return Math.Round(spellScalingTable.GetByCategory(Class) * spellEffect.ScalingResourceCoefficient);
        }

        public bool GetPoints(SpellEffectDb2 spellEffect, int level, int itemLevel, int itemSlot, out double points, out double minPoints, out double maxPoints, int? categoryOverride = null)
        {
            minPoints = 0;
            maxPoints = 0;
            points = 0;

            double coefficient, variance;

            int category = categoryOverride ?? Class;
            if (category == 0)
            {
                return false;
            }

            if (this.ScalesFromItemLevel != 0)
            {
                itemLevel = this.ScalesFromItemLevel;
                category = -1;
            }
            if (this.MaxScalingLevel != 0)
            {
                level = this.MaxScalingLevel;
            }
            if (category == -2 && spellEffect.Spell.ComputedLevel != 0)
            {
                level = spellEffect.Spell.ComputedLevel;
            }

            coefficient = spellEffect.ScalingCoefficient;
            variance = spellEffect.ScalingVariance;

            if ((category == -1 || category == -7) && itemLevel != 0)
            {
                // -1 is a special case ????
                // -7 is a special case where it wants to use CombatRatingsMultByILvl later, ugh
                var propPoints = BuildLoader.Db2.RandPropPoints.Get(itemLevel);
                minPoints = (propPoints?.Epic1 ?? 0) * coefficient;

                switch (itemSlot)
                {
                    case (int)InventorySlot.Trinket:
                        // WOW-719: category -7 is a special case and we need to apply combat rating scaling
                        if (category == -7)
                        {
                            var multTable = BuildLoader.GameTable.CombatRatingsMultByILvl.Get(itemLevel);
                            minPoints *= multTable.TrinketMultiplier;
                        }
                        break;

                    case (int)InventorySlot.Holdable:
                    default:
                        minPoints = (propPoints?.Superior1 ?? 0) * coefficient;
                        break;
                }
            }
            else if (category == -8)
            {
                // WOW-864: -8 is a RandPropPoints lookup on _player_ level, then we use DamageReplaceStat
                var propPoints = BuildLoader.Db2.RandPropPoints.Get(level);
                minPoints = (propPoints?.DamageReplaceStat ?? 0) * coefficient;
            }
            else
            {
                var spellScaling = BuildLoader.GameTable.SpellScaling.Get(level);
                minPoints = spellScaling.GetByCategory(category) * coefficient;
            }

            //if (MaxCastTime != 0)
            //    minPoints *= (GetCastTimeMs(level) / (double)MaxCastTime);

            //if (MaxLevel != 0 && MaxLevelFactor != 0)
            //    minPoints *= (Math.Min(level, MaxLevel) + (MaxLevelFactor * Math.Max(0, level - MaxLevel))) / level;
            maxPoints = minPoints;

            if (variance != 0)
            {
                minPoints -= (minPoints * variance / 2);
                maxPoints += (maxPoints * variance / 2);
            }

            minPoints = Math.Round(minPoints);
            maxPoints = Math.Round(maxPoints);
            points = (minPoints + ((maxPoints - minPoints) / 2)).SuperRound();

            return true;
        }
    }
}
