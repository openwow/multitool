using System;
using System.Collections.Generic;
using System.Text;

namespace OpenWoW.Multitool.Entities
{
    internal class SkillLineDb2 : Db2Base<SkillLineDb2>, IDb2
    {
        public int CanLink { get; private set; }
        public int Category { get; private set; }
        public int Flags { get; private set; }
        public int ParentSkillLineID { get; private set; }
        public int ParentTierIndex { get; private set; }
        public int SpellBookSpellID { get; private set; }
        public int SpellIconFileDataID { get; private set; }
        public string AlternateVerb { get; private set; }
        public string Description { get; private set; }
        public string DisplayName { get; private set; }
        public string HordeDisplayName { get; private set; }
        public string NeutralDisplayName { get; private set; }
    }
}
