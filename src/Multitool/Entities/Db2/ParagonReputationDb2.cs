using System;
using System.Collections.Generic;
using System.Text;

namespace OpenWoW.Multitool.Entities
{
    internal class ParagonReputationDb2 : Db2Base<ParagonReputationDb2>, IDb2
    {
        public int FactionID { get; private set; }
        public int LevelThreshold { get; private set; }
        public int QuestID { get; private set; }
    }
}
