using System;
using System.Collections.Generic;
using System.Text;

namespace OpenWoW.Multitool.Entities
{
    internal class ArtifactAppearanceDb2 : Db2Base<ArtifactAppearanceDb2>, IDb2
    {
        public float ModelDesaturation { get; private set; }
        public float ModelOpacity { get; private set; }
        public int AltItemAppearanceID { get; private set; }
        public int ArtifactAppearanceSetID { get; private set; }
        public int Flags { get; private set; }
        public int ItemAppearanceID { get; private set; }
        public int ItemAppearanceModiferID { get; private set; }
        public int OverrideShapeshiftCreatureDisplayInfoID { get; private set; }
        public int OverrideShapeshiftFormID { get; private set; }
        public int SwatchColor { get; private set; }
        public int UICameraID { get; private set; }
        public int UIOrder { get; private set; }
        public int UnlockFailurePlayerConditionID { get; private set; }
        public int UsablePlayerConditionID { get; private set; }
        public string AppearanceName { get; private set; }
    }
}
