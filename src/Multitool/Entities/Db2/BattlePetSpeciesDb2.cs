using System;
using System.Collections.Generic;
using System.Text;

namespace OpenWoW.Multitool.Entities
{
    internal class BattlePetSpeciesDb2 : Db2Base<BattlePetSpeciesDb2>, IDb2
    {
        public int CardUIModelSceneID { get; private set; }
        public int CreatureID { get; private set; }
        public int Flags { get; private set; }
        public int IconFileDataID { get; private set; }
        public int LoadoutUIModelSceneID { get; private set; }
        public int PetTypeEnum { get; private set; }
        public int SourceTypeEnum { get; private set; }
        public int SummonSpellID { get; private set; }
        public string Description { get; private set; }
        public string SourceText { get; private set; }
    }
}
