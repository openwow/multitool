using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace OpenWoW.Multitool.Entities
{
    [Category(DiffCategory.Miscellaneous)]
    [SubCategory("Creature Types")]
    internal class CreatureTypeDb2 : Db2Base<CreatureTypeDb2>, IDb2, IDiffable
    {
        public int Flags { get; private set; }
        [Diffable]
        public string Name { get; private set; }

        public IEnumerable<string> SubCategories => Enumerable.Empty<string>();
    }
}
