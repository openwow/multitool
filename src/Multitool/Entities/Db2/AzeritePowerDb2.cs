using System;
using System.Collections.Generic;
using System.Text;

namespace OpenWoW.Multitool.Entities
{
    internal class AzeritePowerDb2 : Db2Base<AzeritePowerDb2>, IDb2
    {
        public int AzeriteSpecSetID { get; private set; }
        public int Flags { get; private set; }
        public int ItemBonusListID { get; private set; }
        public int SpellID { get; private set; }
    }
}
