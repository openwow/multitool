using System;
using System.Collections.Generic;
using System.Text;

namespace OpenWoW.Multitool.Entities
{
    internal class ResearchSiteDb2 : Db2Base<ResearchSiteDb2>, IDb2
    {
        public int AreaPOIIconEnum { get; private set; }
        public int MapID { get; private set; }
        public int QuestPOIBlobID { get; private set; }
        public string Name { get; private set; }
    }
}
