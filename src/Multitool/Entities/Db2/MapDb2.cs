using EnumsNET;
using OpenWoW.Common;
using OpenWoW.Multitool.Tools.DiffBuild;
using System;
using System.Collections.Generic;
using System.Text;

namespace OpenWoW.Multitool.Entities
{
    [Category(DiffCategory.Miscellaneous)]
    [SubCategory("Maps")]
    internal class MapDb2 : Db2Base<MapDb2>, IDb2, IDiffable
    {
        public float MinimapIconScale { get; private set; }
        public float[] Corpse { get; private set; }
        public int AreaTableID { get; private set; }
        public int CorpseMapID { get; private set; }
        public int CosmeticParentMapID { get; private set; }
        public int InstanceType { get; private set; }
        public int LoadingScreenID { get; private set; }
        public int MapType { get; private set; }
        [Diffable(Name = "Max Players")]
        public int MaxPlayers { get; private set; }
        public int TimeOfDayOverride { get; private set; }
        public int TimeOffset { get; private set; }
        public int WDTFileDataID { get; private set; }
        public int WindSettingsID { get; private set; }
        public int ZMPFileDataID { get; private set; }
        public int[] Flags { get; private set; }
        [Diffable]
        public string Directory { get; private set; }
        [Diffable(Order = 1, Name = "Horde Description")]
        public string MapDescription0 { get; private set; }
        [Diffable(Order = 1, Name = "Alliance Description")]
        public string MapDescription1 { get; private set; }
        public string MapName { get; private set; }
        [Diffable(Order = 3, Name = "PvP Description")]
        public string PvpLongDescription { get; private set; }
        [Diffable(Order = 2, Name = "PvP Objective")]
        public string PvpShortDescription { get; private set; }

        public Expansion Expansion { get; private set; }

        public MapDb2 ParentMap { get; private set; }

        [Diffable(Name = "Expansion")]
        public string ExpansionName => Expansion.AsString(EnumFormat.DisplayName, EnumFormat.Name);

        [Diffable(Order = 0)]
        public string Name => string.Join(" -> ", TraversalHelpers.GetParentNames(this, (map) => map.ParentMap, (map) => map.MapName));

        public IEnumerable<string> SubCategories => new string[] { ExpansionName };
    }
}
