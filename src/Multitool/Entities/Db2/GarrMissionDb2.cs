using System;
using System.Collections.Generic;
using System.Text;

namespace OpenWoW.Multitool.Entities
{
    internal class GarrMissionDb2 : Db2Base<GarrMissionDb2>, IDb2
    {
        public float[] MapPos { get; private set; }
        public float[] WorldPos { get; private set; }
        public int AreaID { get; private set; }
        public int BaseCompletionChance { get; private set; }
        public int BaseFollowerXP { get; private set; }
        public int EnvGarrMechanicID { get; private set; }
        public int EnvGarrMechanicTypeID { get; private set; }
        public int Flags { get; private set; }
        public int FollowerDeathChance { get; private set; }
        public int GarrFollowerTypeID { get; private set; }
        public int GarrMissionSetID { get; private set; }
        public int GarrMissionTypeID { get; private set; }
        public int GarrTypeID { get; private set; }
        public int MaxFollowers { get; private set; }
        public int MissionCost { get; private set; }
        public int MissionCostCurrencyTypesID { get; private set; }
        public int MissionDuration { get; private set; }
        public int OfferDuration { get; private set; }
        public int OfferedGarrMissionTextureID { get; private set; }
        public int OverMaxRewardPackID { get; private set; }
        public int PlayerConditionID { get; private set; }
        public int TargetItemLevel { get; private set; }
        public int TargetLevel { get; private set; }
        public int TravelDuration { get; private set; }
        public int UITextureKitID { get; private set; }
        public string Description { get; private set; }
        public string Location { get; private set; }
        public string Name { get; private set; }
    }
}
