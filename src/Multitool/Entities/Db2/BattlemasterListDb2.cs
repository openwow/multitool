using System;
using System.Collections.Generic;
using System.Text;

namespace OpenWoW.Multitool.Entities
{
    internal class BattlemasterListDb2 : Db2Base<BattlemasterListDb2>, IDb2
    {
        public int Flags { get; private set; }
        public int GroupsAllowed { get; private set; }
        public int HolidayWorldStateID { get; private set; }
        public int IconFileDataID { get; private set; }
        public int InstanceType { get; private set; }
        public int MaxGroupSize { get; private set; }
        public int MaxLevel { get; private set; }
        public int MaxPlayers { get; private set; }
        public int MinLevel { get; private set; }
        public int MinPlayers { get; private set; }
        public int RatedPlayers { get; private set; }
        public int RequiredPlayerConditionID { get; private set; }
        public int[] MapID { get; private set; }
        public string GameType { get; private set; }
        public string LongDescription { get; private set; }
        public string Name { get; private set; }
        public string ShortDescription { get; private set; }
    }
}
