using System;
using System.Collections.Generic;
using System.Text;

namespace OpenWoW.Multitool.Entities
{
    internal class ItemDb2 : Db2Base<ItemDb2>, IDb2
    {
        public int IconFileDataID { get; private set; }
        public int InventoryType { get; private set; }
        public int ItemClassID { get; private set; }
        public int ItemGroupSoundsID { get; private set; }
        public int ItemSubClassID { get; private set; }
        public int Material { get; private set; }
        public int SheatheType { get; private set; }
        public int SoundOverrideSubClassID { get; private set; }
    }
}
