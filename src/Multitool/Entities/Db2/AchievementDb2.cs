﻿using OpenWoW.Common;
using OpenWoW.Multitool.Tools.DiffBuild;
using System.Collections.Generic;
using System.Linq;

namespace OpenWoW.Multitool.Entities
{
    [Category(DiffCategory.Achievements)]
    internal class AchievementDb2 : Db2Base<AchievementDb2>, IDb2, IDiffable
    {
        public int IconFileDataID { get; set; }
        public int InstanceID { get; set; }
        public int MinimumCriteria { get; set; }
        [Diffable]
        public int Points { get; set; }
        public int RewardItemID { get; set; }
        public int SharesCriteria { get; set; }
        public int Supersedes { get; set; }
        public int UIOrder { get; set; }

        [Diffable(Order = 1)]
        public string Description { get; set; }
        [Diffable(Order = 1)]
        public string Reward { get; set; }
        public string Title { get; set; }

        public AchievementFlags Flags { get; set; }
        [Diffable]
        public Faction Faction { get; set; }

        public AchievementCategoryDb2 Category { get; set; }
        public CriteriaTreeDb2 CriteriaTree { get; set; }

        #region IDiffable
        [Diffable(Order = 0)]
        string IDiffable.Name => Title;

        public IEnumerable<string> SubCategories => TraversalHelpers.GetParentNames(Category, (cat) => cat.Parent, (cat) => cat.Name);
        #endregion
    }
}
