using System;
using System.Collections.Generic;
using System.Text;

namespace OpenWoW.Multitool.Entities
{
    internal class ChrRacesDb2 : Db2Base<ChrRacesDb2>, IDb2
    {
        public float[] FemaleCustomizeOffset { get; private set; }
        public float[] MaleCustomizeOffset { get; private set; }
        public int BaseLanguage { get; private set; }
        public int BaseRaceID { get; private set; }
        public int CharComponentTextureLayoutID { get; private set; }
        public int CinematicSequenceID { get; private set; }
        public int CreateScreenFileDataID { get; private set; }
        public int CreatureTypeID { get; private set; }
        public int DefaultClassID { get; private set; }
        public int Faction { get; private set; }
        public int FactionID { get; private set; }
        public int FemaleDisplayID { get; private set; }
        public int FemaleModelFallbackRaceID { get; private set; }
        public int FemaleModelFallbackSex { get; private set; }
        public int FemaleSkeletonFileDataID { get; private set; }
        public int FemaleTextureFallbackRaceID { get; private set; }
        public int FemaleTextureFallbackSex { get; private set; }
        public int Flags { get; private set; }
        public int HDCharComponentTextureLayoutID { get; private set; }
        public int HDFemaleDisplayID { get; private set; }
        public int HDMaleDisplayID { get; private set; }
        public int HeritageArmorAchievementID { get; private set; }
        public int LowResScreenFileDataID { get; private set; }
        public int MaleDisplayID { get; private set; }
        public int MaleModelFallbackRaceID { get; private set; }
        public int MaleModelFallbackSex { get; private set; }
        public int MaleSkeletonFileDataID { get; private set; }
        public int MaleTextureFallbackRaceID { get; private set; }
        public int MaleTextureFallbackSex { get; private set; }
        public int NeutralRaceID { get; private set; }
        public int RelatedRaceID { get; private set; }
        public int ResSicknessSpellID { get; private set; }
        public int SelectScreenFileDataID { get; private set; }
        public int SplashSoundID { get; private set; }
        public int StartingLevel { get; private set; }
        public int UIDisplayOrder { get; private set; }
        public int UnalteredVisualRaceID { get; private set; }
        public int[] AlteredFormFinishSpellVisualKitID { get; private set; }
        public int[] AlteredFormStartSpellVisualKitID { get; private set; }
        public string ClientFileString { get; private set; }
        public string ClientPrefix { get; private set; }
        public string Name { get; private set; }
        public string NameFemale { get; private set; }
        public string NameFemaleLowercase { get; private set; }
        public string NameLowercase { get; private set; }
    }
}
