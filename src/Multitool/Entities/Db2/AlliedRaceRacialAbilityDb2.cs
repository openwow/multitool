using System;
using System.Collections.Generic;
using System.Text;

namespace OpenWoW.Multitool.Entities
{
    internal class AlliedRaceRacialAbilityDb2 : Db2Base<AlliedRaceRacialAbilityDb2>, IDb2
    {
        public int AlliedRaceID { get; private set; }
        public int IconFileDataID { get; private set; }
        public int Order { get; private set; }
        public string Description { get; private set; }
        public string Name { get; private set; }
    }
}
