using System;
using System.Collections.Generic;
using System.Text;

namespace OpenWoW.Multitool.Entities
{
    internal class LfgDungeonGroupDb2 : Db2Base<LfgDungeonGroupDb2>, IDb2
    {
        public int OrderIndex { get; private set; }
        public int ParentGroupID { get; private set; }
        public int TypeID { get; private set; }
        public string Name { get; private set; }
    }
}
