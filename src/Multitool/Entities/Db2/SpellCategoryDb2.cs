using System;
using System.Collections.Generic;
using System.Text;

namespace OpenWoW.Multitool.Entities
{
    internal class SpellCategoryDb2 : Db2Base<SpellCategoryDb2>, IDb2
    {
        public int ChargeRecoveryTime { get; private set; }
        public int Flags { get; private set; }
        public int MaxCharges { get; private set; }
        public int TypeMask { get; private set; }
        public int UsesPerWeek { get; private set; }
        public string Name { get; private set; }
    }
}
