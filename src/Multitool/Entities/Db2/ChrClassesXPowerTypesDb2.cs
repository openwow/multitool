using System;
using System.Collections.Generic;
using System.Text;

namespace OpenWoW.Multitool.Entities
{
    internal class ChrClassesXPowerTypesDb2 : Db2Base<ChrClassesXPowerTypesDb2>, IDb2
    {
        public ChrClassesDb2 Class { get; private set; }
        public PowerTypeDb2 PowerType { get; private set; }
    }
}
