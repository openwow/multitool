using System;
using System.Collections.Generic;
using System.Text;

namespace OpenWoW.Multitool.Entities
{
    internal class MountXDisplayDb2 : Db2Base<MountXDisplayDb2>, IDb2
    {
        public int CreatureDisplayInfoID { get; private set; }
        public int MountID { get; private set; }
        public int PlayerConditionID { get; private set; }
    }
}
