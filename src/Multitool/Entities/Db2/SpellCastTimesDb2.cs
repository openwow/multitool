using System;
using System.Collections.Generic;
using System.Text;

namespace OpenWoW.Multitool.Entities
{
    internal class SpellCastTimesDb2 : Db2Base<SpellCastTimesDb2>, IDb2
    {
        public int Base { get; private set; }
        public int Minimum { get; private set; }
        public int PerLevel { get; private set; }
    }
}
