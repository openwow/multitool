using System;
using System.Collections.Generic;
using System.Text;

namespace OpenWoW.Multitool.Entities
{
    internal class SpellShapeshiftFormDb2 : Db2Base<SpellShapeshiftFormDb2>, IDb2
    {
        public float DamageVariance { get; private set; }
        public int AttackIconFileDataID { get; private set; }
        public int BonusActionBar { get; private set; }
        public int CombatRoundTime { get; private set; }
        public int CreatureType { get; private set; }
        public int Flags { get; private set; }
        public int MountTypeID { get; private set; }
        public int[] CreatureDisplayInfoID { get; private set; }
        public int[] PresetSpellID { get; private set; }
        public string Name { get; private set; }
    }
}
