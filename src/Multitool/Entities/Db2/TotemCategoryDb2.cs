using System;
using System.Collections.Generic;
using System.Text;

namespace OpenWoW.Multitool.Entities
{
    internal class TotemCategoryDb2 : Db2Base<TotemCategoryDb2>, IDb2
    {
        public int TotemCategoryMask { get; private set; }
        public int TotemCategoryType { get; private set; }
        public string Name { get; private set; }
    }
}
