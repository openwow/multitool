using OpenWoW.LibSpellParser.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace OpenWoW.Multitool.Entities
{
    internal class GarrBuildingDb2 : Db2Base<GarrBuildingDb2>, IDb2, IParserGarrBuilding
    {
        public int AllianceGameObjectID { get; private set; }
        public int AllianceSceneScriptPackageID { get; private set; }
        public int AllianceUITextureKitID { get; private set; }
        public int BonusGarrAbilityID { get; private set; }
        public int BuildSeconds { get; private set; }
        public int BuildingTypeID { get; private set; }
        public int CurrencyQuantity { get; private set; }
        public int CurrencyTypesID { get; private set; }
        public int Flags { get; private set; }
        public int GarrAbilityID { get; private set; }
        public int GarrSiteID { get; private set; }
        public int GarrTypeID { get; private set; }
        public int GoldCost { get; private set; }
        public int HordeGameObjectID { get; private set; }
        public int HordeSceneScriptPackageID { get; private set; }
        public int HordeUITextureKitID { get; private set; }
        public int IconFileDataID { get; private set; }
        public int MaxAssignments { get; private set; }
        public int ShipmentCapacity { get; private set; }
        public int UpgradeLevel { get; private set; }
        public string AllianceName { get; private set; }
        public string Description { get; private set; }
        public string HordeName { get; private set; }
        public string Tooltip { get; private set; }

        public string Name => AllianceName == HordeName ? AllianceName : $"{ AllianceName } / { HordeName }";
    }
}
