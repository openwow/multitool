using System;
using System.Collections.Generic;
using System.Text;

namespace OpenWoW.Multitool.Entities
{
    internal class PvpTalentCategoryDb2 : Db2Base<PvpTalentCategoryDb2>, IDb2
    {
        public int TalentSlotMask { get; private set; }
    }
}
