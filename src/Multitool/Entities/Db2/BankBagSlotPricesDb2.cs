using System;
using System.Collections.Generic;
using System.Text;

namespace OpenWoW.Multitool.Entities
{
    internal class BankBagSlotPricesDb2 : Db2Base<BankBagSlotPricesDb2>, IDb2
    {
        public int Cost { get; private set; }
    }
}
