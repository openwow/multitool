using System;
using System.Collections.Generic;
using System.Text;

namespace OpenWoW.Multitool.Entities
{
    internal class GarrFollowerDb2 : Db2Base<GarrFollowerDb2>, IDb2
    {
        public int AllianceCreatureID { get; private set; }
        public int AllianceFlavorGarrStringID { get; private set; }
        public int AllianceGarrClassSpecID { get; private set; }
        public int AllianceGarrFollItemSetID { get; private set; }
        public int AllianceGarrUIAnimRaceInfoID { get; private set; }
        public int AllianceIconFileDataID { get; private set; }
        public int AllianceSourceTypeEnum { get; private set; }
        public int AllianceUITextureKitID { get; private set; }
        public int AllySlottingBroadcastTextID { get; private set; }
        public int ClassID { get; private set; }
        public int Flags { get; private set; }
        public int FollowerLevel { get; private set; }
        public int GarrFollowerTypeID { get; private set; }
        public int GarrTypeID { get; private set; }
        public int Gender { get; private set; }
        public int HordeCreatureID { get; private set; }
        public int HordeFlavorGarrStringID { get; private set; }
        public int HordeGarrClassSpecID { get; private set; }
        public int HordeGarrFollItemSetID { get; private set; }
        public int HordeGarrUIAnimRaceInfoID { get; private set; }
        public int HordeIconFileDataID { get; private set; }
        public int HordeSlottingBroadcastTextID { get; private set; }
        public int HordeSourceTypeEnum { get; private set; }
        public int HordeUITextureKitID { get; private set; }
        public int ItemLevelArmor { get; private set; }
        public int ItemLevelWeapon { get; private set; }
        public int Quality { get; private set; }
        public int Vitality { get; private set; }
        public string AllianceSourceText { get; private set; }
        public string HordeSourceText { get; private set; }
        public string Name { get; private set; }
    }
}
