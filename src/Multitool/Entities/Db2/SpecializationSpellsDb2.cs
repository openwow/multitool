using System;
using System.Collections.Generic;
using System.Text;

namespace OpenWoW.Multitool.Entities
{
    internal class SpecializationSpellsDb2 : Db2Base<SpecializationSpellsDb2>, IDb2
    {
        public int DisplayOrder { get; private set; }
        public int OverridesSpellID { get; private set; }
        public int SpecID { get; private set; }
        public int SpellID { get; private set; }
        public string Description { get; private set; }
    }
}
