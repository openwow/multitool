using System;
using System.Collections.Generic;
using System.Text;

namespace OpenWoW.Multitool.Entities
{
    internal class RewardPackXItemDb2 : Db2Base<RewardPackXItemDb2>, IDb2
    {
        public int ItemID { get; private set; }
        public int ItemQuantity { get; private set; }
        public int RewardPackID { get; private set; }
    }
}
