using System;
using System.Collections.Generic;
using System.Text;

namespace OpenWoW.Multitool.Entities
{
    internal class CurveDb2 : Db2Base<CurveDb2>, IDb2
    {
        public int Flags { get; private set; }
        public int Type { get; private set; }
    }
}
