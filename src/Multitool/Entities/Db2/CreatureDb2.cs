using System;
using System.Collections.Generic;
using System.Text;

namespace OpenWoW.Multitool.Entities
{
    internal class CreatureDb2 : Db2Base<CreatureDb2>, IDb2
    {
        public float[] DisplayProbability { get; private set; }
        public int Classification { get; private set; }
        public int CreatureFamilyID { get; private set; }
        public int CreatureTypeID { get; private set; }
        public int StartAnimState { get; private set; }
        public int[] AlwaysItem { get; private set; }
        public int[] DisplayID { get; private set; }
        public string Name { get; private set; }
        public string NameAlt { get; private set; }
        public string Title { get; private set; }
        public string TitleAlt { get; private set; }
    }
}
