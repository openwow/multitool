using System;
using System.Collections.Generic;
using System.Text;

namespace OpenWoW.Multitool.Entities
{
    internal class ScenarioStepDb2 : Db2Base<ScenarioStepDb2>, IDb2
    {
        public int CriteriaTreeID { get; private set; }
        public int Flags { get; private set; }
        public int OrderIndex { get; private set; }
        public int RelatedStep { get; private set; }
        public int RewardQuestID { get; private set; }
        public int ScenarioID { get; private set; }
        public int Supersedes { get; private set; }
        public int UIWidgetSetID { get; private set; }
        public int VisibilityPlayerConditionID { get; private set; }
        public string Description { get; private set; }
        public string Title { get; private set; }
    }
}
