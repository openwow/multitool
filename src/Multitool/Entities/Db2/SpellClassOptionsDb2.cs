using OpenWoW.Common;
using System;
using System.Collections.Generic;
using System.Text;

namespace OpenWoW.Multitool.Entities
{
    internal class SpellClassOptionsDb2 : Db2Base<SpellClassOptionsDb2>, IDb2
    {
        public int ModalNextSpell { get; private set; }
        public int SpellClassSet { get; private set; }
        public int[] SpellClassMask { get; private set; }

        public SpellDb2 Spell { get; private set; }

        public int SpellClassMask1 => SpellClassMask.SafeGet(0);
        public int SpellClassMask2 => SpellClassMask.SafeGet(1);
        public int SpellClassMask3 => SpellClassMask.SafeGet(2);
        public int SpellClassMask4 => SpellClassMask.SafeGet(3);
    }
}
