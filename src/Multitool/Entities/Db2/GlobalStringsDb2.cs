using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace OpenWoW.Multitool.Entities
{
    [Category(DiffCategory.Miscellaneous)]
    [SubCategory("Global Strings")]
    internal class GlobalStringsDb2 : Db2Base<GlobalStringsDb2>, IDb2, IDiffable
    {
        public int Flags { get; private set; }
        public string BaseTag { get; private set; }
        [Diffable(Name = "Text")]
        public string TagText { get; private set; }

        [Diffable]
        public string Name => BaseTag;

        public IEnumerable<string> SubCategories => Enumerable.Empty<string>();
    }
}
