using System;
using System.Collections.Generic;
using System.Text;

namespace OpenWoW.Multitool.Entities
{
    internal class ScenarioDb2 : Db2Base<ScenarioDb2>, IDb2
    {
        public int AreaTableID { get; private set; }
        public int Flags { get; private set; }
        public int Type { get; private set; }
        public int UITextureKitID { get; private set; }
        public string Name { get; private set; }
    }
}
