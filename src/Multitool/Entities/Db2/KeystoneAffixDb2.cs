using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace OpenWoW.Multitool.Entities
{
    [Category(DiffCategory.Miscellaneous)]
    [SubCategory("Keystone Affixes")]
    internal class KeystoneAffixDb2 : Db2Base<KeystoneAffixDb2>, IDb2, IDiffable
    {
        public int FileDataID { get; private set; }
        [Diffable]
        public string Description { get; private set; }
        [Diffable(Order = 0)]
        public string Name { get; private set; }

        public IEnumerable<string> SubCategories => Enumerable.Empty<string>();
    }
}
