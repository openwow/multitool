using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace OpenWoW.Multitool.Entities
{
    [Category(DiffCategory.Miscellaneous)]
    [SubCategory("Creature Families")]
    internal class CreatureFamilyDb2 : Db2Base<CreatureFamilyDb2>, IDb2, IDiffable
    {
        public float MaxScale { get; private set; }
        public float MinScale { get; private set; }
        public int IconFileDataID { get; private set; }
        public int MaxScaleLevel { get; private set; }
        public int MinScaleLevel { get; private set; }
        public int PetFoodMask { get; private set; }
        public int PetTalentType { get; private set; }
        public int[] SkillLine { get; private set; }
        [Diffable]
        public string Name { get; private set; }

        public IEnumerable<string> SubCategories => Enumerable.Empty<string>();
    }
}
