using System;
using System.Collections.Generic;
using System.Text;

namespace OpenWoW.Multitool.Entities
{
    internal class MapChallengeModeDb2 : Db2Base<MapChallengeModeDb2>, IDb2
    {
        public int Flags { get; private set; }
        public int MapID { get; private set; }
        public int[] CriteriaCount { get; private set; }
        public string Name { get; private set; }
    }
}
