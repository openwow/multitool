using System;
using System.Collections.Generic;
using System.Text;

namespace OpenWoW.Multitool.Entities
{
    internal class HolidaysDb2 : Db2Base<HolidaysDb2>, IDb2
    {
        public int CalendarFilterType { get; private set; }
        public int Flags { get; private set; }
        public int Looping { get; private set; }
        public int Priority { get; private set; }
        public int Region { get; private set; }
        public int[] CalendarFlags { get; private set; }
        public int[] Date { get; private set; }
        public int[] Duration { get; private set; }
        public int[] IconFileDataID { get; private set; }

        public HolidayDescriptionsDb2 HolidayDescription { get; private set; }
        public HolidayNamesDb2 HolidayName { get; private set; }
    }
}
