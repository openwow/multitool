using System;
using System.Collections.Generic;
using System.Text;

namespace OpenWoW.Multitool.Entities
{
    internal class ResearchBranchDb2 : Db2Base<ResearchBranchDb2>, IDb2
    {
        public int BigTextureFileDataID { get; private set; }
        public int CurrencyTypesID { get; private set; }
        public int ItemID { get; private set; }
        public int ResearchFieldID { get; private set; }
        public int TextureFileDataID { get; private set; }
        public string Name { get; private set; }
    }
}
