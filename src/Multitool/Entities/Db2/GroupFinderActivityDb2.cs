using System;
using System.Collections.Generic;
using System.Text;

namespace OpenWoW.Multitool.Entities
{
    internal class GroupFinderActivityDb2 : Db2Base<GroupFinderActivityDb2>, IDb2
    {
        public int AreaID { get; private set; }
        public int DifficultyID { get; private set; }
        public int DisplayType { get; private set; }
        public int Flags { get; private set; }
        public int GroupFinderActivityGrpID { get; private set; }
        public int GroupFinderCategoryID { get; private set; }
        public int MapID { get; private set; }
        public int MaxLevelSuggestion { get; private set; }
        public int MaxPlayers { get; private set; }
        public int MinGearLevelSuggestion { get; private set; }
        public int MinLevel { get; private set; }
        public int OrderIndex { get; private set; }
        public string FullName { get; private set; }
        public string ShortName { get; private set; }
    }
}
