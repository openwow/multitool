using System;
using System.Collections.Generic;
using System.Text;

namespace OpenWoW.Multitool.Entities
{
    internal class SourceInfoDb2 : Db2Base<SourceInfoDb2>, IDb2
    {
        public int PvpFaction { get; private set; }
        public int SourceTypeEnum { get; private set; }
        public int SpellID { get; private set; }
        public string SourceText { get; private set; }
    }
}
