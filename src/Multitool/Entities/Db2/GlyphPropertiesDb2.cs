using System;
using System.Collections.Generic;
using System.Text;

namespace OpenWoW.Multitool.Entities
{
    internal class GlyphPropertiesDb2 : Db2Base<GlyphPropertiesDb2>, IDb2
    {
        public int GlyphExclusiveCategoryID { get; private set; }
        public int GlyphType { get; private set; }
        public int SpellID { get; private set; }
        public int SpellIconID { get; private set; }
    }
}
