using System;
using System.Collections.Generic;
using System.Text;

namespace OpenWoW.Multitool.Entities
{
    internal class ToyDb2 : Db2Base<ToyDb2>, IDb2
    {
        public int Flags { get; private set; }
        public int ItemID { get; private set; }
        public int SourceTypeEnum { get; private set; }
        public string SourceText { get; private set; }
    }
}
