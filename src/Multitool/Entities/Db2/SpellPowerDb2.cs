using OpenWoW.LibSpellParser;
using OpenWoW.LibSpellParser.Interfaces;

namespace OpenWoW.Multitool.Entities
{
    internal class SpellPowerDb2 : Db2Base<SpellPowerDb2>, IDb2, IParserSpellPower
    {
        public float PowerCostMaxPct { get; private set; }
        public float PowerCostPct { get; private set; }
        public float PowerPctPerSecond { get; private set; }
        public int AltPowerBarID { get; private set; }
        public int ManaCost { get; private set; }
        public int ManaCostPerLevel { get; private set; }
        public int ManaPerSecond { get; private set; }
        public int OptionalCost { get; private set; }
        public int OrderIndex { get; private set; }
        public int PowerDisplayID { get; private set; }
        public int PowerType { get; private set; }
        public int RequiredAuraSpellID { get; private set; }

        public SpellDb2 Spell { get; private set; }

        public PowerTypeDb2 GetPowerType()
        {
            return BuildLoader.Relationships.GetPowerType(PowerType);
        }

        public override string ToString()
        {
            var powerType = GetPowerType();
            if (powerType == null)
            {
                return $"??";
            }

            if (PowerPctPerSecond != 0)
            {
                if (PowerCostPct != 0)
                {
                    return $"{ PowerCostPct }% of base { powerType.Name }, plus { PowerPctPerSecond }% per second";
                }
                else
                {
                    return $"{ PowerPctPerSecond }% { powerType.Name } per second";
                }
            }
            else if (PowerCostPct != 0)
            {
                return $"{ PowerCostPct }% of base { powerType.Name }";
            }
            else if (PowerCostMaxPct != 0)
            {
                return $"{ PowerCostMaxPct } of total { powerType.Name }";
            }

            double powerCost = ManaCost, powerCostPerSecond = ManaPerSecond;
            if (powerType.DisplayModifier > 1)
            {
                powerCost /= powerType.DisplayModifier;
                powerCostPerSecond /= powerType.DisplayModifier;
            }

            if (powerCost <= 0)
            {
                return null;
            }

            string ret = $"{ powerCost.ToTooltipNumber() } { powerType.Name }";
            if (powerCostPerSecond != 0)
            {
                ret += $", plus { powerCostPerSecond.ToTooltipNumber() } per sec";
            }
            return ret;
        }
    }
}
