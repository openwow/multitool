using System;
using System.Collections.Generic;
using System.Text;

namespace OpenWoW.Multitool.Entities
{
    internal class GarrTalentTreeDb2 : Db2Base<GarrTalentTreeDb2>, IDb2
    {
        public int ClassID { get; private set; }
        public int Flags { get; private set; }
        public int GarrTypeID { get; private set; }
        public int MaxTiers { get; private set; }
        public int UIOrder { get; private set; }
        public int UITextureKitID { get; private set; }
        public string Title { get; private set; }
    }
}
