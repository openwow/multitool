using System;
using System.Collections.Generic;
using System.Text;

namespace OpenWoW.Multitool.Entities
{
    internal class ResearchProjectDb2 : Db2Base<ResearchProjectDb2>, IDb2
    {
        public int IconFileDataID { get; private set; }
        public int NumSockets { get; private set; }
        public int Rarity { get; private set; }
        public int RequiredWeight { get; private set; }
        public int ResearchBranchID { get; private set; }
        public int SpellID { get; private set; }
        public string Description { get; private set; }
        public string Name { get; private set; }
    }
}
