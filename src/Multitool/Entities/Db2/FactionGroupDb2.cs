using System;
using System.Collections.Generic;
using System.Text;

namespace OpenWoW.Multitool.Entities
{
    internal class FactionGroupDb2 : Db2Base<FactionGroupDb2>, IDb2
    {
        public int ConquestCurrencyFileDataID { get; private set; }
        public int HonorCurrencyFileDataID { get; private set; }
        public int MaskID { get; private set; }
        public string InternalName { get; private set; }
        public string Name { get; private set; }
    }
}
