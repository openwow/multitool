using System;
using System.Collections.Generic;
using System.Text;

namespace OpenWoW.Multitool.Entities
{
    [Filename("Achievement_Category")]
    internal class AchievementCategoryDb2 : Db2Base<AchievementCategoryDb2>, IDb2
    {
        public int UIOrder { get; private set; }
        public string Name { get; private set; }

        public AchievementCategoryDb2 Parent { get; private set; }
    }
}
