using System;
using System.Collections.Generic;
using System.Text;

namespace OpenWoW.Multitool.Entities
{
    internal class PvpTalentSlotUnlockDb2 : Db2Base<PvpTalentSlotUnlockDb2>, IDb2
    {
        public int DeathKnightLevelRequired { get; private set; }
        public int DemonHunterLevelRequired { get; private set; }
        public int LevelRequired { get; private set; }
        public int Slot { get; private set; }
    }
}
