﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OpenWoW.Multitool.Entities
{
    internal class SpellScalingGameTable : IGameTable<int>
    {
        public int Level { get; private set; } // 0

        public double Rogue { get; private set; } // 1
        public double Druid { get; private set; } // 2
        public double Hunter { get; private set; } // 3
        public double Mage { get; private set; } // 4
        public double Paladin { get; private set; } // 5
        public double Priest { get; private set; } // 6
        public double Shaman { get; private set; } // 7
        public double Warlock { get; private set; } // 8
        public double Warrior { get; private set; } // 9 
        public double DeathKnight { get; private set; } // 10
        public double Monk { get; private set; } // 11
        public double DemonHunter { get; private set; } // 12

        public double Item { get; private set; } // 13
        public double Consumable { get; private set; } // 14
        public double Gem1 { get; private set; } // 15
        public double Gem2 { get; private set; } // 16
        public double Gem3 { get; private set; } // 17
        public double Health { get; private set; } // 18
        public double DamageReplaceStat { get; private set; } // 19
        public double DamageSecondary { get; private set; } // 20

        public int Key => Level;

        public double GetByCategory(int category)
        {
            switch (category)
            {
                case 0: return 0;
                case 1: return Rogue;
                case 2: return Druid;
                case 3: return Hunter;
                case 4: return Mage;
                case 5: return Paladin;
                case 6: return Priest;
                case 7: return Shaman;
                case 8: return Warlock;
                case 9: return Warrior;
                case 10: return DeathKnight;
                case 11: return Monk;
                case 12: return DemonHunter;
                // Negative category = (class count) + ABS(category)
                case -1: return Item;
                case -2: return Consumable;
                case -3: return Gem1;
                case -4: return Gem2;
                case -5: return Gem3;
                case -6: return Health;
                case -7: return DamageReplaceStat;
                case -8: return DamageSecondary;
            }

            return 0;
        }
    }
}
