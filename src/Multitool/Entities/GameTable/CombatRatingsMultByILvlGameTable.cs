﻿using OpenWoW.Common;
using System;
using System.Collections.Generic;
using System.Text;

namespace OpenWoW.Multitool.Entities
{
    internal class CombatRatingsMultByILvlGameTable : IGameTable<int>
    {
        public int ItemLevel { get; private set; }
        public double ArmorMultiplier { get; private set; }
        public double JewelryMultiplier { get; private set; }
        public double TrinketMultiplier { get; private set; }
        public double WeaponMultiplier { get; private set; }

        public int Key => ItemLevel;

        public double GetValueByInventorySlot(InventorySlot inventorySlot)
        {
            switch (inventorySlot)
            {
                case InventorySlot.MHWeapon:
                case InventorySlot.OHWeapon:
                case InventorySlot.OffHandWeapon:
                case InventorySlot.OneHandWeapon:
                case InventorySlot.Holdable:
                case InventorySlot.Ranged:
                case InventorySlot.Ranged2:
                    return WeaponMultiplier;

                case InventorySlot.Trinket:
                    return TrinketMultiplier;

                case InventorySlot.Finger:
                case InventorySlot.Neck:
                    return JewelryMultiplier;

                default:
                    return ArmorMultiplier;
            }
        }
    }
}
