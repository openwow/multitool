﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OpenWoW.Multitool.Entities
{
    internal class StaminaMultByILvlGameTable : IGameTable<int>
    {
        public int ItemLevel { get; private set; }
        public double ArmorMultiplier { get; private set; }
        public double JewelryMultiplier { get; private set; }
        public double TrinketMultiplier { get; private set; }
        public double WeaponMultiplier { get; private set; }

        public int Key => ItemLevel;
    }
}
