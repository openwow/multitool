﻿using OpenWoW.Multitool.Tools.DiffBuild.Loaders;
using System;
using System.Collections.Generic;
using System.Reflection;

namespace OpenWoW.Multitool.Entities
{
    internal abstract class Db2Base<TDb2> where TDb2 : IDb2, new()
    {
        public int ID { get; set; }

        protected static BuildLoader BuildLoader { get; private set; }
        protected static Dictionary<(Db2Base<TDb2>, string), int> ReflectIDs = new Dictionary<(Db2Base<TDb2>, string), int>();
        public static readonly TDb2[] EmptyArray = new TDb2[] { };

        private static readonly string _subCategoryAttribute = null;

        public Db2Base()
        { }

        static Db2Base()
        {
            var subCategoryAttribute = typeof(TDb2).GetCustomAttribute<SubCategoryAttribute>();
            _subCategoryAttribute = subCategoryAttribute?.Name ?? string.Empty;
        }

        protected int GetReflectID(string fieldName) => ReflectIDs.GetValueOrDefault((this, fieldName));

        private string _subCategoryString = null;
        public string SubCategoryString
        {
            get
            {
                if (_subCategoryString == null)
                {
                    var parts = new List<string>();
                    if (_subCategoryAttribute != string.Empty)
                    {
                        parts.Add(_subCategoryAttribute);
                    }

                    if (this is IDiffable diffable)
                    {
                        parts.AddRange(((IDiffable)this).SubCategories);
                    }

                    _subCategoryString = string.Join(" - ", parts);
                }
                return _subCategoryString;
            }
        }
    }
}
