﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;

namespace OpenWoW.Multitool.Tasks
{
    public class MigrateDatabaseTask : TaskBase<MigrateDatabaseTask>
    {
        public MigrateDatabaseTask(string buildPath) : base(buildPath)
        {
        }

        protected override void Run()
        {
            using (var webDbContext = ToolConfig.WebDatabaseContext)
            {
                var sw = Stopwatch.StartNew();
                webDbContext.Database.Migrate();
                sw.Stop();
                _logger.Info("Database migrations took {0}ms", sw.ElapsedMilliseconds);
            }
        }
    }
}
