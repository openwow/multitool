﻿using OpenWoW.Common;
using OpenWoW.LibWowData;
using SereniaBLPLib;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;

namespace OpenWoW.Multitool.Tasks
{
    public class SplitRaceIconsTask : TaskBase<SplitRaceIconsTask>
    {
        private const int ATLAS_ID = 897;

        public SplitRaceIconsTask(string buildPath) : base(buildPath)
        {
        }

        protected override void Run()
        {
            string flagFile = Path.Combine(BuildPath, ".split_race_icons");
            if (File.Exists(flagFile))
            {
                return;
            }

            // Load the useful DB2
            // TODO: make this a generic function somewhere, we do the same thing over and over
            var db2Path = FileUtilities.GetFilenameCaseInsensitive(BuildPath, "DBFilesClient", "UITextureAtlasMember.db2");
            var bytes = File.ReadAllBytes(db2Path);

            var wdf = new WowDataFile(bytes);
            wdf.Load(onlyLoadHeader: true);

            var layout = ToolConfig.DBConfig.GetByTableHashAndLayoutHash(wdf.Header.TableHashHex, wdf.Header.LayoutHashHex);
            var fieldLayouts = layout.FixFields(wdf);

            wdf = new WowDataFile(bytes, fieldLayouts, layout.IdColumn);
            wdf.Load();

            // Process the file
            var columnMap = layout.Fields.Select((value, index) => (value, index)).ToDictionary(k => k.value.Name, v => v.index);
            var atlases = new List<(string, int, int, int, int)>();
            foreach (var record in wdf.Records)
            {
                int atlasID = record.Fields[columnMap["UITextureAtlasID"]].IntVal;
                if (atlasID != ATLAS_ID)
                {
                    continue;
                }

                string name = wdf.GetString(record.Fields[columnMap["CommittedName"]].UIntVal);
                int x = record.Fields[columnMap["CommittedLeft"]].IntVal;
                int w = record.Fields[columnMap["CommittedRight"]].IntVal - x;
                int y = record.Fields[columnMap["CommittedTop"]].IntVal;
                int h = record.Fields[columnMap["CommittedBottom"]].IntVal - y;

                //_logger.Debug("{0}: {1},{2} {3}x{4}", name, x, y, w, h);

                atlases.Add((name, x, y, w, h));
            }

            // Load image
            string imagePath = FileUtilities.GetFilenameCaseInsensitive(BuildPath, "Interface", "Glues", "CharacterCreate", "CharacterCreateIcons.blp");

            Bitmap bmp;
            using (var stream = new MemoryStream(File.ReadAllBytes(imagePath)))
            using (var blp = new BlpFile(stream))
            {
                bmp = blp.GetBitmap(0);
            }

            // Split image
            string dstDirectory = FileUtilities.GetFilenameCaseInsensitive(BuildPath, "Interface", "Icons");
            foreach (var tuple in atlases)
            {
                string dstPath = Path.Combine(dstDirectory, FileUtilities.SanitizeFilename(tuple.Item1) + ".png");
                using (var bmpIcon = bmp.Clone(new Rectangle(tuple.Item2, tuple.Item3, tuple.Item4, tuple.Item5), bmp.PixelFormat))
                {
                    bmpIcon.Save(dstPath);
                }
            }

            bmp.Dispose();

            //File.Create(flagFile).Close();
        }
    }
}
