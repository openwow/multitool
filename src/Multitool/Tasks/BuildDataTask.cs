﻿using Microsoft.EntityFrameworkCore;
using OpenWoW.Common;
using OpenWoW.Common.Extensions;
using OpenWoW.Database.Web.Entities;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;

namespace OpenWoW.Multitool.Tasks
{
    [DependsOn(typeof(MigrateDatabaseTask))]
    public class BuildDataTask : TaskBase<BuildDataTask>
    {
        public BuildDataTask(string buildPath) : base(buildPath)
        { }

        protected override void Run()
        {
            var sw = Stopwatch.StartNew();

            using (var webContext = ToolConfig.WebDatabaseContext)
            {
                webContext.ChangeTracker.AutoDetectChangesEnabled = false;

                var buildsPath = Path.Combine(BLTE.Constants.BasePath, "builds");
                if (!Directory.Exists(buildsPath))
                {
                    _logger.Error("BLTE builds path not found: {0}", buildsPath);
                    return;
                }

                var branches = new List<(WowBranch, BuildMatchAttribute)>();
                foreach (WowBranch branch in Enum.GetValues(typeof(WowBranch)))
                {
                    var buildMatchAttribute = branch.GetAttribute<BuildMatchAttribute>();
                    if (buildMatchAttribute != null)
                    {
                        branches.Add((branch, buildMatchAttribute));
                    }
                }

                sw.Restart();

                var buildDatas = new Dictionary<(WowBranch, int), BuildData>();

                var buildsInfo = new DirectoryInfo(buildsPath);
                foreach (FileInfo fileInfo in buildsInfo.GetFiles("*.txt"))
                {
                    foreach ((WowBranch branch, BuildMatchAttribute buildMatchAttribute) in branches)
                    {
                        var wbm = buildMatchAttribute.RegexMatch(branch, fileInfo.Name);
                        if (wbm == null)
                        {
                            continue;
                        }

                        DateTime lowestTime = fileInfo.CreationTime < fileInfo.LastWriteTime ? fileInfo.CreationTime : fileInfo.LastWriteTime;

                        if (!buildDatas.TryGetValue((branch, wbm.Build), out BuildData existing) || existing.Date > lowestTime)
                        {
                            // 8.x is expansion 7, etc
                            var expansion = (Expansion)(int.Parse(wbm.Patch.Split('.')[0]) - 1);
                            buildDatas[(branch, wbm.Build)] = new BuildData
                            {
                                Date = lowestTime,
                                Version = wbm.Patch,
                                Build = wbm.Build,
                                Branch = branch,
                                Expansion = expansion,
                            };
                        }

                        //_logger.Info("{0} => branch={1} patch={2} build={3} create={4} write{5}", fileInfo.Name, branch, wbm.Patch, wbm.Build, fileInfo.CreationTime, fileInfo.LastWriteTime);
                        break;
                    }

                    /*if (!foundMatch)
                    {
                        _logger.Debug("No match for {0}", fileInfo.Name);
                    }*/
                }

                sw.Stop();
                _logger.Info("Matched {0} builds in {1}ms", buildDatas.Count, sw.ElapsedMilliseconds);
                sw.Restart();

                // Fetch existing BuildDatas
                var buildDataMap = webContext.BuildData
                    //.AsNoTracking()
                    .ToDictionary(k => (k.Branch, k.Build));

                // Add any new ones
                foreach (var kvp in buildDatas)
                {
                    if (!buildDataMap.ContainsKey(kvp.Key))
                    {
                        webContext.BuildData.Add(kvp.Value);
                    }
                }
                webContext.SaveChanges();

                // This does dumb things, wtf:
                // - 640 rows exist, delete Id=640
                // - run the upsert
                // - new row is added with Id=1280 instead of 641
                /*webContext.BuildData
                    .UpsertRange(buildDatas.Values)
                    .On(bd => new { bd.Branch, bd.Build })
                    .Run();
                webContext.SaveChanges();*/

                sw.Stop();
                _logger.Info("Database voodoo took {0}ms", sw.ElapsedMilliseconds);
            }
        }
    }
}
