﻿using OpenWoW.Multitool.Tools;
using System;
using System.Collections.Generic;
using System.Text;

namespace OpenWoW.Multitool.Tasks
{
    public class ExtractTask : TaskBase<ExtractTask>
    {
        private BuildExtractorOptions Options;

        public ExtractTask(string buildPath) : base(buildPath)
        {
            Options = new BuildExtractorOptions
            {
                BuildPath = buildPath,
                Extract = ExtractType.DiffsAll,
            };
        }

        public ExtractTask(BuildExtractorOptions options) : base(options.BuildPath)
        {
            Options = options;
        }

        protected override void Run()
        {
            var extractor = new BuildExtractor(Options);
            extractor.Run();
        }
    }
}
