﻿using Microsoft.EntityFrameworkCore;
using OpenWoW.Database.Web.Entities;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace OpenWoW.Multitool.Tasks
{
    [DependsOn(typeof(MigrateDatabaseTask))]
    public class TableHashLookupTask : TaskBase<TableHashLookupTask>
    {
        public TableHashLookupTask(string buildPath) : base(buildPath)
        {
        }

        protected override void Run()
        {
            var tableHashes = new List<TableHashLookup>();
            foreach (string line in File.ReadAllLines(Path.Combine(ToolConfig.DbconfigPath, "TableHash.cfg")))
            {
                var parts = line.Split(": ");
                if (parts.Length == 2)
                {
                    tableHashes.Add(new TableHashLookup
                    {
                        Name = parts[0],
                        TableHash = uint.Parse(parts[1], System.Globalization.NumberStyles.HexNumber),
                    });
                }
            }

            using (var webDbContext = ToolConfig.WebDatabaseContext)
            {
                webDbContext.TableHashLookup
                    .UpsertRange(tableHashes)
                    .On(thl => new { thl._TableHash })
                    .Run();
                webDbContext.SaveChanges();
            }
        }
    }
}
