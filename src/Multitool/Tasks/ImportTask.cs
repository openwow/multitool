﻿using OpenWoW.Common;
using OpenWoW.LibWowData.DBConfig;
using OpenWoW.Multitool.BuildImport;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Text;

namespace OpenWoW.Multitool.Tasks
{
    [DependsOn(typeof(BuildDataTask))]
    [DependsOn(typeof(DbconfigLookupTask))]
    [DependsOn(typeof(ExtractTask))]
    [DependsOn(typeof(BuildHashTask))]
    [DependsOn(typeof(ConvertIconsTask))]
    [DependsOn(typeof(ConvertLoadingScreensTask))]
    [DependsOn(typeof(ConvertMapsTask))]
    public class ImportTask : TaskBase<ImportTask>
    {
        public ImportTask(string buildPath) : base(buildPath)
        {
        }

        protected override void Run()
        {
            string flagFile = Path.Combine(BuildPath, ".import");
            if (File.Exists(flagFile))
            {
                return;
            }

            var sw = Stopwatch.StartNew();

            var dbConfig = ToolConfig.DBConfig;

            sw.Stop();
            _logger.Debug("Load DBConfig took {0}ms", sw.ElapsedMilliseconds);
            sw.Restart();

            var importer = new BuildImporter(BuildPath, dbConfig);
            importer.Run();

            sw.Stop();
            _logger.Debug("Import took {0}ms", sw.ElapsedMilliseconds);

            File.Create(flagFile).Close();
        }
    }
}
