﻿using Microsoft.EntityFrameworkCore;
using OpenWoW.Common;
using OpenWoW.Database.Web.Entities;
using OpenWoW.Multitool.Tools.DiffBuild;
using System;
using System.Diagnostics;
using System.Linq;

namespace OpenWoW.Multitool.Tasks
{
    [DependsOn(typeof(BuildDataTask), 1)]
    [DependsOn(typeof(ExtractTask), 2)]
    [DependsOn(typeof(PublishImagesTask), 2)]
    public class DiffTask : TaskBase<DiffTask>
    {
        private string BuildPath2;

        public DiffTask(string oldBuildPath, string newBuildPath) : base(oldBuildPath)
        {
            BuildPath2 = newBuildPath;
        }

        public override object[] GetConstructorParams(Type taskType, int index = 0)
        {
            return new object[] { index == 0 ? BuildPath : BuildPath2 };
        }

        protected override void Run()
        {
            (WowBranch oldBranch, int oldBuild) = BuildUtilities.ParseDirectory(BuildPath);
            (WowBranch newBranch, int newBuild) = BuildUtilities.ParseDirectory(BuildPath2);

            // Diff
            var diffOptions = new BuildDifferOptions
            {
                OldBuildPath = BuildPath,
                NewBuildPath = BuildPath2,
                OldBranch = oldBranch,
                NewBranch = newBranch,
                OldBuild = oldBuild,
                NewBuild = newBuild,
                NotificationAction = (msg) => _logger.Debug(msg),
            };
            var differ = new BuildDiffer(diffOptions);
            differ.Run();

            // Save to database
            using (var webContext = ToolConfig.WebDatabaseContext)
            {
                BuildData oldBuildData = webContext.BuildData.SingleOrDefault(bd => bd.Branch == oldBranch && bd.Build == oldBuild);
                if (oldBuildData == null)
                {
                    _logger.Error("No build found for {0}/{1}", oldBranch, oldBuild);
                    return;
                }
                BuildData newBuildData = webContext.BuildData.SingleOrDefault(bd => bd.Branch == newBranch && bd.Build == newBuild);
                if (newBuildData == null)
                {
                    _logger.Error("No build found for {0}/{1}", newBranch, newBuild);
                    return;
                }

                var tooltips = differ.GenerateDiffTooltips(oldBuildData, newBuildData);

                var sw = Stopwatch.StartNew();

                webContext.DiffTooltip
                    .UpsertRange(tooltips)
                    .On(dt => new { dt.BuildId, dt.EntityType, dt.EntityId })
                    .Run();
                webContext.SaveChanges();

                sw.Stop();
                _logger.Info("Database voodoo took {0}ms", sw.ElapsedMilliseconds);
            }
        }
    }
}
