﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Reflection;

namespace OpenWoW.Multitool.Tasks
{
    public abstract class TaskBase<TTask>
    {
        protected string BuildPath;

        protected static readonly NLog.Logger _logger;
        private static readonly NLog.Logger _baseLogger = NLog.LogManager.GetCurrentClassLogger();

        static TaskBase()
        {
            _logger = NLog.LogManager.GetLogger(typeof(TTask).FullName);
        }

        public TaskBase(string buildPath)
        {
            BuildPath = buildPath;
        }

        public virtual object[] GetConstructorParams(Type taskType, int index = 0)
        {
            return new object[] { BuildPath };
        }

        protected abstract void Run();

        public void RunAll()
        {
            var totalSw = Stopwatch.StartNew();

            var taskType = typeof(TTask);
            _baseLogger.Debug("Resolving dependencies", taskType.Name);
            var graph = new DependencyGraph(taskType);
            var _taskOrder = graph.Resolve(taskType);
            _baseLogger.Debug("Task order: {0}", string.Join(", ", _taskOrder.Select(r => r.ToString())));

            var attributes = (DependsOnAttribute[])GetType().GetCustomAttributes(typeof(DependsOnAttribute), false);
            var thisType = GetType();

            foreach (DependencyNode taskNode in _taskOrder)
            {
                if (taskNode.Type == thisType)
                {
                    _baseLogger.Debug("Running task {0}", thisType);
                    var sw = Stopwatch.StartNew();
                    Run();
                    sw.Stop();
                    _baseLogger.Debug("Completed task {0} in {1}ms", thisType, sw.ElapsedMilliseconds);
                }
                else
                {
                    for (int runIndex = 0; runIndex < taskNode.Count; runIndex++)
                    {
                        var parameters = GetConstructorParams(taskNode.Type, runIndex);

                        _baseLogger.Debug("Running task {0}[{1}] => [ {2} ]", taskNode.Type, runIndex, string.Join(", ", parameters.Select(p => p.ToString())));

                        var instance = Activator.CreateInstance(taskNode.Type, parameters);
                        var methodInfo = taskNode.Type.GetMethod("Run", BindingFlags.Instance | BindingFlags.NonPublic);

                        var sw = Stopwatch.StartNew();
                        methodInfo.Invoke(instance, new object[] { });
                        sw.Stop();
                        _baseLogger.Debug("Completed task {0}[{1}] in {2}ms", taskNode.Type, runIndex, sw.ElapsedMilliseconds);
                    }
                }
            }

            totalSw.Stop();
            _baseLogger.Debug("RunAll took {0}ms", totalSw.ElapsedMilliseconds);
        }
    }
}
