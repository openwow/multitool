﻿using OpenWoW.Common;
using OpenWoW.Multitool.Tools;
using System.IO;

namespace OpenWoW.Multitool.Tasks
{
    [DependsOn(typeof(ExtractTask))]
    [DependsOn(typeof(SplitRaceIconsTask))]
    public class ConvertIconsTask : TaskBase<ConvertIconsTask>
    {
        public ConvertIconsTask(string buildPath) : base(buildPath)
        {
        }

        protected override void Run()
        {
            string flagFile = Path.Combine(BuildPath, ".convert_icons");
            if (File.Exists(flagFile))
            {
                return;
            }

            string srcDirectory = FileUtilities.GetFilenameCaseInsensitive(BuildPath, "interface", "icons");
            string dstDirectory = Path.Combine(BuildPath, "icons");

            BulkImageConverter.ConvertIcons(srcDirectory, dstDirectory);

            File.Create(flagFile).Close();
        }
    }
}
