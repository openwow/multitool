﻿using OpenWoW.Common;
using OpenWoW.Multitool.Tools.SyncBuild;

namespace OpenWoW.Multitool.Tasks
{
    [DependsOn(typeof(ImportTask))]
    [DependsOn(typeof(PublishImagesTask))]
    public class SyncTask : TaskBase<SyncTask>
    {
        public SyncTask(string buildPath) : base(buildPath)
        {
        }

        protected override void Run()
        {
            (WowBranch branch, int build) = BuildUtilities.ParseDirectory(BuildPath);
            var buildData = BuildUtilities.GetBuildDataByBranchAndBuild(branch, build);

            var syncer = new BuildSyncer(BuildPath, buildData);
            syncer.Run();
        }
    }
}
