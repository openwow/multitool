﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OpenWoW.Multitool.Tasks
{
    public class DependencyNode
    {
        public int Count;
        public Type Type;
        public HashSet<DependencyNode> Edges = new HashSet<DependencyNode>();

        public DependencyNode(Type type, int count)
        {
            Type = type;
            Count = count;
        }

        public void AddEdge(DependencyNode edge)
        {
            Edges.Add(edge);
        }

        public override string ToString()
        {
            return $"{ Type.Name }[{ Count }]";
        }
    }
}
