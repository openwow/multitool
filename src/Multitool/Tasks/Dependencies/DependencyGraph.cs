﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace OpenWoW.Multitool.Tasks
{
    public class DependencyGraph
    {
        public Dictionary<Type, DependencyNode> Nodes = new Dictionary<Type, DependencyNode>();

        public DependencyGraph(Type firstType)
        {
            FindDependencies(firstType, 1);
        }

        private void FindDependencies(Type taskType, int count)
        {
            var taskNode = GetNode(taskType, count);
            var attributes = (DependsOnAttribute[])taskType.GetCustomAttributes(typeof(DependsOnAttribute), false);
            foreach (var dependsOn in attributes)
            {
                int newCount = dependsOn.Count == -1 ? count : dependsOn.Count;
                var node = GetNode(dependsOn.TaskType, newCount);
                taskNode.AddEdge(node);
                FindDependencies(dependsOn.TaskType, newCount);
            }
        }

        private DependencyNode GetNode(Type type, int count)
        {
            if (!Nodes.TryGetValue(type, out DependencyNode node))
            {
                node = Nodes[type] = new DependencyNode(type, count);
            }
            return node;
        }

        public List<DependencyNode> Resolve(Type startType)
        {
            var resolved = new List<DependencyNode>();
            Visit(resolved, GetNode(startType, 1));
            return resolved;
        }

        private void Visit(List<DependencyNode> resolved, DependencyNode node)
        {
            foreach (var edge in node.Edges)
            {
                if (!resolved.Contains(edge))
                {
                    Visit(resolved, edge);
                }
            }
            resolved.Add(node);
        }
    }
}
