﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OpenWoW.Multitool.Tasks
{
    [AttributeUsage(AttributeTargets.Class, AllowMultiple = true)]
    public class DependsOnAttribute : Attribute
    {
        public Type TaskType { get; set; }
        public int Count { get; set; }

        public DependsOnAttribute(Type taskType, int count = -1)
        {
            TaskType = taskType;
            Count = count;
        }
    }
}
