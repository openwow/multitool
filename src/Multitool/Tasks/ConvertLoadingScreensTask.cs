﻿using OpenWoW.Common;
using OpenWoW.Multitool.Tools;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace OpenWoW.Multitool.Tasks
{
    [DependsOn(typeof(ExtractTask))]
    public class ConvertLoadingScreensTask : TaskBase<ConvertLoadingScreensTask>
    {
        public ConvertLoadingScreensTask(string buildPath) : base(buildPath)
        {
        }

        protected override void Run()
        {
            string flagFile = Path.Combine(BuildPath, ".convert_loadingscreens");
            if (File.Exists(flagFile))
            {
                return;
            }

            string srcDirectory = FileUtilities.GetFilenameCaseInsensitive(BuildPath, "interface", "glues", "loadingscreens");
            string dstDirectory = Path.Combine(BuildPath, "loadingscreens");

            BulkImageConverter.ConvertLoadingScreens(srcDirectory, dstDirectory);

            File.Create(flagFile).Close();
        }
    }
}
