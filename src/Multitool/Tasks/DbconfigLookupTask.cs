﻿using Microsoft.EntityFrameworkCore;
using OpenWoW.Database.Web.Entities;
using OpenWoW.LibWowData.DBConfig;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;

namespace OpenWoW.Multitool.Tasks
{
    [DependsOn(typeof(MigrateDatabaseTask))]
    public class DbconfigLookupTask : TaskBase<DbconfigLookupTask>
    {
        public DbconfigLookupTask(string buildPath) : base(buildPath)
        {
        }

        protected override void Run()
        {
            var sw = Stopwatch.StartNew();

            var dbConfig = ToolConfig.DBConfig;

            sw.Stop();
            _logger.Debug("Load DBConfig took {0}ms", sw.ElapsedMilliseconds);
            sw.Restart();

            var dbconfigLookups = new List<DbconfigLookup>();
            foreach (var layout in dbConfig.Layouts)
            {
                var fieldTypes = new byte[layout.Fields.Count];
                var arraySizes = new byte[layout.Fields.Count];
                var fieldNames = new string[layout.Fields.Count];

                for (int i = 0; i < layout.Fields.Count; i++)
                {
                    var fieldInfo = layout.Fields[i];
                    fieldTypes[i] = (byte)fieldInfo.Type;
                    arraySizes[i] = (byte)fieldInfo.ArraySize;
                    fieldNames[i] = fieldInfo.Name;
                }

                dbconfigLookups.Add(new DbconfigLookup
                {
                    TableHash = uint.Parse(layout.TableHash, System.Globalization.NumberStyles.HexNumber),
                    LayoutHash = uint.Parse(layout.LayoutHash, System.Globalization.NumberStyles.HexNumber),
                    IdColumn = (short)layout.IdColumn,
                    FieldTypes = fieldTypes,
                    ArraySizes = arraySizes,
                    FieldNames = fieldNames,
                });
            }

            using (var webDbContext = ToolConfig.WebDatabaseContext)
            {
                webDbContext.DbconfigLookup
                    .UpsertRange(dbconfigLookups)
                    .On(dl => new { dl._TableHash, dl._LayoutHash })
                    .Run();
                //webDbContext.SaveChanges();
            }

            sw.Stop();
            _logger.Debug("Database voodoo took {0}ms", sw.ElapsedMilliseconds);
        }
    }
}
