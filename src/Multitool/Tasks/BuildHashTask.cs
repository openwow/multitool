﻿using Microsoft.EntityFrameworkCore;
using OpenWoW.Common;
using OpenWoW.Database.Web.Entities;
using OpenWoW.LibWowData;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;

namespace OpenWoW.Multitool.Tasks
{
    [DependsOn(typeof(ExtractTask))]
    [DependsOn(typeof(MigrateDatabaseTask))]
    [DependsOn(typeof(TableHashLookupTask))]
    public class BuildHashTask : TaskBase<BuildHashTask>
    {
        public BuildHashTask(string buildPath) : base(buildPath)
        {
        }

        protected override void Run()
        {
            (WowBranch branch, int build) = BuildUtilities.ParseDirectory(BuildPath);

            using (var webContext = ToolConfig.WebDatabaseContext)
            {
                BuildData buildData = webContext.BuildData.SingleOrDefault(bd => bd.Branch == branch && bd.Build == build);
                if (buildData == null)
                {
                    _logger.Error("No build found for {0}/{1}", branch, build);
                    return;
                }

                _logger.Info("Using BuildData {0}/{1}/{2}", buildData.Id, buildData.Branch, buildData.Build);

                // Try to find DBFilesClient
                string directoryName = null;
                var directories = Directory.GetDirectories(BuildPath);
                for (int i = 0; i < directories.Length; i++)
                {
                    if (Path.GetFileName(directories[i]).Equals("DBFilesCLient", StringComparison.InvariantCultureIgnoreCase))
                    {
                        directoryName = directories[i];
                        break;
                    }
                }

                if (directoryName == null)
                {
                    throw new DirectoryNotFoundException($"Unable to find DBFilesClient directory in {BuildPath}");
                }

                var sw = Stopwatch.StartNew();

                //_logger.Debug("Using path {0}", directoryName);
                var buildHashes = new List<BuildHash>();
                foreach (string filename in Directory.GetFiles(directoryName))
                {
                    var wdf = new WowDataFile(filename);
                    wdf.Load(true);

                    buildHashes.Add(new BuildHash
                    {
                        BuildId = buildData.Id,
                        TableHash = wdf.Header.TableHash,
                        LayoutHash = wdf.Header.LayoutHash,
                    });
                }

                sw.Stop();
                _logger.Info("Load hashes took {0}ms", sw.ElapsedMilliseconds);
                sw.Restart();

                webContext.BuildHash
                    .UpsertRange(buildHashes)
                    .On(bh => new { bh.BuildId, bh._TableHash })
                    .Run();
                webContext.SaveChanges();

                sw.Stop();
                _logger.Info("Database voodoo took {0}ms", sw.ElapsedMilliseconds);
            }
        }
    }
}
