﻿using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace OpenWoW.Multitool.Tasks
{
    [DependsOn(typeof(ConvertIconsTask))]
    [DependsOn(typeof(ConvertLoadingScreensTask))]
    [DependsOn(typeof(ConvertMapsTask))]
    public class PublishImagesTask : TaskBase<PublishImagesTask>
    {
        private static readonly int[] IconSizes = new int[] { 16, 36, 56 };
        private static readonly string[] Paths = new string[] { "icons", "loadingscreens" };
        private static readonly string HexChars = "0123456789abcdef";

        public PublishImagesTask(string buildPath) : base(buildPath)
        {
        }

        protected override void Run()
        {
            // This task is fairly fast
            foreach (string typePath in Paths)
            {
                string targetPath = Path.Combine(ToolConfig.MediaPath, typePath);
                var existing = new HashSet<string>(Directory.GetFiles(targetPath, "*.*", SearchOption.AllDirectories));
                
                // Copy images
                if (typePath == "icons")
                {
                    CopyIcons(Path.Combine(BuildPath, typePath, "hash"), targetPath, existing);
                    CopyIcons(Path.Combine(BuildPath, typePath, "name"), targetPath, existing);
                }
                else
                {
                    CopyFiles(Path.Combine(BuildPath, typePath, "hash"), targetPath, existing);
                    CopyFiles(Path.Combine(BuildPath, typePath, "name"), targetPath, existing);
                }
            }
        }

        private void CopyFiles(string srcPath, string dstPath, HashSet<string> existing)
        {
            if (!Directory.Exists(srcPath))
            {
                _logger.Warn("Source path does not exist: {0}", srcPath);
                return;
            }

            Directory.CreateDirectory(dstPath);

            int copied = 0, skipped = 0;
            foreach (string srcFilePath in Directory.GetFiles(srcPath))
            {
                string filename = Path.GetFileName(srcFilePath);
                string dstFilePath = Path.Combine(dstPath, filename);
                if (!existing.Contains(dstFilePath))
                {
                    File.Copy(srcFilePath, dstFilePath);
                    copied++;
                }
                else
                {
                    skipped++;
                }
            }

            _logger.Info("Published images from {0} - {1} copied, {2} skipped", srcPath, copied, skipped);
        }

        private void CopyIcons(string srcPath, string dstPath, HashSet<string> existing)
        {
            if (!Directory.Exists(srcPath))
            {
                _logger.Warn("Source path does not exist: {0}", srcPath);
                return;
            }

            Directory.CreateDirectory(dstPath);

            var iconPaths = new List<(int, string, Rectangle)>(IconSizes.Length);
            foreach (int size in IconSizes)
            {
                string dstSizePath = Path.Combine(dstPath, size.ToString());
                Directory.CreateDirectory(dstSizePath);
                iconPaths.Add((size, dstSizePath, new Rectangle(0, 0, size, size)));
            }

            var encoder = ImageCodecInfo.GetImageEncoders().First(e => e.MimeType == "image/jpeg");
            var parameters = new EncoderParameters(1);
            parameters.Param[0] = new EncoderParameter(Encoder.Quality, 75L);

            int copied = 0, skipped = 0;
            string[] srcFiles = Directory.GetFiles(srcPath);
            Parallel.ForEach(srcFiles, (srcFilePath) =>
            {
                Image image = null;
                string filename = Path.GetFileName(srcFilePath);
                foreach ((int size, string dstSizePath, Rectangle dstRect) in iconPaths)
                {
                    string dstFilePath = Path.Combine(dstSizePath, filename);
                    if (!existing.Contains(dstFilePath))
                    {
                        if (image == null)
                        {
                            image = Image.FromFile(srcFilePath);
                        }

                        // Resize if required
                        if (image.Width != size)
                        {
                            using (var tempImage = new Bitmap(size, size))
                            using (var graphics = Graphics.FromImage(tempImage))
                            {
                                graphics.CompositingMode = CompositingMode.SourceCopy;
                                graphics.CompositingQuality = CompositingQuality.HighQuality;
                                graphics.InterpolationMode = InterpolationMode.HighQualityBicubic;
                                graphics.SmoothingMode = SmoothingMode.HighQuality;
                                graphics.PixelOffsetMode = PixelOffsetMode.HighQuality;

                                using (var wrapMode = new ImageAttributes())
                                {
                                    wrapMode.SetWrapMode(WrapMode.TileFlipXY); // this fixes a weird GDI+ blending bug
                                    graphics.DrawImage(image, dstRect, 0, 0, image.Width, image.Height, GraphicsUnit.Pixel, wrapMode);
                                }

                                tempImage.Save(Path.Combine(dstSizePath, filename), encoder, parameters);
                            }
                        }
                        else
                        {
                            image.Save(Path.Combine(dstSizePath, filename), encoder, parameters);
                        }

                        Interlocked.Increment(ref copied);
                    }
                    else
                    {
                        Interlocked.Increment(ref skipped);
                    }
                }

                image?.Dispose();
            });

            _logger.Info("Published images from {0} - {1} copied, {2} skipped", srcPath, copied, skipped);
        }
    }
}
