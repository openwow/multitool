﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OpenWoW.Multitool
{
    public interface IDiffable
    {
        IEnumerable<string> SubCategories { get; }
        string Name { get; }
    }
}
