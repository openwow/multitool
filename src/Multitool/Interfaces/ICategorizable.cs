﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OpenWoW.Multitool
{
    internal interface ICategorizable
    {
        bool ShouldDiff { get; }

        void Categorize();
    }
}
