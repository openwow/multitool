﻿using Microsoft.Extensions.Configuration;
using StackExchange.Redis;
using System.Threading.Tasks;

namespace OpenWoW.Discord
{
    public class RedisService
    {
        private readonly IConfigurationRoot _config;
        private ConnectionMultiplexer _connection;

        public RedisService(IConfigurationRoot config)
        {
            _config = config;
        }

        public async Task StartAsync()
        {
            _connection = await ConnectionMultiplexer.ConnectAsync(_config["connection_strings:redis"]);
        }

        public IDatabase GetDatabase()
        {
            return _connection.GetDatabase(_config.GetValue<int>("discord:redis_database", -1));
        }
    }
}
