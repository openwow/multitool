﻿using Discord;
using Discord.WebSocket;
using Microsoft.Extensions.Logging;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace OpenWoW.Discord
{
    public class NotificationService
    {
        private const string NOTIFICATION_LIST_KEY = "notification_queue";
        private const string NOTIFICATION_HASH_KEY = "notifications:{0}";
        private static ulong[] PATCH_NOTIFICATION_CHANNELS = new ulong[] {
            566310269798711306, // OpenWoW
            582688818449219595,
        };

        private readonly DiscordSocketClient _discord;
        private readonly RedisService _redis;
        private readonly ILogger<NotificationService> _logger;

        public NotificationService(DiscordSocketClient discord, RedisService redis, ILogger<NotificationService> logger)
        {
            _discord = discord;
            _redis = redis;
            _logger = logger;

            _discord.Ready += _discord_Ready;
        }

        // Starts the notification loop once the Discord client is ready
        private Task _discord_Ready()
        {
            Task.Run(NotificationLoop);
            return Task.CompletedTask;
        }

        private async Task NotificationLoop()
        {
            _logger.LogInformation("Notification loop started");
            try
            {
                var redis = _redis.GetDatabase();
                while (true)
                {
                    var key = await redis.ListLeftPopAsync(NOTIFICATION_LIST_KEY);
                    if (key.HasValue)
                    {
                        var hashKey = string.Format(NOTIFICATION_HASH_KEY, key);
                        var data = await redis.HashGetAllAsync(hashKey);
                        if (data.Length > 0)
                        {
                            _logger.LogInformation("Processing notification {0}", key);
                            var dataMap = data.ToDictionary(k => k.Name, v => v.Value.ToString());

                            // Let's just assume that things are correctly entered
                            switch (dataMap["type"])
                            {
                                case "patch":
                                    string message = "@everyone 🚨 PATCH 🚨\n" +
                                        $"- **Product**: { dataMap["product"] }\n" +
                                        $"- **Patch**: { dataMap["patch"] }\n" +
                                        $"- **Config hash**: { dataMap["config_hash"] }";

                                    foreach (ulong channelId in PATCH_NOTIFICATION_CHANNELS)
                                    {
                                        var channel = _discord.GetChannel(channelId) as ISocketMessageChannel;
                                        await channel.SendMessageAsync(message);
                                    }

                                    break;

                                default:
                                    _logger.LogWarning("Don't know what to do about notification type `{0}`!", dataMap["type"]);
                                    break;
                            }

                            await redis.KeyDeleteAsync(hashKey);
                        }
                        else
                        {
                            _logger.LogWarning("Notification queue contained item `{0}` but no data!", key);
                        }
                    }

                    await Task.Delay(1000);
                }
            }
            catch (Exception e)
            {
                _logger.LogError(e, "NotificationService exploded");
                throw;
            }
        }
    }
}
