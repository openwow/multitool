﻿using Discord;
using Discord.Commands;
using Discord.WebSocket;
using Microsoft.Extensions.Configuration;
using System;
using System.Reflection;
using System.Threading.Tasks;

namespace OpenWoW.Discord
{
    public class StartupService
    {
        private readonly IServiceProvider _provider;
        private readonly IConfigurationRoot _config;
        private readonly DiscordSocketClient _discord;
        private readonly CommandService _commands;
        private readonly RedisService _redis;
        private readonly NotificationService _notifications;

        public StartupService(DiscordSocketClient discord, CommandService commands, IConfigurationRoot config, IServiceProvider provider, RedisService redis, NotificationService notifications)
        {
            _provider = provider;
            _config = config;
            _discord = discord;
            _commands = commands;

            _redis = redis;
            _notifications = notifications;
        }

        public async Task StartAsync()
        {
            string discordToken = _config["discord:token"];     // Get the discord token from the config file
            if (string.IsNullOrWhiteSpace(discordToken))
            {
                throw new Exception("Missing Discord token");
            }

            // Connect to Redis
            await _redis.StartAsync();

            // Login to discord
            await _discord.LoginAsync(TokenType.Bot, discordToken);
            await _discord.StartAsync();

            // Load commands and modules into the command service
            await _commands.AddModulesAsync(Assembly.GetAssembly(typeof(StartupService)), _provider);
        }
    }
}
