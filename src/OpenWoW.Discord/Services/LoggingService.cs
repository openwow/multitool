﻿using Discord;
using Discord.Commands;
using Discord.WebSocket;
using Microsoft.Extensions.Logging;
using System;
using System.Threading.Tasks;

namespace OpenWoW.Discord
{
    public class LoggingService
    {
        private readonly DiscordSocketClient _discord;
        private readonly CommandService _commands;
        private readonly ILogger<LoggingService> _logger;

        public LoggingService(DiscordSocketClient discord, CommandService commands, ILogger<LoggingService> logger)
        {
            _discord = discord;
            _commands = commands;
            _logger = logger;

            _discord.Log += OnLogAsync;
            _commands.Log += OnLogAsync;
        }

        private Task OnLogAsync(LogMessage msg)
        {
            switch (msg.Severity)
            {
                case LogSeverity.Critical:
                    _logger.LogCritical(msg.Message);
                    break;
                case LogSeverity.Error:
                    _logger.LogError(msg.Exception.ToString());
                    break;
                case LogSeverity.Warning:
                    _logger.LogWarning(msg.Message);
                    break;
                case LogSeverity.Info:
                case LogSeverity.Verbose:
                    _logger.LogInformation(msg.Message);
                    break;
                case LogSeverity.Debug:
                    _logger.LogDebug(msg.Message);
                    break;
            }
            return Task.CompletedTask;

            // TODO: better logging
            string logText = $"{DateTime.UtcNow.ToString("hh:mm:ss")} [{msg.Severity}] {msg.Source}: {msg.Exception?.ToString() ?? msg.Message}";

            return Console.Out.WriteLineAsync(logText);

            //return Task.CompletedTask;
        }
    }
}
