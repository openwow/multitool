import os
import re
import sys


typeMap = dict(
    int64="long",
    int32="int",
    int16="int",
    int8="int",
    uint32="int",
    uint16="int",
    uint8="int",
)


def main():
    for className in sys.argv[1:]:
        configPath = os.path.join("..", "dbconfig", "Config", className + ".cfg")
        if not os.path.exists(configPath):
            print("Can't find config", configPath)
            continue

        className = className.replace("_", "")

        csPath = os.path.join("src", "BuildDiff", "Db2s", className + "Db2.cs")
        if os.path.exists(csPath):
            print("EXISTS!", csPath)
            #continue

        outFile = open(csPath, 'w')
        outFile.write("""using BuildDiff.Loaders;
using System;
using System.Collections.Generic;
using System.Text;

namespace BuildDiff.Db2s
{
    internal class %sDb2 : Db2Base<%sDb2>, IDb2
    {
""" % (className, className))

        props = []
        for line in open(configPath):
            line = line.strip()
            if line == '':
                break
            
            if line.startswith("BUILD") or line.startswith("LAYOUT"):
                continue

            m = re.match(r'^(\w+)(?:\[(\d+)\])? (.*?)$', line)
            if m:
                name = m.group(3).replace(" ", "").replace("_", "")
                if name == '' or name == 'ID' or name[0].isdigit():
                    continue

                typ = typeMap.get(m.group(1), m.group(1))
                if m.group(2) is not None:
                    typ = typ + '[]'

                props.append('        public %s %s { get; private set; }\n' % (typ, name))
            else:
                print(':(', line)

        for prop in sorted(props):
            outFile.write(prop)

        outFile.write("""    }
}
""")

        outFile.close()


if __name__ == '__main__':
    main()
