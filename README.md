# Multitool

This project provides a variety of strange and wonderful tools to use.

## Commands

### Test

This is a test of the emergency tool system.

## Creating a command

1. Create a new class in `Commands/` that implements the `ICommand` interface.
1. If you need to handle arguments/options, you'll probably want to look at the [CommandLineUtils docs](https://natemcmaster.github.io/CommandLineUtils/docs/options.html?tabs=using-builder-api).
1. TODO: implement common argument verifiers somewhere (branch should match `/^Live|PTR|Beta$/i`, etc)
